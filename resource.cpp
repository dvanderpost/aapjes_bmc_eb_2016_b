#include "resource.h"
#include <iostream>
#include <map>
#include <cmath>
#include <assert.h>

using namespace std;
//CONSTRUCTOR RESOURCETYPE
ResourceType::ResourceType(long apptime, long restime, float q)
{
  this->appear_time = apptime;
  this->residence_time = restime;
  this->quality = q;
}

//CONSTRUCTOR RESOURCETYPE
ResourceType::ResourceType(long apptime, long restime, float q, float h_pow_n, int n)
{
  this->appear_time = apptime;
  this->residence_time = restime;
  this->quality = q;
  this->h_pow_n = h_pow_n;
  this->n = n;
}

//CUNSTRUCTOR RESOURCE
Resource::Resource(Point2d pos, long app_time, int t)
{
  this->position = pos;   //TODO covert to some int
  appear_time=app_time;    
  type=t;
}

bool Resource::IsAvailable(long time, long YEAR, long restime)
{

  //outcome is an appear_time > time: not available
  //or apptime<time && apptime+restime > time: available
  if( this->appear_time <= time)
    {
      //update time when this year's residence time has passed
      this->UpdateAppearTime(time, YEAR, restime);
      
      if( (this->appear_time + restime)>=time) 
	{
	  return true;
	}
      else return false;
    }
  else return false;
}

void Resource::UpdateAppearTime(long time, long YEAR, long restime)
{
  bool CHECK=false;

  //update time when this year's residence time has passed
  /*if( (this->appear_time + restime) < time)
    {
      int x = (int)(((float)(time - (restime + this->appear_time)))/YEAR);
      this->appear_time = this->appear_time + (x+1)*YEAR;
      }*/

  while( (this->appear_time + restime) < time)
    this->appear_time+=YEAR;

  
  if(CHECK)
    {
      while( (this->appear_time + restime) < time)
	{
	  this->appear_time+=YEAR;
	  std::cout<<"res.cpp (UpdateAppearTime): ERROR! In while loop: YEAR "<<YEAR<<" aptime "<<appear_time<<std::endl;;
	  std::cin.get();
	}
    }
}

void Resource::GetsEaten(long time, long YEAR, long restime)
{
  this->UpdateAppearTime(time, YEAR, restime);
  if( (time >= this->appear_time) && (time <= this->appear_time + restime) ) //available now
    this->appear_time+=YEAR; //no longer available until next year
    }

//MAKES RECTANLGE FOR RSTAR_TREE
Rectangle Resource::mbr()
{
  Rectangle mbr;

  mbr.min.x = position.x-0.01; //TODO: check if this is sufficient
  mbr.max.x = position.x+0.01;
  mbr.min.y = position.y-0.01;
  mbr.max.y = position.y+0.01;

  return mbr;
}
