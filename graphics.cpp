//#include "main.h"
#include "graphics.h"
#include "environment.h"
#include "constants.h"
#include "aapjes.h"
#include "config.h"
#include <GL/freeglut.h>
#include <png.h>
#include <boost/lexical_cast.hpp>
#include <string.h>
#include <math.h>
#include <stdio.h>

namespace graphics
{
  //SPECIFIC FOR THIS MODEL
  void ShowIt(void); //GLUT FUNCTION WHICH CALLS THE FUNCTION BELOW
  void PatchDisplay();
  std::vector<Patch*> VisiblePatches();
  void ResourceDisplay();
  std::vector<Resource*> VisibleResources();
  void AapDisplay();
  void DisplayTrajectoryAap(Aapje* f);
  
  //MORE GENERAL FUNCTIONS FOR DISPLAYING 
  void DrawSector(Point2d pos, Vec2d dir, float radius, float angle); //draws circle or sector
  void DrawSector(float cx, float cy, float r, float start_angle, float arc_angle);//sector
  void DrawCircle(float cx, float cy, float r);//circle

  //GENERAL GLUT
  void InitGLUT(int *argc, char *argv[]);
  void InitGraph(void);

  //GLUT INTERATIVE + SAVING IMAGES
  void ScreenToWorldCoordinates(int screenx, int screeny, float *worldx, float *worldy);
  void Closer();
  void Further();
  void MyKeys(unsigned char key, int x, int y);
  void MyMice(int button, int state, int x, int y);
  void MyReshape(int w, int h);
  void MySpecialKeys(int key, int x, int y);
  void PNGImage();
  void SavePNGImage (char *imagename);
  void Up();
  void Down();
  void Right();
  void Left();
  void Reset();
  void Quit();
  
  //VARIABLES
  float fooddisplaysize = 0.5;
  bool treesdifferentcolors = false;
  bool displayfood = false;
  
  //display display_mode = TREE_DISPLAY;
  bool running = true;
  bool stepwise = false;
  bool updating = true;
  bool opname = false;
  bool movie_toggle = false;
  int movie_starttime, movie_stoptime;
  static GLfloat increase = 10.;
  static GLfloat factor = 10.;
  static GLfloat scale = 1.;
  const int XFIELD = 100;
  const int YFIELD = 100;
  int rwidth = XFIELD;
  int rheight = YFIELD;
  float transx = 0;
  float transy = 0;
  bool follow = false;
  int follow_target = 0;
  int delay = 0; // in milliseconds

  Environment *environment;
  std::vector<int> resource_colors;
  std::vector<Aapje*> population;
  
  /////////////////////////////////////////////////////////
  //SETTING UP POINTERS TO ENVIRONMENT AND FORAGERS (graphics.h)
  void set_environment(Environment *env)
  {
    environment = env;
   }

  void set_population(std::vector<Aapje*> pop)
  {
    population = pop;
  }

  //////////////////////////////////////////////////////////
  //SPECIFIC DISPLAY FUNCTIONS AND WHEN THEY ARE USED
  void ShowIt(void)
  {
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0,0.0,0.0);
    
    switch(display_mode)
      {
      default:
	PatchDisplay();
	ResourceDisplay();
	AapDisplay();
      }

    //allows for time delay
    struct timespec ts, tn;
    ts.tv_sec = 0;
    ts.tv_nsec = delay*1000000;
    nanosleep(&ts, &tn);
    
    if(!opname)
      glutSwapBuffers();
  }
 
  void AapDisplay()
  {
    for(int a=0; a<(int)population.size(); a++)
      {
	Aapje* f = dynamic_cast<Aapje*>(population[a]);
	glColor3f(0.0, 0.0, 0.0);
	DisplayTrajectoryAap(f);
	glColor4f(0.0, 0.0, 0.0, 0.4);
	DrawCircle(f->pos.x, f->pos.y, f->d_r);
	DrawSector(f->pos, f->dir, f->d_f, f->a_f);
	glColor3f(0.0, 0.0, 0.0);
	DrawCircle(f->pos.x, f->pos.y, (float)f->age/environment->MAX_AGE); //age dependent size

	if(f->food_target)
	  {
	    glColor4f(0.0, 0.0, 0.0, 0.1);
	    DrawCircle(f->food_target->position.x, f->food_target->position.y, fooddisplaysize);
	    glLineWidth(2.0);
	    glColor3f(0.0, 0.0, 0.0);
	    glBegin(GL_LINE_STRIP);
	    glVertex2f(f->pos.x, f->pos.y);
	    glVertex2f(f->food_target->position.x, f->food_target->position.y);
	    glEnd();
	  }

	//TODO: who copied who incremental network display (directional?)

	if(f->last_demon) //this shows who was the individuals last demonstrator
	  {
	    glLineWidth(5.0);
	    glColor3f(0.5, 0.5, 0.0);
	    glBegin(GL_LINE_STRIP);
	    glVertex2f(f->pos.x, f->pos.y);
	    glVertex2f(f->last_demon->pos.x, f->last_demon->pos.y);
	    glEnd();
	  }
      
	if(follow && follow_target==a)
	  {
	    glLoadIdentity();
	    transx = -f->pos.x;
	    transy = -f->pos.y;
	    glTranslatef(transx,transy,0.0); 
	  }
      }
  }

  void DisplayTrajectoryAap(Aapje* f)
  {
    int i;
    int n = f->traj->size();
    float x = f->pos.x;
    float y = f->pos.y;
    
    glLineWidth(5.0);
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_LINE_STRIP);
    glVertex2f(x, y);
    
    i = 0;
    for (std::list<Point2d>::iterator it=f->traj->begin(); it != f->traj->end(); ++it)
      {
	Point2d p = *it;
	float r = 1.0 - ((float) i / (float) n);
	float b = (float) i / (float) n;
	
	glColor3f(r, 0.0, b);
	glVertex2f(p.x, p.y);
	i++;
      }
    glEnd();
    
    float d = 0.02;
    glColor3f(1.0, 0.0, 0.0);
    glRectd(x-d, y-d, x+d, y+d);
    
    i = 0;
    for (std::list<Point2d>::iterator it=f->traj->begin(); it != f->traj->end(); ++it)
      {
	Point2d p = *it;
	float r = 1.0 - ((float) i / (float) n);
	float b = (float) i / (float) n;
	
	glColor3f(r, 0.0, b);
	glRectd(p.x-d, p.y-d, p.x+d, p.y+d);
	i++;
      }    
  }

  void ResourceDisplay()
  {
    if(resource_colors.empty())
      {
	std::vector<int> temp;
	for(int r=0; r<(int)environment->resourcetypes.size(); r++)
	  temp.push_back(r);
	while(!temp.empty())
	  {
	    int rand = (int)(env_uni()*temp.size());
	    resource_colors.push_back(temp[rand]);
	    temp.erase(temp.begin()+rand);
	  }
      }

    std::vector<Resource*> resources = VisibleResources();

    for(unsigned long r=0; r<resources.size(); r++)
      {
	Resource *resource = resources[r];
	if( environment->ResourceIsAvailable(resource) ) //if it is not depleted
	  {
	    float rel_id = 0;
	    //rel_id = (float) resource->type / (float) environment->resourcetypes.size();
	    rel_id = (float) resource_colors[resource->type] / (float) environment->resourcetypes.size();	    
	    if(treesdifferentcolors) glColor3f(0.0, rel_id, 1-rel_id);
	    else glColor3f(0.5, 0.0, 0.0);
	    DrawCircle(resource->position.x, resource->position.y, fooddisplaysize);
	  }   
      }
  }
  
  void PatchDisplay()
  {
    std::vector<Patch*> patches = VisiblePatches();
    for(unsigned long p=0; p<patches.size(); p++)
      {
	Patch *patch = patches[p];
	float rel_id = (float) patch->id / (float) environment->patches.size();
	if(treesdifferentcolors) glColor4f(0.0, rel_id, 1-rel_id, 0.1);
	else glColor4f(0.5, 0.0, 0.0, 0.1);
	DrawCircle(patch->position.x, patch->position.y, patch->radius);
      }   
  }

  std::vector<Resource*> VisibleResources()
  {
    float minx, miny, maxx, maxy;

    ScreenToWorldCoordinates(0, rheight, &minx, &miny);
    ScreenToWorldCoordinates(rwidth, 0, &maxx, &maxy);

    Rectangle r;

    r.min.x = minx;
    r.min.y = miny;
    r.max.x = maxx;
    r.max.y = maxy;
    
    std::vector<Resource*> resources = environment->ResourcesWithinGRID(minx-1, maxx+1, miny-1, maxy+1);    
    //std::vector<Resource*> resources = environment->ResourcesWithin(r);
    return resources;
  }

  std::vector<Patch*> VisiblePatches()
  {
        float minx, miny, maxx, maxy;

    ScreenToWorldCoordinates(0, rheight, &minx, &miny);
    ScreenToWorldCoordinates(rwidth, 0, &maxx, &maxy);

    Rectangle r;

    r.min.x = minx;
    r.min.y = miny;
    r.max.x = maxx;
    r.max.y = maxy;

    std::vector<Patch*> patches = environment->PatchesWithinGRID(r);
    //std::vector<Patch*> patches = environment->PatchesWithin(r);
    return patches;
  }

  ///////////////////////////////////////////////////////////////////
  /// MORE GENERAL DISPLAY OPTIONS

  void DrawSector(Point2d pos, Vec2d dir, float radius, float angle)
  {
    if(angle >= PI*2.0)
      {
	DrawCircle(pos.x, pos.y, radius);
      }
    else
      {
	DrawSector(pos.x, pos.y, radius, dir.angle()-angle/2.0, angle);
      }
  }

  /*
   * This code is an adaptation of the DrawArc() function
   * by SiegeLord's Abode
   * http://slabode.exofire.net/circle_draw.shtml
   */
  void DrawSector(float cx, float cy, float r, float start_angle, float arc_angle)
  {
    int num_segments = 30 * sqrtf(r);
    float theta = arc_angle / float(num_segments - 1);//theta is now calculated from the arc angle instead, the - 1 bit comes from the fact that the arc is open
    float tangetial_factor = tanf(theta);
    float radial_factor = cosf(theta);
    
    float x = r * cosf(start_angle);//we now start at the start angle
    float y = r * sinf(start_angle);
    
    glPolygonMode(GL_FRONT, GL_FILL);
    glBegin(GL_POLYGON);
    glVertex2f(cx, cy);
    for(int ii = 0; ii < num_segments; ii++)
      {
	glVertex2f(x + cx, y + cy);
	
	float tx = -y;
	float ty = x;
	
	x += tx * tangetial_factor;
	y += ty * tangetial_factor;
	
	x *= radial_factor;
	y *= radial_factor;
      }
    glVertex2f(cx, cy);
    glEnd();
  }
  
  /*
   * This code is an adaptation of the DrawCircle() function
   * by SiegeLord's Abode
   * http://slabode.exofire.net/circle_draw.shtml
   */
  void DrawCircle(float cx, float cy, float r)
  {
    int num_segments = 30 * sqrtf(r);
    float theta = 2 * 3.1415926 / float(num_segments);
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;
    
    float x = r;//we start at angle = 0
    float y = 0;
    
    glPolygonMode(GL_FRONT, GL_FILL);
    glBegin(GL_POLYGON);
    for(int ii = 0; ii < num_segments; ii++)
      {
	glVertex2f(x + cx, y + cy);//output vertex
	
	//apply the rotation matrix
	t = x;
	x = c * x - s * y;
	y = s * t + c * y;
      }
    glEnd();
  }
  
  ///////////////////////////////////////////////////////////////
  /// GENERAL GLUT FUNCTIONS FOR RUNNING THE GRAPHICS
  
  
  void init(Config c) { //graphics.c
    fooddisplaysize = c.get<float>("fooddisplaysize");
    displayfood = c.get<bool>("displayfood"); //NOT USED
    treesdifferentcolors = c.get<bool>("treesdifferentcolors");
    
    int argc = 0;
    char** argv = 0;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(0, 0);
    glutInitWindowSize(800,800);
    glutCreateWindow("Aapjes");
    InitGraph();
    
    // enable alpha channel (transparency)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    
    glutReshapeFunc(MyReshape);
    glutKeyboardFunc(MyKeys);
    glutSpecialFunc(MySpecialKeys);
    glutMouseFunc(MyMice);
    glutDisplayFunc(ShowIt);          //displays visual output
    
    for(int i = 0; i < 10; i++)
      {
	Closer();
      }
  }

  void InitGraph(void)
  {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glShadeModel(GL_FLAT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    transx = -0.5;
    transy = -0.5;
    glTranslatef(transx*(GLfloat)XFIELD,transy*(GLfloat)YFIELD,0.0);
  }

  //////////////////////////////////////////////////////////////////
  /// GLUT FUNCTIONS TO MAKE GRAPHICS INTERACTIVE + SAVING IMAGES

  void ScreenToWorldCoordinates(int screenx, int screeny, float *worldx, float *worldy)
  {
    GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX, winY, winZ;
    GLdouble posX, posY, posZ;

    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );

    winX = (float)screenx;
    winY = (float)viewport[3] - (float)screeny;
    glReadPixels( screenx, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

    *worldx = (float) posX;
    *worldy = (float) posY;
  }

  void Closer()
  {
    factor*=(100./(100.+increase));
    GLfloat s = 1+increase/100.;
    scale*=s;
    glMatrixMode(GL_PROJECTION);
    glScalef(s,s,1.0);
    glMatrixMode(GL_MODELVIEW);
    glutPostRedisplay();
  }
  
  
  void Further()
  {
    factor*=(1+increase/100.);
    GLfloat s = 100./(100.+increase);
    scale*=s;
    glMatrixMode(GL_PROJECTION);
    glScalef(s, s, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glutPostRedisplay();
  }

  void Movie()
  {

    if(movie_toggle==false)
      {
	movie_toggle = true;
	movie_starttime=0; movie_stoptime=0;
	
	//should ask for start time (default now)
	std::cout<<"Starting movie. Enter start time (time now: "<<environment->global_time<<")\n";
	//int starttime;
	bool incorrect=true;
	while(incorrect)
	  {
	    std::cin >> movie_starttime;
	    if(movie_starttime>=environment->global_time && movie_starttime<environment->SIMULATION_TIME) incorrect=false;
	    else
	      {
		std::cout<<"Start time out of range!\n";	  
		std::cout<<"Enter starttime (more than "<<environment->global_time<<"; less than "<<environment->SIMULATION_TIME<<"):\n";
	      }
	  }
	std::cout<<"Starttime chosen: "<<movie_starttime<<"\n";
	
	//should ask for stop time
	std::cout<<"Enter stop time (>: "<<movie_starttime<<")\n";
	//int stoptime;
	incorrect=true;
	while(incorrect)
	  {
	    std::cin >> movie_stoptime;
	    if(movie_stoptime>movie_starttime && movie_stoptime<environment->SIMULATION_TIME) incorrect=false;
	    else
	      {
		std::cout<<"Stop time out of range!\n";	  
		std::cout<<"Enter stoptime (more than "<<movie_starttime<<"; less than "<<environment->SIMULATION_TIME<<"):\n";
	      }
	  }
	std::cout<<"Stoptime chosen: "<<movie_stoptime<<"\n";
      }
    else
      {
	if(movie_starttime<=environment->global_time)
	  {
	    std::cout<<" PNG produced at time "<<environment->global_time<<" stop time "<<movie_stoptime<<std::endl;
	    PNGImage();
	  }
	if(movie_stoptime<=environment->global_time)
	  {
	    movie_toggle=false;	    
	  }
      }
    //should run PNGSave for each time step
  }
  
  void MyKeys(unsigned char key, int x, int y)
  {
    // so far we don't use x and y
    // this silences compiler warnings
    (void)x;
    (void)y;
    
    //Aapje* f = dynamic_cast<Aapje*>(focal);
    switch (key)
      {
      case ' ' : running = true; break;
      case 's' : stepwise = !stepwise; running = true; break;
      case 'b' : Reset(); break;
      case 'f' : ToggleFollow(); break;
      case 'i' : SetFollowTarget(); break;
      case 'P' : PNGImage(); break;
      case 'M' : Movie(); break;
      case 'u' : updating =!updating; break;
      case 'q' : Quit(); break;
      case '\33' : Quit(); break; //ESC
      case '-' : Further(); break;
      case '+' : Closer(); break;
      case ',' : delay = std::max(0,delay-100); break;
      case '.' : delay = std::min(999,delay+100); break;
      case 'z' : MyReshape(rwidth+10, rheight+10); break;
      }
  }
  
  void ToggleFollow()
  {
    follow = !follow;
    if(follow) std::cout<<"Now following individual "<<follow_target<<"\n";
    else std::cout<<"No longer following individual "<<follow_target<<"\n";
  }

  void SetFollowTarget()
  {
    int max =population.size();
    std::cout<<"Now following: "<<follow_target<<"\n";
    std::cout<<"Enter number of ID (more than 0; less than "<<max<<") of individual to follow:\n";
    int ind;
    bool incorrect=true;
    while(incorrect)
      {
	std::cin >> ind;
	if(ind>=0 && ind<max) incorrect=false;
	else
	  {
	    std::cout<<"Number out of range!\n";	  
	    std::cout<<"Enter number of ID (more than 0; less than "<<max<<") of individual to follow:\n";
	  }
      }
    std::cout<<"Ind chosen: "<<ind<<"\n";
    follow_target = ind;
  }
  
  void MyMice(int button, int state, int x, int y)
  {
    float wx, wy;
    
    ScreenToWorldCoordinates(x, y, &wx, &wy);
    
    std::cout << wx << "/" << wy<<std::endl;
    
    switch (button)
      {
      case GLUT_LEFT_BUTTON:
	if (state == GLUT_DOWN)
	  {
	  }
	break;
      case GLUT_RIGHT_BUTTON:
	if (state == GLUT_DOWN)
	  {
	  }
	break;
      default:
	break;
      }
  }
  
  void MyReshape(int w, int h)
  {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w*YFIELD <= h*XFIELD)
      glOrtho (-0.5*XFIELD, 0.5*XFIELD, -0.5*XFIELD*(GLfloat)h/(GLfloat)w,
	       0.5*XFIELD*(GLfloat)h/(GLfloat)w, -1.0, 1.0);
    else
      glOrtho (-0.5*YFIELD*(GLfloat)w/(GLfloat)h, 0.5*YFIELD*(GLfloat)w/(GLfloat)h, -0.5*YFIELD, 0.5*YFIELD, -1.0, 1.0);
    
    glScalef(scale, scale, 1.0);
    glMatrixMode(GL_MODELVIEW);
    rwidth=w;
    rheight=h;
    //factor=increase;
  }
  
  void MySpecialKeys(int key, int x, int y)
  {
    // so far we don't use x and y
    // this silences compiler warnings
    (void)x;
    (void)y;
    
    switch (key)
      {
      case GLUT_KEY_LEFT : Left(); break;
      case GLUT_KEY_RIGHT : Right(); break;
      case GLUT_KEY_UP : Up(); break;
      case GLUT_KEY_DOWN : Down(); break;
      }
  }
  
  void PNGImage()
  {
    char a_string[250];
    char another_string[400];
    
    opname= true;
    
    sprintf(a_string,"screenshot.%lu.png", environment->global_time);
    sprintf(another_string,"\\rm -f %s",a_string);
    int ret = system(another_string);
    
    if(ret==0)
      {
	glutPopWindow();
	ShowIt();
	glFinish();
	SavePNGImage(a_string);
	printf("Saved PNG image: %s\n",a_string);
      }
    else
      {
	printf("Did not save PNG image: %s\n",a_string);
      }
    
    opname=false;
  }
  
  void SavePNGImage (char *imagename)
  {
    FILE *PNGFileP;
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytep row_pointer;
    unsigned char *data;
    int row;
    
    PNGFileP = fopen(imagename, "wb");
    /*check memory on heap*/
    if((data = (unsigned char *) malloc (12*rwidth*rheight*sizeof(unsigned char)))==NULL)
      {
	printf("memory problem\n");
	exit(1);
      }
    
    if(rwidth%2) /* moet even zijn */
      rwidth-=1;
    if(rheight%2)
      rheight-=1;
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glReadPixels(0,0, rwidth, rheight, GL_RGB, GL_UNSIGNED_BYTE, data);
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
				      (png_voidp) NULL, (png_error_ptr) NULL, (png_error_ptr) NULL );
    info_ptr = png_create_info_struct ( png_ptr );
    png_init_io ( png_ptr, PNGFileP );
    png_set_IHDR ( png_ptr, info_ptr, rwidth, rheight,
		   8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
		   PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE );
    
    /* write header */
    png_write_info ( png_ptr, info_ptr );
    
    /* write out image, one row at a time */
    for ( row = rheight-1; row >= 0; row-- )
      {
	row_pointer = ( data + rwidth * row * 3 );
	png_write_rows ( png_ptr, &row_pointer, 1 );
      }
    
    png_write_end ( png_ptr, info_ptr );
    fflush ( PNGFileP );
    png_destroy_write_struct ( &png_ptr, (png_infopp)NULL );
    fclose(PNGFileP);
    free (data);
  }
   
  void Up()
  {
    transy += (-factor/100.*XFIELD);
    glTranslatef(0.0,-factor/100.*YFIELD,0.0);
    glutPostRedisplay();
    follow = false;
  }
  
  void Down()
  {
    transy += (factor/100.*XFIELD);
    glTranslatef(0.0,factor/100.*YFIELD,0.0);
    glutPostRedisplay();
    follow = false;
  }

  void Right()
  {
    transx += (-factor/100.*XFIELD);
    glTranslatef(-factor/100.*XFIELD,0.0,0.0);
    glutPostRedisplay();
    follow = false;
  }
  
  void Left()
  {
    transx += (factor/100.*XFIELD);
    glTranslatef(factor/100.*XFIELD,0.0,0.0);
    glutPostRedisplay();
    follow = false;
  }

  void Reset()
  {
    glLoadIdentity();
    transx = -0.5;
    transy = -0.5;
    glTranslatef(transx*(GLfloat)XFIELD,transy*(GLfloat)YFIELD,0.0);
    glutPostRedisplay();
  }
  
  void Quit()
  {
    printf("********************************\n");
    printf("*   PROGRAM TERMINATED [ESC}   *\n");
    printf("********************************\n");
    exit(0);
  }
  
  void update() //graphics.c
  {
    if (!opname && updating)
      glutPostRedisplay();

    if (movie_toggle==true)
      Movie();
    
    glutMainLoopEvent();
  }


} //END NAMESPACE
