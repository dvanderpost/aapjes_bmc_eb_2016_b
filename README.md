# README #

### What is this repository for? ###

This repository is a documentation of the development of the program 'aapjes' starting in 2013 in the Laland Lab at St Andrews University. Originally this development took place in a different repository mixed with other programs. The commits of the different model versions were then copied to this repository to preserve the documentation of development, and to create an independent repository.

### GNU license ###

Program name: aapjes
Copyright 2016 Daniel J. van der Post

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You will find a copy of the GNU General Public License in this repository (GNU_lisence_v3.txt). If not, see http://www.gnu.org/licenses/.

### MODEL DESCRIPTION ###
Please see: van der Post et al. (2016) The evolution of social learning mechanisms and cultural phenomena in group foragers. BMC Evolutionary Biology

### How do I get set up? ###

* Requirements: this depends on the size of the environment but using the parameter files below would require 2.2-2.3 GB of RAM.
* Compiling in Ubuntu: make
* Running program: ./aapjes[_v2015.16] parameter.cfg randomseedENV randomseedIND > output.file
* Parameter.cfg: this is a file that specifies the parameter values
* randomseedENV: this is a value that seeds the random number generator used for environmental features
* randomseedIND: this is value that seeds the random number generator used for environmental features

### Contribution guidelines ###

I am happy for others to develop and use this code as they wish. Please contact me if you are interested.

### Who do I talk to? ###

Repo owner or admin

# Contents #

### Code and Versions ###

* Source code and makefiles: in main directory
* Versions: sub-directory with all executable versions of model
* geom: various libraries for vector calculations etc
* rstartree: libraries for storing memory in r-star-trees (for compatibility with older models, not used here)

### Shell scripts ###

* run10_script_diff_env.sh: uses tsp to queue and run 10 simulations with different random seeds for both individuals and environment using particular version of model and parameter file.
* run10_script_same_env.sh: uses tsp to queue and run 10 simulations with different random seeds for only individuals (same environment seed in each case) using particular version of model and parameter file.
* runN_script_qualAvg.sh: runs diet_weighted_avg_quality.py on raw data of 10 simulations generating files ending with _QA.dat
* runN_script_qualAvg_diff_env.sh: runs diet_weighted_avg_quality.py on raw data of 10 simulations generating files ending with _QA.dat

### Python scripts ###

* diet_weighted_avg_quality.py: calculates various repertoire statistics, used by runN_script_qualAvg.sh
* traditions_v2015.1.py: calculates various measures about between group similarity in repertoires over time, as well as within and between group similarity.

### R scripts ###

These R scripts show which kind data were used for different figures (the 'simulations' section below shows the corresponding kinds of simulation):

* Fig 2A: trad_vs_H_EC5.R
* Fig 2B: CC_vs_H_EC5.R

* Fig 3A: BG_vs_H_EC5.R
* Fig 3B: WG_vs_H_EC5.R
* Fig 3C: RQ_vs_H_EC5.R
* Fig 3D: ccSK_ccRQ_ccDD_vs_OL_EC5.R

### Parameter files ###

* Simulations

# Simulations #

We do not include the any data files, but we include the parameter files used to run the simulations and which model version was used. Most files (.par) can be used directly as input for model, while other (.txt) need to be converted to appropriate format (i.e. .par format). The parameters files are in directories of the corresponding simulation type:

### Fig 2 ###

* Fig 2A: Simulations: H0.1,H1,H5,H10 EC0 Gr_LE, Gr_SE, Gr_OL (Parfiles: NEVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H0.1/1/5/10_0.1/1/5/10_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0/1_SSL0/1_EC1_5yr_v2015.16.par, run with run10_script_same_env.sh)
* Fig 2B: Simulations: H0.1,H1,H5,H10 EC0 Gr_LE, Gr_SE, Gr_OL (Parfiles: same as Fig 2A, run with run10_script_diff_env.sh)

### Fig 3 ###

* Fig 3A: same as Fig 2A
* Fig 3B: same as Fig 2A
* Fig 3C: same as Fig 2B
* Fig 3D: same as Fig 2B

