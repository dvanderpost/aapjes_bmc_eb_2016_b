/*
  Copyright (C) 2010 by The Regents of the University of California

  Redistribution of this file is permitted under
  the terms of the BSD license.

  Date: 11/01/2009
  Author: Sattam Alsubaiee <salsubai (at) ics.uci.edu>
  Shengyue Ji <shengyuj (at) ics.uci.edu>
*/

#include "util.h"

bool Object::operator==(const Object &o) const
{
  return id == o.id && mbr == o.mbr;
}

bool NodeMinDist2::operator<(const NodeMinDist2 &n2) const
{
  return minDist2 > n2.minDist2;
}

bool CenterDistance::operator<(const CenterDistance &d) const
{
  return distance > d.distance;
}

bool EntryValue::operator<(const EntryValue &e) const
{
  return value < e.value;
}
