/*
  Copyright (C) 2010 by The Regents of the University of California

  Redistribution of this file is permitted under
  the terms of the BSD license.

  Date: 11/01/2009
  Author: Sattam Alsubaiee <salsubai (at) ics.uci.edu>
  Shengyue Ji <shengyuj (at) ics.uci.edu>
*/

#ifndef _UTIL_H_
#define _UTIL_H_

#include <vector>
#include <stdint.h>
#include "rectangle.h"

class Object
{
 public:
  // the object/node id
  uintptr_t id;

  // the rectangle that represents the object/node
  Rectangle mbr;

  bool operator==(const Object &o) const;
};

class NodeMinDist2
{
 public:
  unsigned id;
  double minDist2;
  const std::vector<char> *flags;

  bool operator<(const NodeMinDist2 &n2) const;
};

class CenterDistance
{
 public:
  Object object;
  double distance;

  bool operator<(const CenterDistance &d) const;
};

class EntryValue
{
 public:
  Object object;
  double value;
  unsigned index;

  bool operator<(const EntryValue &e) const;
};

#endif
