#ifndef _INDIVIDUAL_H_GUARD_
#define _INDIVIDUAL_H_GUARD_

#include <vector>

class Individual {
 private:
  static int instances;
  int id;
  float weight; //in kg
 protected:
  float x, y;
 public:
  Individual(float x, float y);
  int r_id();
  float r_x();
  float r_y();
  float r_weight();
  virtual void step() = 0;
};

#endif // _INDIVIDUAL_H_GUARD_
