#!/usr/bin/python
#RUN with: python pythonscripts/traditions_v2015.1.py directory_path/ROOT_OF_FILENAME
#ROOT_OF_FILENAME = FILENAME WITHOUT '_isX_rsX.dat'

import string, random
from string import split
import sys
import os
import numpy
from numpy import array
from numpy import in1d
from scipy.spatial import distance
import math

##################
### PARAMETERS ###
##################
MIN_AGE = 2.0
STARTTIME=120.0
FILENAME_ROOT = sys.argv[1] #file to be read in (i.e. output file of evolutionary simulation)
ENV_RS = sys.argv[2]        #randomseed of environment

############################################################################

###     FUNCTIONS    #######################################################

############################################################################

###################
### For random filenames ###
###################
def randomstr(length):
   return ''.join(random.choice(string.lowercase) for i in range(length))

##################
###  READ IN FILE ###
##################
#Used for reading in GSE.dat as list because of irregular line lengths
def read_in_file(file_object):
	file_data=[]
	for l in file_object:
		file_data.append(l)
	return file_data

##################
###  GET GSE DATA ###
##################
def get_gselist(FILENAME):
	temp_file = randomstr(5)+'.dat'
	SHELL_COMMAND = "grep '^GSE' " + FILENAME + " | cut -d' ' -f2- | sort -n -r -k 1 > "+temp_file
	os.system(SHELL_COMMAND)
	gseinfile = file( temp_file )       
	gselist = read_in_file(gseinfile)   #0=time 1=gr_id 2=gr_size 3-end=group members; Irregular line lengths!
	SHELL_COMMAND = "rm -f "+temp_file
	os.system(SHELL_COMMAND)
	return gselist

def get_gsedata(gselist):
	gsedata = numpy.zeros(shape=(len(gselist),23)) #convert to array with zeroes for missing members if GS < 20
	for i in range(0,len(gselist)):
		l = gselist[i].split()
		for j in range(0,len(l)):
			gsedata[i,j] = float(l[j])
	return gsedata

##################
###  GET AGE DATA ###
##################
def get_agedata(FILENAME,MAIN):
	temp_file = randomstr(5)+'.dat'
	if MAIN:
		SHELL_COMMAND = "grep '^A ' " + FILENAME + " | cut -d' ' -f2,3,4 | sort -n -r -k 1 > "+temp_file
	else:
		SHELL_COMMAND = "tail -n 400 " + FILENAME2 + " | grep '^A' | cut -d' ' -f2,3,4 | sort -n -r -k 1 > "+temp_file
	os.system(SHELL_COMMAND)
	agefile = file( temp_file ) #0=time 1=ind_id 2=age
	agedata = numpy.loadtxt(agefile)
	SHELL_COMMAND = "rm -f "+temp_file
	os.system(SHELL_COMMAND)
	return agedata

##################
###  GET DIET DATA ###
##################
def get_dietdata(FILENAME, MAIN):
	temp_file = randomstr(5)+'.dat'
	if MAIN:
		SHELL_COMMAND = "grep '^D' " + FILENAME + " | cut -d' ' -f2- | sort -n -r -k 1 > "+temp_file
	else:
		SHELL_COMMAND = "tail -n 400 " + FILENAME2 + " | grep '^D' | cut -d' ' -f2- | sort -n -r -k 1 > "+temp_file
	os.system(SHELL_COMMAND)
	dietfile = file( temp_file )  #0=time 1=id 2= 3= 4:end amount eaten
	dietdata = numpy.loadtxt(dietfile)
	SHELL_COMMAND = "rm -f "+temp_file
	os.system(SHELL_COMMAND)
	return dietdata

##################
###  GET QUAL DATA ###
##################
def get_qual_data(FILENAME):
	temp_file1 = randomstr(5)+'.dat'
	temp_file2 = randomstr(5)+'.dat'
	#1=Q 2-251=resourceQ
	SHELL_COMMAND = "grep '^Q ' " + FILENAME + " | cut -d' ' -f2- | sort -n -r -k 1 > "+temp_file1
	#1=QN 2=time 3-252=resourceQ
	SHELL_COMMAND2 = "grep '^QN' " + FILENAME + " | cut -d' ' -f2- | sort -n -r -k 1 > "+temp_file2
	os.system(SHELL_COMMAND)
	qfile = file( temp_file1 )  #0=time 1=id 2= 3= 4:end amount eaten
	qdata = numpy.loadtxt(qfile)
	qdata = numpy.insert(qdata, 0, 0.0)

	os.system(SHELL_COMMAND2)
	qfile2 = file( temp_file2 )  #0=time 1=id 2= 3= 4:end amount eateno
	if os.stat(temp_file2).st_size != 0: #if not an empty file!
		qdata2 = numpy.loadtxt(qfile2)
		qdata2 = numpy.vstack([qdata2, qdata]) #concatenate Q and QN lines
	else:
		qdata2 = qdata #otherwise stick to Q lines only
	SHELL_COMMAND = "rm -f "+temp_file1
	os.system(SHELL_COMMAND)
	SHELL_COMMAND = "rm -f "+temp_file2
	os.system(SHELL_COMMAND)
	return qdata2 #First column=time, other columns are resources

##################
###  CHECK ENV SIM ###
##################
def check_env_sim(FILENAME, FILENAME2):
	temp_file1 = randomstr(5)+'.dat'
	temp_file2 = randomstr(5)+'.dat'
	SHELL_COMMAND = "grep '^Q ' " + FILENAME + " | cut -d' ' -f2- | sort -n -r -k 1 >"+temp_file1
	os.system(SHELL_COMMAND)
	qfile = file( temp_file1 )  #0=time 1=id 2= 3= 4:end amount eaten
	q1 = numpy.loadtxt(qfile)
	SHELL_COMMAND = "grep '^Q ' " + FILENAME2 + " | cut -d' ' -f2- | sort -n -r -k 1 >"+temp_file2
	os.system(SHELL_COMMAND)
	qfile = file( temp_file2 )  #0=time 1=id 2= 3= 4:end amount eaten
	q2 = numpy.loadtxt(qfile)
	SHELL_COMMAND = "rm -f "+temp_file1
	os.system(SHELL_COMMAND)
	SHELL_COMMAND = "rm -f "+temp_file2
	os.system(SHELL_COMMAND)
	if not numpy.array_equal(q1,q2):
		print 'ERROR: the environments are not exactly the same!'
		print 'Exiting program!'
		sys.exit()

##################
###  CHECK STARTTIME ###
##################
def check_starttime(STARTTIME, agedata):
	maxtime = max(agedata[:,0])
	if STARTTIME > maxtime:
		print 'ERROR: STARTTIME > maxtime in data! Exiting now'
		sys.exit()


##################
###  GET GROUP LINEAGES ###
##################
def get_group_lineages(lin_ids, gselist, gsedata):
	times = sorted(numpy.unique(gsedata[:,0]), reverse=True) #get all time points
	LENGTH = len(lin_ids)*len(times)                         #total number of lineages * total number of groups
	grlindata = numpy.zeros(shape=(LENGTH,3))
	for g in range(0, len(lin_ids)): #for each group
		gr = lin_ids[g]     #define starting focal group
		PREV_TIME=0
		for t in range(0,len(times)):
			sub = gsedata[(gsedata[:,0]==times[t]) & (gsedata[:,1]==gr),] #get data for group at time t
			if sub.shape[0]==0: # no groups found: find ancestor
				#get group members at previous time point
				pmems = gsedata[(gsedata[:,0]==PREV_TIME) & (gsedata[:,1]==gr),3:]
				if pmems.shape[0]==0: #no group members found of previous time point
					print 'ERROR (get_group_lineages): previous time point with group members not found!'
				else: #if members found at previous time point
					pmems = numpy.reshape(pmems, pmems.shape[1])
					pmems = pmems[pmems>0]

					#get all groups at present time point (will not include focal group gr)
					tsub = gsedata[(gsedata[:,0]==times[t]),]
					groups = tsub[:,1]
					MATCHES=False
					for g2 in range(0, len(groups)): #for each group get members + check matches
						mems = tsub[tsub[:,1]==groups[g2],3:]
						mems = numpy.reshape(mems, mems.shape[1])
						mems = mems[mems>0]
						matches = set(mems).intersection(pmems) 
						if matches: # matches found: hence this is ancestor: update gr, store data, continue
							gr = groups[g2]
							grlindata[(g*len(times))+t,0] = lin_ids[g]
							grlindata[(g*len(times))+t,1] = times[t]
							grlindata[(g*len(times))+t,2] = gr
							PREV_TIME=times[t] #when group is found
							MATCHES=True
							break
					if not MATCHES:
						#if reach here no matches were found (no break), hence:
						grlindata[(g*len(times))+t,0] = lin_ids[g]
						grlindata[(g*len(times))+t,1] = times[t]
						grlindata[(g*len(times))+t,2] = 'NaN'	

			else: #group is found: store data, continue
				grlindata[(g*len(times))+t,0] = lin_ids[g]
				grlindata[(g*len(times))+t,1] = times[t]
				grlindata[(g*len(times))+t,2] = gr
				PREV_TIME=times[t] #when group is found
	return grlindata

##################
###  GET LCA GROUP ###
##################
def get_last_common_anc(group1, group2, lindata):
	#grlindata: 0) lineage; 1) time; 2) group
	sub1 = lindata[lindata[:,2]==group1]          #select data of group (to get lineage number)
	lineage1 = min( group1, max(sub1[:,0]) )
	lin1 = lindata[lindata[:,0]==lineage1]  #select data of lneage
	sub2 = lindata[lindata[:,2]==group2]          #select data of group (to get lineage number)
	lineage2 = min( group2, max(sub2[:,0]) )
	lin2 = lindata[lindata[:,0]==lineage2]  #select data of lneage
	matches = set(lin1[:,2]).intersection(lin2[:,2]) #get shared groups in ANC trace

	if matches:
		if group1 < 10 or group2 < 10:
			lca = min(group1, group2)
		else:
			lca = max(matches)                               #get last common group
		time = min(max(lin1[lin1[:,2]==lca,1]),max(lin2[lin2[:,2]==lca,1])) #min of last time in lin1 & 2
		return [lca, time]
	else:
		return ['NA','NA']


##################
###  GET GROUP MEMBERS ###
##################
def get_group_members(group, time, gsedata):
	#gsedata 0) time 1) gr id 2) gr size 3-end) members
	sub1 = gsedata[(gsedata[:,0]==time) & (gsedata[:,1]==group),]  #get time & group subset
	sub1 = numpy.reshape(sub1,sub1.shape[1])                         #reshape to 1D array
	sub1 = sub1[sub1>0,]                                             #chop off 'empty' members
	return sub1[3:len(sub1)]                                        #select members

##################
###  SELECT MEMBERS THAT ARE OLD ENOUGH ###
##################
def select_members_old_enough(members, time, agedata, MIN_AGE):
	#agedata 0) time 1) id 2) age
	sub = agedata[agedata[:,0]==time,]        #select agedata at time point
	sub = sub[numpy.in1d(sub[:,1], members)]  #select agedata of members at that time point
	return sub[sub[:,2]>=MIN_AGE,1]           #select members older than MIN_AGE

##################
###  GET GROUP DIET ###
##################
def get_group_diet(members,time,dietdata):
	sub1 = dietdata[dietdata[:,0]==time,]      #select dietdata at time point
	sub1 = sub1[numpy.in1d(sub1[:,1], members)]  #select dietdata of members at time point
	return sub1                          #select diet scores of members

##################
###  GET PAIRWISE COSINE DISTANCES ###
##################
def cos_cdist(matrix1, matrix2):
	return numpy.mean(1- distance.cdist(matrix1, matrix2, 'cosine'))

def WG_cos_cdist(dietdata1, dietdata2):
	tot=0
	count=0
	for i in range(0,dietdata1.shape[0]):
		for j in range(0,dietdata2.shape[0]):
			if i!=j:				
				if dietdata1[i,4:].size>2 and dietdata2[j,4:].size>2: #exclude if too short vectors
					if sum(dietdata1[i,4:])>0 and sum(dietdata2[j,4:])>0: #exclude if empty
						tot += 1 - distance.cosine(dietdata1[i,4:], dietdata2[j,4:])
						diff = 1 - distance.cosine(dietdata1[i,4:], dietdata2[j,4:])
						if numpy.isnan(diff):
							print 'ERROR in WG_cos_cdist'
							print 'Dietdata1 ', dietdata1[i,4:]
							print 'Dietdata2 ', dietdata2[j,4:]
							print 'Diff ', diff
							sys.exit()
						count += 1
	if count>0:
		return tot/count
	else:
		return 'NA'

def BG_cos_cdist(dietdata1, dietdata2, shared, shared_flag, g1_members, g2_members):
	if shared_flag and shared.size<5: #checking if a vector
		return 'NA'
	if numpy.sum(dietdata1[:,4:])<=0: #checking if a vector
		return 'NA'
	if numpy.sum(dietdata2[:,4:])<=0: #checking if a vector
		return 'NA'
	if g1_members.size<=0:            #checking if members
		return 'NA'
	if g2_members.size<=0:            #checking if members
		return 'NA'

	if numpy.isnan(1 - distance.cosine( numpy.sum(dietdata1[:,4:], axis=0), numpy.sum(dietdata2[:,4:], axis=0) )):
		print 'ERROR in BG_cos_cdist'
		print 'Dietdata1 ', dietdata1[:,4:]
		print 'Dietdata2 ', dietdata2[:,4:]
		print 'Shared flag ', shared_flag
		print 'Shared ', shared		
		sys.exit()

	return 1 - distance.cosine( numpy.sum(dietdata1[:,4:], axis=0), numpy.sum(dietdata2[:,4:], axis=0) )

#############################
### GET BETWEEN GROUP BETWEEN SIM SIMILARITY = BASELINE FOR NON-RELATED GROUPS ####
##############################
def between_groups_between_sims(STARTTIME, MIN_AGE, glinlist, glinlist2, gsedata, gsedata2, agedata, agedata2, dietdata, dietdata2, outfile):
	TIME=STARTTIME
	for g1 in range(0, len(glinlist)):
		g1_members = get_group_members(glinlist[g1],TIME,gsedata)
		g1_members = select_members_old_enough(g1_members, TIME, agedata, MIN_AGE)
		g1_diet = get_group_diet(g1_members, TIME, dietdata)
		for g2 in range(0, len(glinlist2)):
			g2_members = get_group_members(glinlist2[g2],TIME,gsedata2) 
			g2_members = select_members_old_enough(g2_members, TIME, agedata2, MIN_AGE)
			if len(g1_members)>0 and len(g2_members)>0:
				g2_diet = get_group_diet(g2_members, TIME, dietdata2) 
				overlap_g1_g2 = cos_cdist(g1_diet[:,4:], g2_diet[:,4:])
				overlap_avg_g1_g2 = 1 - distance.cosine( numpy.sum(g1_diet[:,4:], axis=0), numpy.sum(g2_diet[:,4:], axis=0) )				
			else:
				overlap_g1_g2 = 'NA'
				overlap_avg_g1_g2 = 'NA'
			#1=BetweenGroupBetweenSim, 2=Time, 3=Sim1, 4=Sim2, 5=Group1, 6=Group2, 7=Group size 1, 8=Group size 2, 9=Overlap
			LINE = 'BGBS '+' '+str(TIME)+' '+str(SIM1)+' '+str(SIM2)+' '+str(glinlist[g1])+' '+str(glinlist2[g2])+' '+str(len(g1_members))+' '+str(len(g2_members))+' '+str(overlap_g1_g2)+' '+str(overlap_avg_g1_g2)+'\n'
			outfile.write(LINE)
			#print 'BGBS', TIME, SIM1, SIM2, glinlist[g1], glinlist2[g2], len(g1_members), len(g2_members), overlap_g1_g2, overlap_avg_g1_g2 
			#raw_input()

##########################
### GET BETWEEN GROUP WITHIN SIM SIMILARITY = BASELINE FOR POTENTIALLY RELATED GROUPS ###
##########################
def between_groups_within_sims(STARTTIME, MIN_AGE, glinlist, gsedata, agedata, dietdata, outfile):
	TIME=STARTTIME
	for g1 in range(0, len(glinlist)-1):
		g1_members = get_group_members(glinlist[g1],TIME,gsedata)
		g1_members = select_members_old_enough(g1_members, TIME, agedata, MIN_AGE)
		g1_diet = get_group_diet(g1_members, TIME, dietdata)
		for g2 in range(g1+1, len(glinlist)):
			g2_members = get_group_members(glinlist[g2],TIME,gsedata) 
			g2_members = select_members_old_enough(g2_members, TIME, agedata, MIN_AGE) 
			lca_and_time = get_last_common_anc(glinlist[g1], glinlist[g2], grlindata)
			if len(g1_members)>0 and len(g2_members)>0:
				g2_diet = get_group_diet(g2_members, TIME, dietdata) 
				overlap_g1_g2 = cos_cdist(g1_diet[:,4:], g2_diet[:,4:]) 
				overlap_avg_g1_g2 = 1 - distance.cosine( numpy.sum(g1_diet[:,4:], axis=0), numpy.sum(g2_diet[:,4:], axis=0) )			
			else:
				overlap_g1_g2 = 'NA'
				overlap_avg_g1_g2 = 'NA'
			#1=BetweenGroupWithinSim, 2=Time, 3=Sim1, 4=Group1, 5=Group2, 6=Group size 1, 7=Group size 2, 8=Overlap 9=time of LCA 10=overlap_g1_g2 
			LINE = 'BGWS '+str(TIME)+' '+str(SIM1)+' '+str(glinlist[g1])+' '+str(glinlist[g2])+' '+str(len(g1_members))+' '+str(len(g2_members))+' '+str(overlap_g1_g2)+' '+str(lca_and_time[1])+' '+str(overlap_avg_g1_g2)+'\n'
			outfile.write(LINE)
			#print 'BGWS', TIME, SIM1, glinlist[g1], glinlist[g2], len(g1_members), len(g2_members), overlap_g1_g2, lca_and_time[1], overlap_avg_g1_g2

#################################
### GET WITHIN GROUP SIMILARITY ACROSS TIME WITHIN LINEAGES (at time zero gives max potential for trad diffs) ####
#################################

def within_groups_across_time(STARTTIME, MIN_AGE, glinlist, gsedata, agedata, dietdata, outfile):
	TIME=int(STARTTIME)
	DIFF=STARTTIME-float(TIME)
	TIME1 = float(TIME) + DIFF
	ENV_CHANGE = False
	if qdata.size > qdata.shape[0]: #if is more than one row (i.e. the env has changed such that QN lines exist)      
		ENV_CHANGE = True
	if ENV_CHANGE:
		maxtime1 = max(qdata[qdata[:,0]<float(TIME1),0]) #select last time of qdata before present
		Q1 = qdata[qdata[:,0]==maxtime1,1:]
		if Q1.shape[0]>1: 
			Q1 = Q1[Q1.shape[0]-1,]
		else:
			Q1 = numpy.reshape(Q1,Q1.shape[1])        
	for g1 in range(0, len(glinlist)): 
		print (g1+1),' of ',len(glinlist),': Group ', glinlist[g1]
		g1_members = get_group_members(glinlist[g1],TIME1,gsedata)
		g1_members = select_members_old_enough(g1_members, TIME1, agedata, MIN_AGE)
		g1_diet = get_group_diet(g1_members, TIME1, dietdata)
		for t in range(0,(TIME-20)): #going backwards each full year
			TIME2 = TIME1-t
			# get corresponding group_id from anc trace: grlin
			gr2 = grlindata[(grlindata[:,0]==glinlist[g1]) & (grlindata[:,1]==TIME2),2]  #lineage & group

			if not numpy.isnan(gr2): #if group exists
				g2_members = get_group_members(gr2, TIME2, gsedata)
				g2_members = select_members_old_enough(g2_members, TIME2, agedata, MIN_AGE)
				g2_diet = get_group_diet(g2_members, TIME2, dietdata)

				if ENV_CHANGE:
					maxtime2 = max(qdata[qdata[:,0]<float(TIME2),0]) #select last time of qdata before present
					if maxtime1 > maxtime2: #if different qualities
						Q2 = qdata[qdata[:,0]==maxtime2,1:]  #get relevant qualities (removes time column)
						if Q2.shape[0]>1: #catches rare incident of two changes at once
							Q2 = Q2[Q2.shape[0]-1,]
						else:
							Q2 = numpy.reshape(Q2,Q2.shape[1])  
					else:
						Q2 = Q1

					diff = numpy.subtract(Q1,Q2)         #get diffs
					nonshared = numpy.argwhere(diff>0)+4 #get column indices where different
					nonshared = numpy.reshape(nonshared,nonshared.shape[0]) 
					shared = numpy.argwhere(diff==0)+4   #column indices where same (+4 is to align with diet matrix which has extra columns)
					shared = numpy.reshape(shared,shared.shape[0])
					shared = numpy.insert(shared,0,[0,1,2,3]) #add columns to align with diet array
					g2_diet_shared = g2_diet[:,shared]  #select agedata of members at that time point
					g1_diet_shared = g1_diet[:,shared]
					g1_diet_all = numpy.hstack( (g1_diet[:,shared], g1_diet[:,nonshared], numpy.zeros(shape=(g1_diet.shape[0],len(nonshared) ) ) ) ) 
					g2_diet_all = numpy.hstack( (g2_diet[:,shared], numpy.zeros(shape=(g2_diet.shape[0],len(nonshared) ) ), g2_diet[:,nonshared] ) ) #add non-shared resources as extra columns which are zeroed for the other group

				else: #not ENVCHANGE
					shared = numpy.arange(0,254,1)
					g2_diet_shared = g2_diet
					g1_diet_shared = g1_diet
					g1_diet_all = g1_diet_shared
					g2_diet_all = g2_diet_shared

				overlap_shared_only = WG_cos_cdist( g1_diet_shared, g2_diet_shared)
				overlap_all = WG_cos_cdist( g1_diet_all, g2_diet_all)
				overlap_avg_shared = BG_cos_cdist(g1_diet_shared, g2_diet_shared, shared, True, g1_members, g2_members)
				overlap_avg_all = BG_cos_cdist(g1_diet_all, g2_diet_all, shared, False, g1_members, g2_members)

				#1=WGWS, 2=Sim1m 3=group id at starttime, 4=group id at earlier time, 5=group size at starttime, 6=group size at earlier time, 7=earlier time, 8=overlap shared 9=overal all 10=overap avg shared 11=overall avg all
				LINE = 'WGWS '+str(SIM1)+' '+str(glinlist[g1])+' '+str(int(gr2))+' '+str(len(g1_members))+' '+str(len(g2_members))+' '+str(TIME2)+' '+str(overlap_shared_only)+' '+str(overlap_all)+' '+str(overlap_avg_shared)+' '+str(overlap_avg_all)+'\n'
				outfile.write(LINE)
				#print 'WGWS', SIM1, glinlist[g1], int(gr2), len(g1_members), len(g2_members), ((TIME+DIFF)-t), overlap_shared_only, overlap_all, overlap_avg_shared, overlap_avg_all

############################################################################

###     RUNNING SCRIPT    #######################################################

############################################################################

OUTFILE = FILENAME_ROOT+str(ENV_RS)+'_TRAD.dat'
outfile = open(OUTFILE, "w")

print 'Running traditions_v2015.1.py'
print 'Writing to:'
print OUTFILE

for s in range(1,11):
	
	print 'Simulation ',s

	#INITIALIZING	
	SIM1 = s
	FILENAME = FILENAME_ROOT+'_is'+str(SIM1)+'_rs'+str(ENV_RS)+'.dat'
	SIM2 = int(SIM1)+1 #NOTE: compares to next simulation in order
	if(SIM2>10):
		SIM2=1
	FILENAME2 = FILENAME_ROOT+'_is'+str(SIM2)+'_rs'+str(ENV_RS)+'.dat'

	#READING IN DATA
	print 'Reading in data'
	qdata = get_qual_data(FILENAME) #1st column = Time; 2-251=resource qualities
	gselist = get_gselist(FILENAME)
	gsedata = get_gsedata(gselist)
	gselist2 = get_gselist(FILENAME2)
	gsedata2 = get_gsedata(gselist2)
	agedata = get_agedata(FILENAME, True)
	agedata2 = get_agedata(FILENAME2, False)
	dietdata = get_dietdata(FILENAME, True)
	dietdata2 = get_dietdata(FILENAME2, False)

	#CHECK STARTTIME
	check_starttime(STARTTIME, agedata2)

	#CHECK ENVIRONMENTS ARE THE SAME
	check_env_sim(FILENAME, FILENAME2)

	#GET GROUPS AT STARTTIME TO GET LINEAGE_IDS FOR SIM1 and SIM2
	print 'Extracting groups and  lineages'
	glinlist = gsedata[gsedata[:,0]==STARTTIME,1]
	glinlist2 = gsedata2[gsedata2[:,0]==STARTTIME,1]

	#GET GROUP LINEAGES FOR SIM1
	grlindata = get_group_lineages(glinlist, gselist, gsedata)

	#GET BETWEEN GROUP BETWEEN SIM SIMILARITY = BASELINE FOR NON-RELATED GROUPS
	print 'Calculating between group between sim similarity'
	between_groups_between_sims(STARTTIME, MIN_AGE, glinlist, glinlist2, gsedata, gsedata2, agedata, agedata2, dietdata, dietdata2, outfile)
	#1=BGBSim, 2=Time, 3=Sim1, 4=Sim2, 5=Group1, 6=Group2, 7=Group size 1, 8=Group size 2, 9=Overlap

	#GET BETWEEN GROUP WITHIN SIM SIMILARITY = BASELINE FOR POTENTIALLY RELATED GROUPS
	print 'Calculating between group within sim similarity'
	between_groups_within_sims(STARTTIME, MIN_AGE, glinlist, gsedata, agedata, dietdata, outfile)
	#1=BGWS, 2=Time, 3=Sim1, 4=Group1, 5=Group2, 6=Group size 1, 7=Group size 2, 8=Overlap 9=time of LCA

	#GET WITHIN GROUP SIMILARITY ACROSS TIME WITHIN LINEAGES (at time zero gives max potential for trad diffs)
	print 'Calculating within group similarity over time'
	within_groups_across_time(STARTTIME, MIN_AGE, glinlist, gsedata, agedata, dietdata, outfile)
	#1=WGWS, 2=Sim1m 3=group id at starttime, 4=group id at earlier time, 5=group size at starttime, 6=group size at earlier time, 7=earlier time, 8=overlap

outfile.close()
