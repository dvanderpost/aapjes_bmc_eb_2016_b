////////////////// TODO LIST /////////////////////

//TODO: 
//3c - ENV CHANGE: sync appearance times in patch - for seasonal change

//31 - MOVEMENT: add noise to forward direction and minturn angle when aligning

//19 - MULTIPLE GROUPS WITH OPPOSING PREFS

//28 - GROUPING: draw new alignment direction from normal

//16 - CODE: make action complete after duration? (as alternative option)

//11 - CODE: population class / only read in general aap variables once
//12 - CODE: memory check (i.e. when too many resources)

//15 - CODE: make separate general graphics repo + one that is taylored for this model

//29 - CODE: something wrong with graphics plotting patches: only visible bottom left of field (but items are visible everywhere)
//32 - CODE: check other diffs with former diet model


//////////////// FUTURE ENHANCEMENTS /////////////////////
//  - GRAPHICS: displaying resource quality

//////////////// MAIN.CPP ////////////////////////////////

#include <iostream>
#include "environment.h"
#include "config.h"

//INITIALIZATION
Environment* InitializeEnvironment(Config c);
void StopSimulation(Environment *environment);

using namespace std;

int main(int argc, char *argv[])
{
  std::cout<<"Version: aapjes2015.17\n";


  //READ IN COMMAND LINE
  if(argc < 4)
    {
      std::cout << "Usage: " << std::endl
		<< "aapjes <config_file> <env_seed> <ind_seed>" << std::endl
		<< std::endl
		<< "<config_file> := path to config file" << std::endl
		<< "<env_seed>    := seed for environment RNG" << std::endl
		<< "<ind_seed>    := seed for individual RNG" << std::endl;
      return(1);
    }

  //READ IN CONFIG FILE
  Config c;
  c.read(argv[1]);
  c.print();

  //INITIALIZE RANDOM SEEDS
  long env_seed = atoi(argv[2]);
  long ind_seed = atoi(argv[3]);

  cout << "# env_seed = " << env_seed << endl
       << "# ind_seed = " << ind_seed << endl;

  //INITIALIZE RANDOM NUMBER GENERATORS
  env_rng.seed(static_cast<boost::uint32_t>(env_seed));
  ind_rng.seed(static_cast<boost::uint32_t>(ind_seed));

  //INITIALIZE SIMULATION
  Environment *environment = InitializeEnvironment(c);

  environment->Simulate(c);     //this runs the program

  StopSimulation(environment); //freeing memory

  return(0);
}

Environment* InitializeEnvironment(Config c)
{
  Environment *environment = 0;	  
  delete environment;
  return environment = new Environment(c);
}

void StopSimulation(Environment *environment)
{
  delete environment;
}

