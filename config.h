#ifndef _CONFIG_H_GUARD_
#define _CONFIG_H_GUARD_

#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>

class Config
{
 private:
  std::map<std::string,std::string> data;
  std::vector<std::string> split(const std::string &s, char delim);
 public:
  void read(const char* filename);
  template<typename T>
    T get(const std::string &key);
  void print();
};


// we have to define this template function in the header
// so the compiler can generate the real functions on demand
template<typename T>
T Config::get(const std::string &key)
{
  std::map<std::string,std::string>::iterator it = data.find(key);

  if(it == data.end())
    {
      std::cout << "the variable \"" << key
		<< "\" was not defined in the config file!" << std::endl;
      std::exit(0);
    }

  std::stringstream ss(it->second);

  T result;
  ss >> result;
  return result;
}

#endif // _CONFIG_H_GUARD_
