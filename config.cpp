#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string/trim.hpp>
#include "config.h"

using namespace std;

vector<string> Config::split(const string &s, char delim) {
  vector<string> elems;
  stringstream ss(s);
  string item;
  while (getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

void Config::read(const char* filename)
{
  string line;
  ifstream file(filename);
  if(file.is_open())
    {
      while(file.good())
	{
	  string line;
	  getline(file, line);

	  if(line.empty())
	    continue;

	  string variable = split(line, '#').at(0);

	  vector<string> key_value = split(variable, '=');

	  if(key_value.size() < 2)
	    continue;

	  string key = split(variable, '=').at(0);
	  string value = split(variable, '=').at(1);

	  boost::algorithm::trim(key);
	  boost::algorithm::trim(value);

	  if(!(key.empty() || value.empty()))
	    data.insert(pair<string,string>(key, value));
	}
      file.close();
    }
  else
    {
      cout << "# unable to open file " << filename << endl;
    }
}

template<>
string Config::get(const string &key)
{
  return data.at(key);
}

void Config::print()
{
  map<string,string>::iterator it = data.begin();
  for (it=data.begin(); it!=data.end(); ++it)
    {
      string key = it->first;
      string value = it->second;
      cout << "\"" << key << "\"" << " => " << "\"" << value << "\"" << endl;
    }
}
