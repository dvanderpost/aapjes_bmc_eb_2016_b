/* In this file we declare all random number generators (RNGs) we need. */

#ifndef _RANDOM_H_GUARD_
#define _RANDOM_H_GUARD_

#include <boost/random.hpp>

/* declare type abbreviations for normal and uniform distributions */
typedef boost::variate_generator<boost::mt19937&,boost::normal_distribution<> > var_nor;
typedef boost::variate_generator<boost::mt19937&,boost::uniform_real<> > var_uni;
typedef boost::variate_generator<boost::mt19937&,boost::gamma_distribution<> > var_gamma;
typedef boost::variate_generator<boost::mt19937&,boost::exponential_distribution<> > var_exp;

extern boost::mt19937 env_rng;		// basic RNG for environment
extern boost::mt19937 ind_rng;		// basic RNG for individuals

/* declare RNGs for environment construction */
extern var_nor env_nor;
extern var_uni env_uni;

/* declare RNGs for individual behavior */
extern var_nor ind_nor;
extern var_uni ind_uni;
extern var_gamma ind_gamma;
extern var_exp ind_exp;

#endif // _RANDOM_H_GUARD_
