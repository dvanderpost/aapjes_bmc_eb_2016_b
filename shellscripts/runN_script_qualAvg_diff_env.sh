#!/bin/bash
#tsch: to run ten sims for given parfile
#inputs:
# 1 = filename (without _isX_rsX.dat)
# 2 = number of parrallel simulations
# 3 = H

#loop 10 times + adjust file name
rm runlist.txt
for i in {1..10}
	do		
		source=$1"_is"$i"_rs"$i".dat"
		save=$1"_is"$i"_rs"$i"_QA.dat"
		command="nohup nice python pythonscripts/diet_weighted_avg_quality.py $source $3 > $save"
		echo $command >> runlist.txt
	done
cat runlist.txt
cat runlist.txt | parallel -j $2
rm runlist.txt
