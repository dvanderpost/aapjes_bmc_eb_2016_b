#!/bin/bash
#tsch: to run ten sims for given parfile
#inputs:
# 1 = aapjes executable version: Note usuage: aapjes <config_file> <env seed> <ind seed>
# 2 = parfile
# 3 = save file
# 4 = RS for ENV random seed

#moves parfile to unique file in save_directory so that always used right parfile
cp -f $2 $3.par

#loop 10 times + adjust file name
for i in {1..10}
	do
		aap=$1
		par=$3.par
		save=$3"_is"$i"_rs"$4".dat"
		tspcommand="'nice $aap $par "$4" "$i" > $save'" #Note usuage: aapjes <config_file> <env seed> <ind seed>
		command="tsp sh -c $tspcommand"	
		echo $command
		eval $command
	done
