// This file implements classes of individuals.

#include <assert.h>
#include <limits>
#include <vector>
#include <stdio.h>
#include "aapjes.h"
#include "environment.h"
#include "resource.h"
#include "constants.h"
#include "config.h"
#include <boost/math/special_functions/fpclassify.hpp>

// initialize instance counter
// C++ does not allow static member
// variables to be initialized in
// the class itself, so we do it here.
int Aapje::instances = 0;

//TODO: move these to environment
bool RANDOMIZE_GENOME;
int MAXVIEW;
int MAXVIEW_SQ;
float MAXALIGN_SQ;
float MAXALIGN;
int SAFESPACE_SQ;
int SAFESPACE;
float REACH_SQ;
int SAFENUMBER;
float ALIGNPROB;
Point2d grouppos(-1,-1);
int MAX_ITEMS_ASSESSED_PER_SEARCH;
int SEARCH_TYPE;
int GROUPING_STYLE;
bool FULL_KNOW_SWITCH = true;
int PREFEXP_UPDATE_STYLE;
bool SATIATION_SWITCH=false;
float SATIATION_FRACTION;
float E_NOISE;
float L_NOISE, P_NOISE;
int SOCIAL_STIMULUS_STYLE=0;
int COPYSPACE, COPYSPACE_SQ;
int LEARN_TYPE=0;
int COPY_STRATEGY=0;
//float MUTATION_RATE=0;
bool COPYSP_BIGGER=true;
int MAX_GROUP_SIZE=0;

//DIET MODEL (van der Post & Hogeweg 2004,2006,2008, 2009; van der Post et al. 2009
//1 - if not safe -> MOVETOGROUP
//2 - if not hungry -> NOTHING
//3 - else -> SEARCH
//4 - if food -> EAT
//5 - else -> MOVEFORWARD

//////////////// BEHAVIOUR ///////////////////

bool Aapje::WillGroup()
{
  //std::cout<<"Check Will Group\n";
  if(this->grouping) 
    {
      if(this->mf_reps==0) 
	{
	  if(action==MOVETOGROUP || ind_uni()<this->prob_check_safe)
	    {
	      if(!this->IsSafe()) //crowded can get set here
		{
		  this->safe=false;
		  return true;   //will group; also if alone
		}
	      else //is safe
		{
		  this->safe=true; //this represents the last time it checked
		  this->environment->groups[this->group_id].moved=false;
		  //if noone moves this can be used to determine safety (saves calcs)
		}
	      return false;	 //will not group
	    }
	  else //not moving to group and did not check for safety
	    return false;        //will not group
	}
      else //still moving forward
	return false; //if still moving forward: will always complete MOVETOGROUP series
                      //will not group
    }
  else //solitary
    {
      if(this->environment->SOLITARY_SOCIAL_LEARNING)
	{
	  if(ind_uni()<this->prob_check_safe) //to maintain the same rate of social stimuli
	    {
	      //solitary choose neighbor function
	      this->ChooseDemonstrator_Solitary(); //sets this->demon to one of random in pop
	      //Temp stimulus
	      if(this->environment->TEMP_STIMULUS_SWITCH && this->demon!=NULL && this->temp_social_stimulus>0)  
		this->TemporarySocialStimulus(); //stores stimulus to enhance prob next event
	      //should then be used in next FoodChoice
	      //given a demonstrator, should also be able to choose to do ObserveNeighbor?
	    }
	}
      return false; //will not group, if not grouping
    }
}

void Aapje::ObserveNeighbour()
{
  //demon should not be NULL since this checked before hand
  //should be eating - just checked
  if(this->demon->action!=EAT)
    {    
      std::cout<<"ERROR: demonstrator not eating\n";
      std::cout<<"aapjes.cpp: ObserveNeighbour()\n";
      std::exit(0);
    }
  
  //PROD/SUM: 0<x<t_o
  int time = std::min(((int)(this->demon->t_a - this->t_a)),this->t_o); 
  //set max time which the demon can be observed, or max observation time
  //this->t_a is present time; this->demon->t_a is point in future where demon ends action

  this->SocialSkillLearning(time); //skill is updated according to time spent observing

  this->action = OBSERVE;
  this->t_a += time; this->o++;
}

//Note does not lead to increased assocation with reward
void Aapje::SocialSkillLearning(int time)
{
  //max proportion of difference that can be acquire = s_k*(obs_time / eat time)
  int type = this->demon->food_target->type;     

  //updating observables for copying behavior
  this->tot_copy_sk[0]++;
  if(this->demon->id==this->parent_id) this->tot_copy_sk[1]++;
  this->tot_copy_sk[2]+=this->reliability[type];
  this->tot_copy_sk[3]+=this->demon->age;
  this->tot_copy_sk[4]+=this->last_chosen_dem[4];
  this->tot_copy_sk[5]+=this->last_chosen_dem[5];
  this->tot_copy_sk[6]+=this->last_chosen_dem[6];
  this->tot_copy_sk[7]+=time;

  //skill is only updated if demon is more skilled than focal 
  if(this->environment->SKILL_LEARN_TYPE==1 && 
     (this->total_processing_events[type] < this->demon->total_processing_events[type]))
    {  
      //PROD/SUM: 0<x<s_k*[MAX_AGE]
      float skill_inc = this->s_k*(((float)time)/this->demon->t_e)*((float)(this->demon->total_processing_events[type] - this->total_processing_events[type]));
      this->total_processing_events[type] += skill_inc;       //PROD/SUM: 0<x<MAX_AGE*SCALAR
      if(this->total_processing_events[type]>this->environment->MAX_AGE)
	this->total_processing_events[type]=this->environment->MAX_AGE;
      this->tot_copy_sk[8] += skill_inc; //update observable //PROD/SUM: 0<x<MAX_AGE*POPSIZE*SCALAR
      //if(this->tot_copy_sk[8]>std::numeric_limits<float>::max())
      //this->tot_copy_sk[8]=std::numeric_limits<float>::max();
    }
  
  if(this->environment->SKILL_LEARN_TYPE==2 && 
     (this->total_processing_time[type] < this->demon->total_processing_time[type]))
    {
      //PROD/SUM: 0<x<s_k*[MAX_AGE]
      float skill_inc = this->s_k*(
				   ((float)time)/this->demon->t_e)*
	((float)(this->demon->total_processing_time[type] - this->total_processing_time[type])
	 );
      this->total_processing_time[type] += skill_inc;  //PROD/SUM: 0<x<MAX_AGE*SCALAR
      if(this->total_processing_time[type]>this->environment->MAX_AGE*this->environment->TIME_SCALAR)
	this->total_processing_time[type]=this->environment->MAX_AGE*this->environment->TIME_SCALAR;
      this->tot_copy_sk[8] += skill_inc; //update observable //PROD/SUM: 0<x<MAX_AGE*POPSIZE*SCALAR
      //if(this->tot_copy_sk[8]>std::numeric_limits<float>::max())
      //this->tot_copy_sk[8]=std::numeric_limits<float>::max();
    }
  //else does not improve skill
}

void Aapje::MoveOrSearch()
{
  if(this->environment->SOC_SKILL_LEARN_SWITCH && this->demon!=NULL && ind_uni()<this->skill_copy_rate)
    this->ObserveNeighbour();                 //can observe neighbor to socially learn skill
  else //does not observe neighbor
    {
      if(this->stomach_tot<this->MAX_STOMACH) //if not satiated
	{
	  if(!this->crowded) this->Search();
	  else 
	    {
	      //std::cout<<"MoveOrSearch: Is crowded, moves forward\n";
	      this->MoveForward(); 
	      this->mf_reps = ((int)this->d_mf);   //PROD/SUM: 0<x<20 (max gene value)
	    }
	}
      else this->Nothing();
    }
}

void Aapje::Todo()
{
  const bool CHECK=false;
  this->crowded=false; //TODO: check why this is needed for solitary?
  this->demon=NULL; //last_demon is set right after demon is established and is stored
  
  //Grouping has priority
  if(this->WillGroup())
    //If moving to group should continue until safe; Not if alone and not safe;
    //Also checks for demonstrators
    {
      this->MoveToGroup();
      this->mg++;
    }
  else //does other stuff: demon should be updated
    {      
      switch(action)
	{
	case MOVEFORWARD:
	  if(CHECK) std::cout<<"TODO: end Moveforward\n";
	  //std::cin.get();
	  if(this->mf_reps>0) 
	    {
	      this->MoveForward();
	      this->mf_reps--;         //PROD/SUM: 0<x<MOVE_DISTANCE
	      //std::cout<<"Did move forward; "<<this->mf_reps<<" steps left\n";
	      //std::cin.get();
	    }
	  else
	    this->MoveOrSearch();
	  break;
	  
	case SEARCH:
	  if(CHECK)std::cout<<"TODO: end Search\n";
	  //std::cin.get();
	  if(this->food_target) //if not NULL
	    {
	      if( this->environment->ResourceIsAvailable(this->food_target) )
		{
		  if( (this->food_target->position - this->pos).sqlength() <= this->d_r_sq )
		    this->Eat();
		  else
		    this->MoveToFood();
		}
	      else //FOOD HAS BEEN EATEN BY SOMEONE ELSE
		{
		  //std::cout<<"TODO: Food no longer avaialbe!\n";
		  this->food_target=NULL;
		  this->MoveOrSearch();
		}
	    }
	  else //DID NOT FIND FOOD       
	    {
	      //std::cout<<"TODO: Did not find food!\n";
	      this->MoveForward(); 
	      this->mf_reps = ((int)this->d_mf);
	    }
	  break;
	  
	case MOVETOGROUP: //was moving to group: is now safe -or- cannot improve safety
	  //otherwise would continue moving to group to IsSafe check
	  if(CHECK)std::cout<<"TODO: end MoveToGroup\n";
	  //std::cin.get();
	  if((!this->maxview_n.empty() || CENTROID_GROUPING) && ind_uni()<ALIGNPROB) 
	    //only aligns if neighbors in view
	    this->AlignToGroup();
	  this->MoveOrSearch();
	  if(CHECK && !this->grouping)
	    {
	      std::cout<<"ERROR: did movetogroup with groupswitch off!\n";
	      std::exit(0);
	    }		
	  break;
	  
	case MOVETOFOOD:
	  if(CHECK)std::cout<<"TODO: end MoveToFood\n";
	  //std::cin.get();
	  if(this->food_target) //if not NULL
	    {
	      if( this->environment->ResourceIsAvailable(this->food_target) )
		{
		  if( (this->food_target->position - this->pos).sqlength() <= this->d_r_sq )
		    this->Eat();
		  else
		    {
		      this->MoveToFood();
		    }
		}
	      else //FOOD HAS BEEN EATEN BY SOMEONE ELSE
		{
		  this->food_target=NULL;
		  this->MoveOrSearch();
		}	  
	    }
	  else //DID NOT FIND FOOD       
	    {
	      std::cout<<"ERROR: did not have foodtarget after MOVETOFOOD!\n";
	      std::exit(0);
	    }
	  break;
	  
	default: //EAT, NOTHING, OBSERVE
	  if(CHECK)std::cout<<"TODO: end Default\n";
	  //std::cin.get();
	  if(this->action==EAT){
	    this->food_target=NULL; //if last action was eat, remove pointer
	    //std::cin.get();
	  }
	  this->MoveOrSearch();
	}
    }
}

//will only align if 'safe' or cannot movetogroup more (e.g. only one in group)
void Aapje::AlignToGroup()
{
  if(GROUPING_STYLE<2) //no aligning for followleader needed
    this->AlignToGroup_Centroid();
}

//NOT USED
/*void Aapje::AlignToLeader()
{
  if(this->follow_target!=NULL)//<(int)this->environment->aapjes.size())
    {
      //check leader is alive (should be!)
      if(this->follow_target->alive)
	{
	  //get its direction
	  this->dir = this->follow_target->dir;
	}
      else
	{
	  std::cout<<"ERROR: follow target not alive!\n";
	  std::cout<<"aapjes.ccp: AlignToLeader()\n";
	  std::exit(0);
	}
    }
  else
    {
      std::cout<<"ERROR: follow target not in range 0--aapjes.size()!\n";
      std::cout<<"aapjes.ccp: AlignToLeader()\n";
      std::exit(0);
    }
    }*/

//Will only align if finished move to group series & safe, or only one left in group
void Aapje::AlignToGroup_Centroid()
{
  Vec2d align; align.x=0; align.y=0;
  int count=0;

  if( !this->environment->groups[this->group_id].members.empty() ) //should not happen???
    {
      for(std::vector<Aapje*>::iterator it = this->environment->groups[this->group_id].members.begin(); it != this->environment->groups[this->group_id].members.end(); ++it)
	{
	  if(*it!=this)
	    {
	      float sqdist = ((*it)->pos - this->pos).sqlength(); //get distsance
	      if(sqdist <= MAXALIGN_SQ)
		{
		  align = align + (*it)->dir;    //PROD/SUM: 0<x<1.0*POPSIZE
		  count++;
		}
	    }
	}

      if(count>0) //if found neighbors
	{
	  Vec2d temp = align.norm();

	  if(!isnan(temp.x) && !isnan(temp.y)) //can happen because two individual opposite align
	    this->dir = temp;
	  //else: keeps present heading
	}
      else //did not find neighbors
	{
	  //should never reach here if pop style == 1 (fixed group size)
	  //can reach here if pop style == 0 or 2, and group is size 1
	  // -> last neighbor died between move to group and align if this ind
	  if(this->environment->POPULATION_STYLE==1)
	    {
	      std::cout<<"ERROR: tried to do align but no one close enough!\n";
	      std::cout<<"aapjes_new.cpp: ****** AlignToGroup() ***********\n";
	      std::exit(0);
	    }
	  //else: keeps present heading
	  else
	    {
	      //group should be size==1
	      if(!this->environment->groups[this->group_id].members.size()==1)
		{
		  std::cout<<"ERROR: tried to do align but noone found!\n";
		  std::cout<<"Is possible for POP_STYLE ==1 or 2\n";
		  std::cout<<"However: group size = "<<this->environment->groups[this->group_id].members.size()<<"; and should be 1\n";
		  std::exit(0);
		}
	    }
	}
    }
  else //no group members
    {
      std::cout<<"ERROR: tried to do align but no one in group members array!\n";
      std::cout<<"aapjes_new.cpp: ****** AlignToGroup() ***********\n";
      std::exit(0);
    }
}


void Aapje::AlignToGroup_XPOS()
{
  Vec2d align; align.x=0; align.y=0;
  int count=0;
  if( !this->maxview_n.empty() )
    {
      for(std::list<Aapje*>::iterator it = this->maxview_n.begin(); it != this->maxview_n.end(); ++it)
	{
	  //NOTE: all in maxview_n are not self
	  float sqdist = ((*it)->pos - this->pos).sqlength(); //get distsance
	  if(sqdist <= MAXALIGN_SQ)
	    {
	      align = align + (*it)->dir; //PROD/SUM: 0<x<1.0*POPSIZE
	      count++;
	    }
	}
      if(count>0)
	{
	  Vec2d temp = align.norm();

	  if(!isnan(temp.x) && !isnan(temp.y)) //can happen because two individual opposite align
	    this->dir = temp;
	  //else: keeps present heading
	}
      else
	{
	  std::cout<<"ERROR: tried to do align but no one close enough!\n";
	  std::cout<<"aapjes_new.cpp: ****** AlignToGroup() ***********\n";
	  std::exit(0);
	}
    }
  else
    {
      std::cout<<"ERROR: tried to do align with no one in maxview array!\n";
      std::cout<<"aapjes_new.cpp: ****** AlignToGroup() ***********\n";
      std::exit(0);
    }
}

//NOT USED
/*int Aapje::FindQuadrant(Vec2d ran_vec, Vec2d ind_vec)
{
  const bool CHECK=false;

  float length = ind_vec.sqlength();
  
  if(length>0) //otherwise normalization is undefined (and is not in any quadrant
               //so, instead is assigned to random quadrant below
    {
      ind_vec = ind_vec*(1.0/sqrt(length));
      if(CHECK && (isnan(ind_vec.x) || isnan(ind_vec.y)) )
	{
	  std::cout<<"ERROR: aapjes.cpp (movetogroup rand quad): ind_vec = NAN!\n";
	  std::exit(0);
	}

      if ( ind_vec.x > 0 )
	{
	  if ( ind_vec.y > 0 )
	    {
	      if ( ind_vec.y < ran_vec.y ) return 3;
	      else return 0;
	    }
	  else //ind_vec.y <= 0
	    {
	      if ( ind_vec.x < ran_vec.y ) return 2;
	      else return 3;
	    }
	}
      else //ind_vec.x <= 0
	{
	  if ( ind_vec.x < 0)
	    {
	      if ( ind_vec.y >= 0 )
		if (ind_vec.x > -ran_vec.y) return 0;
		else return 1;	    
	      else //ind_vec.y < 0 
		if (ind_vec.y > -ran_vec.y) return 1;
		else return 2;
	    }
	  else //if ( ind_vec.x == 0 )
	    {
	      if ( ind_vec.y > 0 ) //from ind_vec.x >= 0
		if ( ind_vec.y < ran_vec.y ) return 3;
		else return 0;		
	      else // ind_vec.y < 0 from ind_vec.x <= 0
		if (ind_vec.y > -ran_vec.y) return 1;
		else return 2;
	    }
	}
    } //end: length>0
  else //length == 0: choose random quadrant 0-3
    {
      return ind_uni()*4;
    }
}

//NOT USED
void Aapje::MoveToGroup_RandQuad()
{
  const bool CHECK=false;

  Vec2d ran_vec = this->RandomVector(0, halfPI); //random vector between 0 and 90 degrees
  
  if(CHECK && (isnan(ran_vec.x) || isnan(ran_vec.y)) )
    {
      std::cout<<"ERROR: ranvecx or y is NAN!2\n";
      std::cout<<"aapjes.cpp: MoveToGroup_RandQuad\n";
      std::exit(0);
    }

  //(a) rx, ry
  //(b) -rx, ry
  //(c) -rx, -ry
  //(d) rx, ry
  //quadrants: 0 = a-b; 1 = b-c; 2 = c-d; 3 = d-a;

  std::vector< std::vector<Aapje*>  > neigh_quads(4, std::vector<Aapje*>() );
  int q_tot[4]={0,0,0,0};
  for(std::list<Aapje*>::iterator it = this->maxview_n.begin(); it != this->maxview_n.end(); ++it)  
    {
      Vec2d ind_vec = ((*it)->pos - this->pos); 
      int quadrant=this->FindQuadrant(ran_vec, ind_vec);
      
      if(quadrant>=0)
	{
	  neigh_quads[quadrant].push_back(*it); //store ind. in quad array
	  q_tot[quadrant]++;       
	}
      else       
	if(CHECK)
	  {
	    std::cout<<"ERROR: quadrant not assigned!\n";
	    std::cout<<"indv.x "<<ind_vec.x<<" indv.y "<<ind_vec.y<<" ranv.x "<<ran_vec.x<<" ranv.y "<<ran_vec.y<<" maxview_n "<<maxview_n.size()<<std::endl;
	    std::cout<<"nd id "<<(*it)->id<<" nx "<<(*it)->pos.x <<" ny "<<(*it)->pos.y <<std::endl;
	    std::cout<<"this id "<<this->id<<" this x "<< this->pos.x <<" this y "<< this->pos.y <<std::endl;
	    std::cout<<" indv x "<<(*it)->pos.x-this->pos.x <<" indv y "<<(*it)->pos.y-this->pos.y <<std::endl;	   
	    std::exit(0);	 
	  }
    }//end: for loop of maxview_n
  
  //find fullest quadrants (+ choose random of the fullest)
  int fullest_quadrant=0;
  int count=1;
  std::vector<int> fullest_quadrants;
  fullest_quadrants.push_back(0); //start with 0
  for(int q=1; q<4; q++)
    {
      if( q_tot[q]>q_tot[fullest_quadrant] )
	{
	  //quaddrant q is bigger than previous biggest
	  fullest_quadrant = q;
	  fullest_quadrants.clear();
	  fullest_quadrants.push_back(q);
	  count=1;
	}
      else if ( q_tot[q] == q_tot[fullest_quadrant] )
	{
	  //equally big, so added to full quads vector
	  fullest_quadrants.push_back(q);
	  count++;
	}
    }
  int chosen_quadrant = fullest_quadrants[(int)(ind_uni()*count)];
  if(CHECK && neigh_quads[chosen_quadrant].empty())
    {
      std::cout<<"ERROR: no neighbours in fullest quadrant\n";
      std::cout<<"aapjes.c: ***** MoveToGroup_RandQuad****\n";
      std::exit(0);
    }

  //find closest neighbor (which is beyond d_mg) in chosen quadrant
  float min_dist = this->environment->XFIELD; //this->environment->XFIELD;

  std::vector<Aapje*> closest_neighs;

  int cn_count=0;
  for(std::vector<Aapje*>::iterator it = neigh_quads[chosen_quadrant].begin(); it != neigh_quads[chosen_quadrant].end(); ++it)
    {
      float dist = ((*it)->pos - this->pos).sqlength();
      if(dist < min_dist && dist > this->d_mg_sq) //close but further than move to group distance
	{
	  min_dist = dist;
	  closest_neighs.clear();
	  closest_neighs.push_back(*it);
	  cn_count=1;
	}
      else
	if(dist == min_dist) {closest_neighs.push_back(*it); cn_count++;}
    }

  //assign new position based on approaching closest neighbor
  Point2d new_pos;

  if(!closest_neighs.empty()) 
    //can happen when all quadrant equally full and choose one with ind too close
    {    
      Aapje* closest = closest_neighs[(int)(ind_uni()*cn_count)];
      
      //get vector there:
      this->dir = (closest->pos - this->pos).norm();
      if(CHECK && (isnan(this->dir.x ) || isnan(this->dir.y)) ) 
	{
	  std::cout<<"aapjes.cpp: MoveToGroupRandQuad(): direction vector of aap "<<this->id<<" NAN "<<this->dir.x<<" "<<this->dir.y<<" at time "<<this->t_a<<std::endl;
	  std::exit(0);
	}
      new_pos = this->pos + (this->dir*this->d_m);
      action=MOVETOGROUP; this->t_a += (long)(this->d_m*this->s_mg); 
    }
  else //could not find suitable neighbor -> MOVEFORWARD
    {
      new_pos = this->pos + (this->dir*this->d_m);
      action=MOVEFORWARD; this->t_a += (long)(this->d_m*this->s_m); 
    }
  
  Move(new_pos, this->action); 

  if(CHECK && !this->environment->CheckPositionIsOnGrid(this->pos))
    {
      std::cout<<"MTG: ERROR! Will move off the field\n";
      std::cout<<" pos x "<<(float)this->pos.x<<" pos y "<<(float)this->pos.y<<" dir x "<<this->dir.x<<" dir y "<<this->dir.y<<" new x "<<(float)new_pos.x<<" new y "<<(float)new_pos.y<<std::endl;
      std::exit(0);
    }
    }*/

void Aapje::MoveToGroup()
{
  //Will only reach here if not alone
  if(GROUPING_STYLE<2)
    this->MoveToGroup_Centroid2();
  else
    //if(this->follow_target!=NULL) //NEEDED?
    this->MoveToLeader();
  //else = independent leader
  }

void Aapje::MoveToLeader()
{
  //check leader exists
  if(this->follow_target!=NULL)
    {
      //check leader is not dead
      if(this->follow_target->alive)
	{
	  //get direction of leader + action timing details
	  float xdis = this->follow_target->pos.x - this->pos.x; //PROD: 0<x<FIELD
	  float ydis = this->follow_target->pos.y - this->pos.y;
	  float distsq = (xdis*xdis)+(ydis*ydis);    //PROD/SUM: FIELD^2 + FIELD^2
	  if(distsq>this->d_mg_sq)
	    {
	      float dist = std::sqrt(distsq);        //SQRT: only happens if distsq > d_mg_sq > 0
	      this->dir.x = xdis/dist;
	      this->dir.y = ydis/dist;
	    }
	  //else keep same direction
	  
	  Move((this->pos + (this->dir*this->d_m)), MOVETOGROUP);  //PROD/SUM: 0<x<XFIELD+d_m
	  
	  //PROD/SUM: 0<x<MAXAGE+[d_m*s_mg]
	  action=MOVETOGROUP; this->t_a += (long)(this->d_m*this->s_mg);  //PROD/SUM: within boun.
	  //std::cout<<"Ind "<<this->id<<" will move to leader\n";
	}
    }
  else
    {
      std::cout<<"ERROR: has no followtarget, should not move to leader\n";
      std::cout<<"aapjes.cpp: MoveToLeader()\n";
      std::exit(0);
    }
}

void Aapje::MoveToGroup_Centroid2()
{
  //for all in maxview    
  Point2d target(0,0);

  int num = 0; //int ind=0;
  //Should be at least one other individual (since IfSafe returns false only if can improve sit.)
  for(std::vector<Aapje*>::iterator it = this->environment->groups[this->group_id].members.begin(); it != this->environment->groups[this->group_id].members.end(); ++it)
    {
      if(*it!=this)
	{
	  //if(this->sqdist_neighs[ind] <= (float)MAXVIEW_SQ) //SQDIST caclulated in IsSafe
	  //{
	  // get central position
	  target.x += (*it)->pos.x;  //PROD/SUM: 0<x<XFIELD*POPSIZE
	  target.y += (*it)->pos.y;
	  num++;                     //PROD/SUM: 0<x<POPSIZE
	  //  }	      
	}
      //ind++;
    }

  if(num==0)
    {
      std::cout<<"ERROR: MoveToGroup_Centroid2: num neighs = 0!\n";
      //ind=0;
      /*for(std::vector<Aapje*>::iterator it = this->environment->groups[this->group_id].members.begin(); it != this->environment->groups[this->group_id].members.end(); ++it)
	{
	  std::cout<<" id "<<(*it)->id<<" dis "<<this->sqdist_neighs[ind]<<" mxsq "<<(float)MAXVIEW_SQ;
	  ind++;
	}
	std::cout<<"\n";*/
      std::exit(0);
    }

  target.x = target.x/num;  //PROD/SUM: 0<x<XFIELD  
  target.y = target.y/num;
  float xdis = target.x - this->pos.x;  //PROD/SUM: -XFIELD<x<XFIELD
  float ydis = target.y - this->pos.y;  //should be -XFIELD<x<XFIELD
  float distsq = (xdis*xdis)+(ydis*ydis); //PROD/SUM should be 0<x<[XFIELD^2 + YFIELD^2]
  if(distsq>this->d_mg_sq)
    {
      float dist = std::sqrt(distsq);     //SQRT: only if distsq > d_mg_sq
      this->dir.x = xdis/dist;            //PROD/SUM: 0<x<1
      this->dir.y = ydis/dist;
    }
  //else keep same direction

  Move((this->pos + (this->dir*this->d_m)), MOVETOGROUP);  //PROD/SUM: -d_m<0<FIELD+d_m

  action=MOVETOGROUP; this->t_a += (long)(this->d_m*this->s_mg);  //PROD/SUM: 0<x<MAXAGE+d_m*s_mg
}

//NOT USED
/*void Aapje::MoveToGroup_XPOS()
{
  const bool CHECK=false;

  //for all those in maxview
  Point2d target;
  for(std::list<Aapje*>::iterator it = this->maxview_n.begin(); it != this->maxview_n.end(); ++it)
    {
      target.x += (*it)->pos.x;
      target.y += (*it)->pos.y;
    }
  int total=maxview_n.size();
  target.x = target.x/total;
  target.y = target.y/total;

  if(CHECK && !this->environment->CheckPositionIsOnGrid(target))
    {
      std::cout<<"MTG: ERROR! average group position is off the field\n"; 
      std::cout<<"aapjes.cpp *** MoveToGroup() ******\n";
      std::cout<<" pos x "<<this->pos.x<<" pos y "<<this->pos.y<<" dir x "<<this->dir.x<<" dir y "<<this->dir.y<<" tar x "<<target.x<<" tar y "<<target.y<<" n in maxview "<<maxview_n.size()<<std::endl;
      std::exit(0);
      }

  int xdis = target.x - this->pos.x;
  int ydis = target.y - this->pos.y;
  int distsq = (xdis*xdis)+(ydis*ydis);  //PROD/SUM within bounds
  if(distsq>this->d_mg_sq)
    {
      float dist = std::sqrt(distsq);    //SQRT only if distsq > d_mg_sq
      this->dir.x = xdis/dist;
      this->dir.y = ydis/dist;
    }
  //else keep same direction

  if(CHECK && (isnan(this->dir.x ) || isnan(this->dir.y))) 
    {  
      std::cout<<"aapjes.cpp: MoveToGroup(): direction vector of aap "<<this->id<<" NAN "<<this->dir.x<<" "<<this->dir.y<<" at time "<<this->t_a<<std::endl;
      std::cout<<" targ x "<<target.x<<" targ y "<<target.y<<std::endl;
      std::cout<<" pos x "<<this->pos.x<<" pos y "<<this->pos.y<<std::endl;  
      std::exit(0);
      }

  Move((this->pos + (this->dir*this->d_m)), MOVETOGROUP);  //PROD/SUM in bounds and on field
  
  action=MOVETOGROUP; this->t_a += (long)(this->d_m*this->s_mg); //PROD/SUM in bounds
  }*/

//NOT USED
/*id Aapje::UpdateMaxviewSafeSpaceCrowding(Aapje *neigh, float xdist, int *maxv_count, int *safesp_count)
{
  float ydist = std::abs(neigh->pos.y - this->pos.y);
  if(ydist<(float)MAXVIEW)
    {
      float sqdist = (xdist*xdist) + (ydist*ydist);  //PROD within bounds
      if(sqdist<(float)MAXVIEW_SQ)
	{
	  this->maxview_n.push_back(neigh);
	  ++*maxv_count;
	  
	  //CANNOT EMBED WITHIN SAFESPACE IN CASE SAFESPACE IS SMALLER
	  if(COPYSP_BIGGER)
	    {
	      if(sqdist<(float)COPYSPACE_SQ) //if no stimulus
		{
		  if(this->time_since_stimulation==0 && neigh->food_target && neigh->action==EAT)
		    {
		      this->copyspace_n.push_back(neigh);
		      // - use vector to choose: payoff, age, maj, familiar
		    }
		  if(sqdist<(float)SAFESPACE_SQ)
		    {
		      ++*safesp_count;
		      if(!this->crowded && sqdist<(float)REACH_SQ)
			{
			  this->crowded=true;
			}						  
		    }
		}
	    }
	  else
	    {
	      if(sqdist<SAFESPACE_SQ)
		{
		  ++*safesp_count;	  
		  
		  if(sqdist<COPYSPACE_SQ) //if no stimulus
		    {
		      if(this->time_since_stimulation==0)
			if(neigh->food_target && neigh->action==EAT)
			  {
			    this->copyspace_n.push_back(neigh);
			  }
		      
		      if(!this->crowded && sqdist<REACH_SQ)
			{
			  this->crowded=true;
			}
		    }
		}
	    }
	}
    }
    }*/

bool Aapje::IsSafe()
{
  if(this->safe && !this->environment->groups[this->group_id].moved)
    {
      //This is only true if noone moved and it was safe last time it checked
      //this->safe remains TRUE
      return true;
    }

  //if not safe, or safe but someone in group moved
  if(GROUPING_STYLE<2)
    {
      //std::cout<<"Will check for safety\n";
      return this->IsSafe_Centroid2();
    }
  else
    return this->IsSafe_FollowLeader(); //independent leaders include for copyspace
}

//need to do copyspace!
bool Aapje::IsSafe_FollowLeader()
{
  bool safe=false;  //safe flag rest to false (someone moved)
  if(this->follow_target==NULL) safe=true;  //if the leader always feel safe
  else
    {
      if(this->follow_target->alive)//can group as long as target is alive
	{
	  float xdis = this->follow_target->pos.x - this->pos.x; //PROD/SUM: -FIELD<x<FIELD
	  float ydis = this->follow_target->pos.y - this->pos.y;
	  float sqdist = (xdis*xdis)+(ydis*ydis);       //PROD/SUM: 0<x<XFIELD^2+YFIELD^2
	  if(sqdist <= (float)SAFESPACE_SQ) 
	    {
	      safe=true;
	    }
	  //esle safe = false;
	}
      else //if not alive: can generate move to leader loop (so will instead forage)
	safe=true; 
    }

  this->crowded=false;

  //Will always observe in order to check for demonstrators
  for(std::vector<Aapje*>::iterator it = this->environment->groups[this->group_id].members.begin(); it != this->environment->groups[this->group_id].members.end(); ++it)
    {
      //not me; is eating; has food target for res ID
      if((*it)!=this)
	{
	  if((*it)->food_target && (*it)->action==EAT) //should be alive and eating
	    {
	      float xdis = this->pos.x - (*it)->pos.x;     //PROD/SUM: -FIELD<x<FIELD
	      float ydis = this->pos.y - (*it)->pos.y;
	      float sqdist = (xdis*xdis)+(ydis*ydis);      //PROD/SUM: 0<x<[XFIELD^2+YFIELD^2]
	      
	      if(sqdist <= COPYSPACE_SQ)
		{
		  this->copyspace_n.push_back(*it);
		  if(!this->crowded && sqdist<=this->d_r_sq) this->crowded=true;
		}
	    }//end if has foodtarget + is eating
	}//end if not me
    }//end for loop over group
      
  if(!this->copyspace_n.empty())
    {
      this->SocialLearning();     //For SE gets stimulus
      this->copyspace_n.clear();  //resets for next time
    }

  return safe;
}

bool Aapje::IsSafe_Centroid2()
{
  //check all distances < safespace
  
  //std::cout<<"Checking for safety\n";

  int safecount=0;
  int totcount=0;
  this->crowded=false;
  int ind=0;
  for(std::vector<Aapje*>::iterator it = this->environment->groups[this->group_id].members.begin(); it != this->environment->groups[this->group_id].members.end(); ++it)
    {
      if(*it!=this) //should be alive!
	{
	  float xdis = this->pos.x - (*it)->pos.x;    //PROD/SUM: 0<x<XFIELD
	  float ydis = this->pos.y - (*it)->pos.y;
	  float sqdist = (xdis*xdis)+(ydis*ydis);     //PROD/SUM: 0<x<XFIELD^2 + YFIELD^2
	    
	  //Store for MoveToGroup/Leader to not calculate twice
	  this->sqdist_neighs[ind] = sqdist; //put in order of groupmember
	    
	  if(sqdist <= (float)SAFESPACE_SQ)
	    {
	      safecount++;
	      if(!this->crowded && sqdist<=this->d_r_sq) this->crowded=true;
	    }
	  
	  if((*it)->action==EAT && sqdist <= COPYSPACE_SQ)
	    {
	      //std::cout<<"Someone in copyspace\n";
	      if((*it)->food_target==NULL) {std::cout<<"ERROR: IsSafe_Centroid2: neighbour action is EAT, but has no food target!\n"; std::exit(0);}
	      this->copyspace_n.push_back(*it);
	    }
     
	  totcount++;
	}
      ind++; //tracks which individual of groupmembers is next for sqdist array
    }

  if(!this->copyspace_n.empty())
    {
      //std::cout<<"Will do SocailLearning\n";
      this->SocialLearning();
      this->copyspace_n.clear();  //resets for next time
    }

  if(safecount>=this->safenumber || totcount==safecount)
    {
      return true; //not safe but cannot improve its situation right now
    }
  return false; //not safe but can improve situation
}

//Occurs during IsSafe() if !copyspace.empty()
void Aapje::SocialLearning()
{
  //Demon bias
  if(this->environment->TEMP_STIMULUS_SWITCH || this->environment->SOC_SKILL_LEARN_SWITCH)    
    {
      if(this->environment->BIASES_SWITCH)
	{
	  if(this->environment->CHOOSE_OLDEST) {this->ChooseDemonstrator_Oldest();}
	  else {this->ChooseDemonstrator_Biases();} //sets chosen demonstrator as this->demon
	}
      else
	this->ChooseDemonstrator_NoBiases(); //sets chosen demonstrator as this->demon
      this->last_demon = this->demon;
    }
      
  //Direct social learning mechanisms
  //Temp stimulus
  if(this->environment->TEMP_STIMULUS_SWITCH && this->demon!=NULL && this->temp_social_stimulus>0)  
    {
      //if did not already have stimulus
      this->TemporarySocialStimulus(); //stores stimulus to enhance prob next event
    }
      
  //Perm stimulus
  //if(this->environment->SOCIAL_STIMULUS_SWITCH>2 && this->demon!=NULL && this->perm_social_stimulus>0)        
  //this->PermanentSocialStimulus(); //updates associataion: pref

  //Skill learning occurs if individual selects 'OBSERVE' action immediately after IsSafe
}

//Note this is a simple increment
void Aapje::PermanentSocialStimulus()
{
  this->prefs[demon->food_target->type] += this->perm_social_stimulus;
}

void Aapje::ChooseDemonstrator_Solitary()
{
  int rand = (int)(ind_uni()*this->environment->aapjes.size()); //select random individual
  
  //store data and set demon
  int num_res=0;
  int count=0;
  std::vector<int> eaten (this->environment->resourcetypes.size(), 0);
  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin();
      it != this->copyspace_n.end(); 
      ++it)
    {
      if(eaten[(*it)->food_target->type]==0) num_res++; //PROD/SUM:: 0<x<POPSIZE
      eaten[(*it)->food_target->type]++; //sums number of individuals eaten that type 
      //PROD/SUM: 0<x<POPSIZE
      
      count++;
      if(count == rand) this->demon = (*it); //SET DEMON!
    }
  this->tot_dem[3]+=num_res; //PROD/SUM: 0<x<[POPSIZE*SAVE_TIME*EVENTS_PER_MINUTE]
  this->tot_dem[0]++; //incr. number of observations //PROD/SUM: 0<x<SAVE_TIME*EVENTS_PER_MINUTE
  
  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin(); 
      it != this->copyspace_n.end(); ++it)
    {
      //bias due to constant, reliability, frequency, age, experience
      //PROD/SUM: 0<x<1
      float freq = (((float)eaten[(*it)->food_target->type])/this->copyspace_n.size());
      //update observables
      //std::cout<<"Add to tot dem\n";
      this->tot_dem[1]++; //inc num of demonstrators observed //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->tot_dem[4]+=this->reliability[(*it)->food_target->type]; //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->tot_dem[5]+=(*it)->age; //PROD/SUM: 0<x<SAVE_TIME*E_P_M*MAX_AGE
      this->tot_dem[6]+=(*it)->total_processing_time[(*it)->food_target->type]; //PROD/SUM: 0<x<SAVE_TIME*MAX_AGE/INF?
      this->tot_dem[8]+=freq;
      
      //payoff
      //PROD/SUM: -20*5-20*|MAXQ|<x<20*5+20*|MAXQ| 
      float payoff = (*it)->ExperienceBasedProcessingReward((*it)->food_target->type);
      this->tot_dem[7]+=payoff; 
      //PROD/SUM: -20*|MAXQ|*SAVE_TIME*E_P_M<x<SAVE_TIME*E_P_M*20*|MAXQ|
      
      //maternal bias
      //PROD/SUM: -20*6-20*|MAXQ|<x<20*6+20*|MAXQ| 
      if((*it)->id==this->parent_id)
	{
	  this->tot_dem[2]++; //incr. number of times mother seen PROD/SUM: 0<x<SAVETIME*E_P_M
	}
    }  
}

void Aapje::ChooseDemonstrator_Oldest()
{
  float age=-1;
  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin();
      it != this->copyspace_n.end(); 
      ++it)
    {
      if((*it)->age > age) 
	{
	  age = (*it)->age;
	  this->demon = (*it);
	}
    }
  if(this->age >= this->demon->age) {this->demon=NULL;}
  else{
    //std::cout<<"Demon "<<this->demon->id<<" age "<<this->demon->age<<" own age "<<this->age<<std::endl;

  //store data
  int num_res=0;
  std::vector<int> eaten (this->environment->resourcetypes.size(), 0);
  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin();
      it != this->copyspace_n.end(); 
      ++it)
    {
      if(eaten[(*it)->food_target->type]==0) num_res++; //PROD/SUM:: 0<x<POPSIZE
      eaten[(*it)->food_target->type]++; //sums number of individuals eaten that type 
      //PROD/SUM: 0<x<POPSIZE
    }
  this->tot_dem[3]+=num_res; //PROD/SUM: 0<x<[POPSIZE*SAVE_TIME*EVENTS_PER_MINUTE]
  this->tot_dem[0]++; //incr. number of observations //PROD/SUM: 0<x<SAVE_TIME*EVENTS_PER_MINUTE

  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin(); 
      it != this->copyspace_n.end(); ++it)
    {
      //bias due to constant, reliability, frequency, age, experience
      //PROD/SUM: 0<x<1
      float freq = (((float)eaten[(*it)->food_target->type])/this->copyspace_n.size());
      //update observables
      //std::cout<<"Add to tot dem\n";
      this->tot_dem[1]++; //inc num of demonstrators observed //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->tot_dem[4]+=this->reliability[(*it)->food_target->type]; //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->tot_dem[5]+=(*it)->age; //PROD/SUM: 0<x<SAVE_TIME*E_P_M*MAX_AGE
      this->tot_dem[6]+=(*it)->total_processing_time[(*it)->food_target->type]; //PROD/SUM: 0<x<SAVE_TIME*MAX_AGE/INF?
      this->tot_dem[8]+=freq;
 
      //payoff
      //PROD/SUM: -20*5-20*|MAXQ|<x<20*5+20*|MAXQ| 
      float payoff = (*it)->ExperienceBasedProcessingReward((*it)->food_target->type);
      this->tot_dem[7]+=payoff; 
      //PROD/SUM: -20*|MAXQ|*SAVE_TIME*E_P_M<x<SAVE_TIME*E_P_M*20*|MAXQ|
      
      //maternal bias
      //PROD/SUM: -20*6-20*|MAXQ|<x<20*6+20*|MAXQ| 
      if((*it)->id==this->parent_id)
	{
	  this->tot_dem[2]++; //incr. number of times mother seen PROD/SUM: 0<x<SAVETIME*E_P_M
	}
    }}
}

void Aapje::ChooseDemonstrator_NoBiases()
{
  //select random demonstrator
  int rand = (int)(ind_uni()*this->copyspace_n.size());
  //this->demon = this->copyspace_n[(int)(ind_uni()*this->copyspace_n.size())];

  //store data
  int num_res=0;
  int count=0;
  std::vector<int> eaten (this->environment->resourcetypes.size(), 0);
  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin();
      it != this->copyspace_n.end(); 
      ++it)
    {
      if(eaten[(*it)->food_target->type]==0) num_res++; //PROD/SUM:: 0<x<POPSIZE
      eaten[(*it)->food_target->type]++; //sums number of individuals eaten that type 
      //PROD/SUM: 0<x<POPSIZE

      count++;
      if(count == rand) this->demon = (*it);
    }
  this->tot_dem[3]+=num_res; //PROD/SUM: 0<x<[POPSIZE*SAVE_TIME*EVENTS_PER_MINUTE]
  this->tot_dem[0]++; //incr. number of observations //PROD/SUM: 0<x<SAVE_TIME*EVENTS_PER_MINUTE

  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin(); 
      it != this->copyspace_n.end(); ++it)
    {
      //bias due to constant, reliability, frequency, age, experience
      //PROD/SUM: 0<x<1
      float freq = (((float)eaten[(*it)->food_target->type])/this->copyspace_n.size());
      //update observables
      //std::cout<<"Add to tot dem\n";
      this->tot_dem[1]++; //inc num of demonstrators observed //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->tot_dem[4]+=this->reliability[(*it)->food_target->type]; //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->tot_dem[5]+=(*it)->age; //PROD/SUM: 0<x<SAVE_TIME*E_P_M*MAX_AGE
      this->tot_dem[6]+=(*it)->total_processing_time[(*it)->food_target->type]; //PROD/SUM: 0<x<SAVE_TIME*MAX_AGE/INF?
      this->tot_dem[8]+=freq;
 
      //payoff
      //PROD/SUM: -20*5-20*|MAXQ|<x<20*5+20*|MAXQ| 
      float payoff = (*it)->ExperienceBasedProcessingReward((*it)->food_target->type);
      this->tot_dem[7]+=payoff; 
      //PROD/SUM: -20*|MAXQ|*SAVE_TIME*E_P_M<x<SAVE_TIME*E_P_M*20*|MAXQ|

      //maternal bias
      //PROD/SUM: -20*6-20*|MAXQ|<x<20*6+20*|MAXQ| 
      if((*it)->id==this->parent_id)
	{
	  this->tot_dem[2]++; //incr. number of times mother seen PROD/SUM: 0<x<SAVETIME*E_P_M
	}
    }
}

void Aapje::ChooseDemonstrator_Biases()
{
  this->demon=NULL; //reset to zero (should be redundant)

  //first establish how many neighbors are eating what
  int num_res=0;
  std::vector<int> eaten (this->environment->resourcetypes.size(), 0);
  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin();
      it != this->copyspace_n.end(); 
      ++it)
    {
      if(eaten[(*it)->food_target->type]==0) num_res++; //PROD/SUM:: 0<x<POPSIZE
      eaten[(*it)->food_target->type]++; //sums number of individuals eaten that type 
      //PROD/SUM: 0<x<POPSIZE
      if((*it)->action!=EAT)
	{
	  std::cout<<"ERROR: neighbor in copyspace is not eating!\n";
	  std::cout<<"aapje.cpp: ChooseDemonstrator3()\n";
	  std::exit(0);
	}
    }
  this->tot_dem[3]+=num_res; //PROD/SUM: 0<x<[POPSIZE*SAVE_TIME*EVENTS_PER_MINUTE]

  //establish claim to select demonstrator (i.e. roulette-wheel system)
  float claim=0; int count=0;                                    //total
  this->tot_dem[0]++; //incr. number of observations //PROD/SUM: 0<x<SAVE_TIME*EVENTS_PER_MINUTE
  std::vector<float> claim_array (this->copyspace_n.size(), 0.0); //claim of each ind

  for(std::list<Aapje*>::iterator it = this->copyspace_n.begin(); 
      it != this->copyspace_n.end(); ++it)
    {
      //bias due to constant, reliability, frequency, age, experience
      //PROD/SUM: 0<x<1
      float freq = (((float)eaten[(*it)->food_target->type])/this->copyspace_n.size());

      //PROD/SUM: -20*5<x<20*5
      float p = this->l_c;

      if(this->environment->BIAS_R_SWITCH)
	p += this->l_r*this->reliability[(*it)->food_target->type];    //PROD/SUM -20<=x<=20
      
      if(this->environment->BIAS_F_SWITCH)
	p += this->l_f*freq;                                           //PROD/SUM -20<=x<=20

      if(this->environment->BIAS_A_SWITCH)
	p += this->l_a*((float)(*it)->age)/this->environment->MAX_AGE;  //PROD/SUM -20<=x<=20

      if(this->environment->BIAS_T_SWITCH)	
	p += this->l_t*(*it)->total_processing_time[(*it)->food_target->type]/this->environment->MAX_AGE;                                                               //PROD/SUM -20<=x<=20

      //update observables
      //std::cout<<"Add to tot dem\n";
      this->tot_dem[1]++; //inc num of demonstrators observed //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->tot_dem[4]+=this->reliability[(*it)->food_target->type]; //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->tot_dem[5]+=(*it)->age; //PROD/SUM: 0<x<SAVE_TIME*E_P_M*MAX_AGE
      this->tot_dem[6]+=(*it)->total_processing_time[(*it)->food_target->type]; //PROD/SUM: 0<x<SAVE_TIME*MAX_AGE/INF?
      this->tot_dem[8]+=freq;
 
      //payoff
      //PROD/SUM: -20*5-20*|MAXQ|<x<20*5+20*|MAXQ| 
      if(this->environment->BIAS_P_SWITCH)
	{
	  float payoff = (*it)->ExperienceBasedProcessingReward((*it)->food_target->type);
	  p += this->l_p*payoff;       //PROD/SUM -20*|MAXQ|<x<20*|MAXQ|
	  this->tot_dem[7]+=payoff; 
	  //PROD/SUM: -20*|MAXQ|*SAVE_TIME*E_P_M<x<SAVE_TIME*E_P_M*20*|MAXQ|
	}

      //maternal bias
      //PROD/SUM: -20*6-20*|MAXQ|<x<20*6+20*|MAXQ| 
      if(this->environment->BIAS_M_SWITCH)
	if((*it)->id==this->parent_id)
	  {
	    p+=this->l_M;
	    this->tot_dem[2]++; //incr. number of times mother seen PROD/SUM: 0<x<SAVETIME*E_P_M
	  }
      
      //attractiveness: biases framed in logistic equation
      //PROD/SUM: 0<x<1
      float a = 1/(1 + std::exp(-p));       //PROD/SUM bound between 0 and 1

      claim+=a;  //PROD/SUM: 0<x<POPSIZE
      claim_array[count]=claim; //count is parallel to index of copyspace
      count++; //iterate to next index of claim_array  PROD/SUM: 0<x<POPSIZE
    }

  float rand = 0;
  //PROD/SUM: 0<x<POPSIZE
  if(claim < 1.0) rand = ind_uni();          //the claim is minimally 1 to allow 'not copying'
  else rand = ind_uni()*claim;

  std::list<Aapje*>::iterator it2 = this->copyspace_n.begin();
  for(std::vector<float>::iterator it3 = claim_array.begin(); it3 != claim_array.end(); ++it3)
    {
      if(rand<(*it3))                           //value pointed to by iterator to claim array
	{
	  this->demon = (*it2);  //KEEP THIS: this assigns chosen demon to memory

	  //update observables
	  this->last_chosen_dem[0] = (*it2)->food_target->type; //PROD/SUM: 0<x<MAX_RES
	  this->last_chosen_dem[1] = this->reliability[(*it2)->food_target->type]; //0<x<1
	  
	  this->last_chosen_dem[2] = 0; //init here; set below
	  this->last_chosen_dem[3] = (*it2)->age; //0<x<MAXAGE
	  //PROD/SUM: 0<x<1
	  float freq2 = (((float)eaten[(*it2)->food_target->type])/this->copyspace_n.size());

	  this->last_chosen_dem[4] = freq2; //0<x<1
	  //0<x<MAXAGE
	  this->last_chosen_dem[5] =(*it2)->total_processing_time[(*it2)->food_target->type];
	  float payoff = (*it2)->ExperienceBasedProcessingReward((*it2)->food_target->type);
	  this->last_chosen_dem[6] = payoff;
	  this->last_chosen_dem[7] = (*it2)->id;

	  this->chosen_dem[0]++; //PROD/SUM: 0<x<SAVE_TIME*E_P_M
	  this->chosen_dem[1]++; //PROD/SUM: 0<x<SAVE_TIME*E_P_M
	  if((*it2)->id==this->parent_id)
	    {
	      this->chosen_dem[2]++;  //PROD/SUM: 0<x<SAVE_TIME*E_P_M
	      this->last_chosen_dem[2] = 1;
	    }
	  this->chosen_dem[3]+=freq2;  //PROD/SUM: 0<x<SAVE_TIME*E_P_M
	  this->chosen_dem[4]+=this->reliability[(*it2)->food_target->type];
	  //PROD/SUM: 0<x<SAVE_TIME*E_P_M
	  this->chosen_dem[5]+=(*it2)->age; //PROD/SUM: 0<x<SAVE_TIME*E_P_M*MAXAGE
	  this->chosen_dem[6]+=(*it2)->total_processing_time[(*it2)->food_target->type];
	  //PROD/SUM: 0<x<SAVE_TIME*E_P_M*MAXAGE
	  this->chosen_dem[7]+=payoff; //PROD/SUM: 0<x<SAVE_TIME*E_P_M*MAXQ
	  
	  if((*it2)->action!=EAT)
	    {
	      std::cout<<"ERROR: neighbor in copyspace is not eating!\n";
	      std::cout<<"aapje.cpp: ChooseDemonstrator3()\n";
	      std::exit(0);
	    }
	  break;
	}
      ++it2;               //iterate through copyspace array which is parallel to claim array
    }
}

//Occurs in SocialLearning() if has POS stim gene, and has a demonstrator
void Aapje::TemporarySocialStimulus()
{
  if(this->time_since_stimulation==0) //only get new stimulus when old one runs out.
    {
      this->resource_stimulated = demon->food_target->type;    //this is reset only here
      this->time_since_stimulation = this->stimulus_duration;  //this is set to zero after choice
      //PROD/SUM: 0<x<SAVE_TIME*E_P_M
      this->num_stim++;                                        //number of social stimuli
    }
}

void Aapje::MakeNaiveAboutResource(int r)
{
  this->novel[r]=true;
  this->prefs[r]=0;
  this->reliability[r]=0.0;
  this->temp_aversion[r]=false;
  this->assoc_res_soc_total-=this->assoc_res_soc[r];
  this->assoc_res_soc[r]=0;
  for(int itt=0; itt<(int)this->perm_social_obs_n.size(); ++itt)
    {
      this->perm_social_obs_n_tot[itt]-=this->perm_social_obs_n[itt][r];
      this->perm_social_obs_n[itt][r]=0;
    }
    this->diet[r]=0;
    this->total_processing_events[r]=0;
    this->total_processing_time[r]=0;
}

float Aapje::ExperienceBasedProcessingReward(int type)
{
  //pref is a function of the number of times T an individual has tried a resource
  // pref = a*(T^n)/(h^n+T^n)
  // a = quality
  float quality = this->environment->resourcetypes[type]->quality;
  // h = the T for which pref is half maximal (how hard to learn)
  float h_pow_n = this->environment->resourcetypes[type]->h_pow_n;
  // n = how hard resource is to learn (how long before sharp rise in pref)
  float n = this->environment->resourcetypes[type]->n;

  //stored T: number of times eaten
  float T = 0;
  if(this->environment->SKILL_LEARN_TYPE==1)
    T = (float)this->total_processing_events[type]; //0<x<MAXAGE
  if(this->environment->SKILL_LEARN_TYPE==2)
    T = (float)this->total_processing_time[type];

  //then calculate new pref from T+1
  //check for INF pow(T,n)
  float pw = pow(T,n);          //PROD/SUM: 0<x<[MAXAGE^n]
  if(!std::isfinite(pw)) pw = std::numeric_limits<float>::max(); //this will set to max
  //Assumes that there is a maximal limit to experience (at which point reward no longer incr)
  float reward = (quality * ((pw)/(h_pow_n + pw)) ) + ind_nor()*L_NOISE; //PROD/SUM 0<x<Q + INF?
  
  while(!std::isfinite(reward))
    reward = (quality * ((pw)/(h_pow_n + pw)) ) + ind_nor()*L_NOISE; //PROD/SUM 0<x<Q + INF?
       
  return reward;
}

void Aapje::TrialLearning()//float reward)
{
  //Not used
  //if(this->environment->TRIAL_RATE_SWITCH==2)
  //{
  //  this->trial_rate += this->U*(reward - this->trial_rate);
  //}
  this->novel[this->food_target->type]=false;
}

void Aapje::UpdateSkill()
{
  if(this->environment->SKILL_LEARN)
    {
      //update skill experience
      this->total_processing_events[this->food_target->type]++;  //PROD/SUM: 0<x<MAXAGE
      float skill_inc = (float)this->t_e/this->environment->TIME_SCALAR + ind_gamma()*P_NOISE; //PROD/SUM: 0<x<MAXAGE+[INF]
      this->total_processing_time[this->food_target->type]=std::min(this->total_processing_time[this->food_target->type]+skill_inc, std::numeric_limits<float>::max());
    }
}

void Aapje::SkillLearning()
{
  this->UpdateSkill();

  float reward = ExperienceBasedProcessingReward(this->food_target->type); //this puts L_NOISE on reward, proportional to experience
  
  //Keeps track of what has been tried
  if(this->novel[this->food_target->type]) this->TrialLearning();//sets novel=false; and can change trial rate
 
  if(!this->environment->LEARN_UPDATE_SWITCH) this->prefs[this->food_target->type] = reward;
  else 
    {
      float diff = reward - this->prefs[this->food_target->type]; //PROD/SUM: 0<x<MAXQ
      this->prefs[this->food_target->type] += this->U*(diff); //PROD/SUM 0<x<MAXQ
      this->reliability[this->food_target->type] = this->reliability[this->food_target->type]*(1 - this->U) + this->U*(1 - std::min( (float)1.0, ( std::fabs(diff)/std::fabs(reward) ) ) );
      //PROD/SUM 0<x<1
      //this->personal_info_prediction += this->U*(reward - this->personal_info_prediction); //NOT USED
    }
  
  this->energy += reward;        //PROD/SUM 0<x<[MAX_QUALITY*MAX_AGE]  
  if(this->energy>std::numeric_limits<int>::max()) this->energy=std::numeric_limits<int>::max();
  this->energy_year += reward;   //PROD/SUM 0<x<[MAX_QUALITY*MAX_AGE]
  this->lifetime_energy += reward;
}

void Aapje::Eat()
{ 
  //NOT USED
  /*if(this->environment->SAVE_BEH_CHOICES)
    {
      if(this->food_target->type==this->resource_stimulated && this->time_since_stimulation>0)
	this->StoreBehaviourChoices(this->food_target->type, true, this->last_demon);
      else
	this->StoreBehaviourChoices(this->food_target->type, false, this->last_demon);
	}*/

  //std::cout<<"EAT\n";
  //DEPLETE RESOURCE ITEM
  this->food_target->GetsEaten(this->environment->global_time, (long)this->environment->YEAR, this->environment->resourcetypes[this->food_target->type]->residence_time);

  this->diet[this->food_target->type]++;  //PROD/SUM 0<x<[SAVE_TIME]

  //NOTE: LEARN_TYPE==0 occurs during digestion
  if(LEARN_TYPE==1)
    this->SkillLearning();

  //reset temp social stimulus
  if(this->food_target->type==this->resource_stimulated) // && this->time_since_stimulation>0)
    this->time_since_stimulation=0;

  this->t_a += (long)this->t_e; this->action=EAT; this->e++; //update action //PROD/SUM in bound

  this->stomach.push_back(this->food_target->type); this->stomach_tot++; //update stomach
  this->stomach_times.push_back(this->DIG_INT);              //for continous digestion
  if(this->stomach_tot>this->MAX_STOMACH)
    {
      std::cout<<"ERROR: ate more than MAX_STOMACH! (in aapjes:Eat()\n";
      std::exit(0);
    }
}

//DID: change to single option?
void Aapje::Search()
{
  this->food_target = PreferredResourceInSector(); //geom.cpp

  //NOT USED
  //std::cout<<"SEARCH\n";
  /*switch(SEARCH_TYPE)
    {
    case 0:
      this->food_target = PreferredResourceInSector(); //geom.cpp
      break;
    case 1:
      {
	this->food_target = RandomResourceInSector(Sector(this->pos, this->d_f, this->dir, this->half_a_f)); //geom.cpp
	break;
      }
    case 2:
      {
	this->food_target = ClosestResourceInSector(Sector(this->pos, this->d_f, this->dir, this->half_a_f)); //geom.cpp
	break;
      }
     default:
      std::cout<<"ERROR: should not reach this default in switch function\n";
      std::cout<<"SEARCH_TYPE probably not between 0 and 2\n";
      std::cout<<"aapjes.cpp (SEARCH)\n";
      std::exit(0);
      }*/

  //NOT USED
  /*if(PREFEXP_UPDATE_STYLE==0)
    {
      if(this->food_target && this->prefs[this->food_target->type] > this->pref_exp)
	this->pref_exp = this->prefs[this->food_target->type];
      else 
	this->pref_exp = this->pref_exp_update*this->pref_exp; //PROD/SUM 0<x<MAX_QUALTIY
	}*/

  this->t_a += (long)this->t_f; this->action = SEARCH; this->fs++; //PROD/SUM 0<x<[MAX_AGE*SCALAR]
}

//Only '0' is used
//NOT USED: simplified to remove switch calculations
/*float Aapje::EffectSocialStimulus(float food_choice_prob)
{
  switch(SOCIAL_STIMULUS_STYLE)
    {
    case 0: //standard van der Post et al. 2009; van der Post & Hogeweg 2009
      return food_choice_prob + this->temp_social_stimulus;  //PROD/SUM 0<x<2
    case 1: //proportional to difference to max: p = p + gamma*(1-p)
      return food_choice_prob + (this->temp_social_stimulus*(1 - food_choice_prob)); //PROD/SUM 0<x<1
    case 2: //proportional to square diff ot max: p = p + gamma*(1 - p^2);
      return food_choice_prob + (this->temp_social_stimulus*(1 - (food_choice_prob*food_choice_prob)));  //PROD/SUM 0<x<1
    case 3: //p = p + gamma*p*(1-p)
      return food_choice_prob + (this->temp_social_stimulus*food_choice_prob*(1-food_choice_prob)); //PROD/SUM 0<x<1
    case 4:
      //soc stimulus added to pref: ???
      return food_choice_prob;
    case 5:
      //pref incremented to level of demonstrator
      return food_choice_prob;
    default:
      std::cout<<"ERROR: should not reach this default in switch function\n";
      std::cout<<"SOCIAL_STIMULUS_STYLE probably not between 0 and 3\n";
      std::cout<<"aapjes.cpp (effectsocialstimulus)\n";
      std::exit(0);
    }
    }*/

float Aapje::FoodChoiceProbability(int type)
{
  float food_choice_prob=0;
 
  if(this->temp_aversion[type]==false) //not having temporary satiation aversion
    {
      if(!this->novel[type]) //if not novel (tried before
	{
	  this->predicted_reward = this->prefs[type] + (ind_nor()*L_NOISE); //PROD/SUM 0<x<[MAXQUALTIY+L_NOISE] 

	  if(!std::isfinite(this->predicted_reward)) //In case INF
	    {
	      if(this->predicted_reward<0) this->predicted_reward = -std::numeric_limits<float>::max();
	      else this->predicted_reward = std::numeric_limits<float>::max();
	    }

	  if(this->predicted_reward>0) //if can use prefexp
	    {
	      //if pref exp is above minimum && reward is less
	      if(this->pref_exp>std::numeric_limits<float>::min() && this->predicted_reward<this->pref_exp)
		{
		  food_choice_prob = pow(this->predicted_reward/this->pref_exp, 
					 this->foodchoice_selectivity);	    
		  //PROD/SUM 0<x<1  (since pred_reward < pref_exp && pred_reward > 0)
		}
	      else //will always take the resource (i.e. minimally selective)
		food_choice_prob = 1.0;
	    }
	  else //once reach rock-bottom of prefexp -> eat everything (if not negative)
	    if(this->predicted_reward==0 && this->pref_exp<=std::numeric_limits<float>::min()) 
	      food_choice_prob=1.0;
	}
      else //if novel, has no expected reward
	this->predicted_reward = 0; //and food_choiceprob remains 0;
      
      //TEMPORARY SOCIAL STIMULUS
      //effect of temporary social stimulus
      this->copy_se_flag=false;
      if(this->environment->TEMP_STIMULUS_SWITCH)
	if(type==this->resource_stimulated && this->time_since_stimulation>0)
	  {
	    //std::cout<<"FC: will add effect of SocStim\n";
	    food_choice_prob += this->temp_social_stimulus;
	    //this->EffectSocialStimulus(food_choice_prob);
	    this->copy_se_flag=true;
	  }

      //EXPLORATION
      //basic exploration rate (potentially reduced by reliability estimate of personal info)
      if(food_choice_prob<1.0)
	food_choice_prob += this->explore_rate*(1 - this->explore_stimulus*this->reliability[type]);
      //PROD/SUM: 0<x<[EXPLORE_RATE]
      
    }
  //else has temp aversion and foodchoiceprob = 0;
      
  return food_choice_prob; //can be bigger than one, but inconsequentially
}

int Aapje::SelectVisibleResources(std::vector<Resource*> *resources, std::vector<Resource*> *visible)
{
  Vec2d LV = this->dir.rotate(this->half_a_f)*this->d_f;
  Vec2d RV = this->dir.rotate(-this->half_a_f)*this->d_f;
  //PROD/SUM: -d_f<x<d_f

  int visible_size=0;
  for(std::vector<Resource*>::iterator it = resources->begin(); it != resources->end(); ++it)
    {
      //faster to do distance before checking if available
      float xvec = (*it)->position.x - this->pos.x;  //PROD/SUM: -d_f<x<d_f
      if( xvec > this->d_f) continue;
 
      float yvec = (*it)->position.y - this->pos.y;  //PROD/SUM: -d_f<x<d_f
      if( yvec > this->d_f) continue;

      if(((xvec*xvec)+(yvec*yvec)) > this->d_fsq) //if too far   //PROD/SUM 0<x<[d_f^2 + d_f^2]
	continue;   
      
      if( !this->environment->ResourceIsAvailable(*it) )//updates + checks if available
	continue;
      
      //based on withinsector2 (dot product)
      //Check if left of LV
      if( ((LV.x*yvec) - (LV.y*xvec)) > 0) continue;   //PROD/SUM: -[d_f^3+d_f^3]<x<[d_f^3+d_f^3]
      //Check if right of s.center - RP
      if( ((RV.x*yvec) - (RV.y*xvec)) < 0) continue;   //PROD/SUM: 0<x<d_f^3+d_f^3
      
      visible->push_back(*it);  //store resource if potentially visible
      visible_size++;
    }
  return visible_size;
}

Resource* Aapje::PreferredResourceInSector()
{      
  int visible_size=0;
  if(this->moved) //if moved have to search again for resources; if didnt -> use stored
    {
      std::vector<Resource*> resources;
      resources.reserve(100);
      //std::cout<<"Ind "<<this->id<<" did move, must search again\n";      

      //PROD/SUM: -d_f<x<[FIELD-d_f]
      int minx = (int)(this->pos.x-this->d_f); int miny = (int)(this->pos.y-this->d_f);
      //PROD/SUM: 0<x<FIELD+d_f
      int maxx = (int)(0.5+(this->pos.x+this->d_f));int maxy = (int)(0.5+(this->pos.y+this->d_f));
      this->environment->ResourcesWithinGRID(&resources, minx, maxx, miny, maxy);
      this->visible_resources.clear();

      if(!resources.empty()) {
	visible_size = this->SelectVisibleResources(&resources, &this->visible_resources);     
	this->moved=false; //fresh new session
      } 
    }
  else //if have not moved used stored memory
    {
      //remove resource items eaten by someone else in the mean time
      std::vector<Resource*>::iterator it = this->visible_resources.begin();
      while(it!=this->visible_resources.end())
	{
	  if(!this->environment->ResourceIsAvailable(*it))
	    {
	      this->visible_resources.erase(it); //iterator goes to next
	    }
	  else ++it;
	  }
      visible_size = this->visible_resources.size();     
    }	    
  
  // - check whether chosen to eat
  int items_assessed = 0;
  int ran=0; 
  while (!this->visible_resources.empty() && items_assessed < MAX_ITEMS_ASSESSED_PER_SEARCH )
    {
      //PROD/SUM: 0<x<[5*5*LOCAL_DENSITY]
      if(visible_size>1) ran = ind_uni()*visible_size; //select random resource in view
      else ran=0;
      int type = this->visible_resources[ran]->type;
      
      if(ind_uni()<this->FoodChoiceProbability(type)) 
	//includes exploration, exp reward + temp social stimulus
	{
	  //store stimulus enhancement 'effective' copy event
	  if(this->copy_se_flag)
	    {
	      //std::cout<<"PrefRes: selected res with soc stim\n";
	      this->tot_copy_se[0]++;  //PROD/SUM: 0<x<SAVE_TIME*E_P_M
	      if((int)this->last_chosen_dem[7] == this->parent_id) 
		this->tot_copy_se[1]++; //PROD/SUM: 0<x<SAVE_TIME*E_P_M
	      this->tot_copy_se[2]+=this->reliability[type]; //PROD/SUM: 0<x<SAVE_TIME*E_P_M
	      this->tot_copy_se[3]+=this->last_chosen_dem[3];
	      this->tot_copy_se[4]+=this->last_chosen_dem[4];
	      this->tot_copy_se[5]+=this->last_chosen_dem[5];
	      this->tot_copy_se[6]+=this->last_chosen_dem[6];
	      this->copy_se_flag=false;
	    }

	  return this->visible_resources[ran]; //stop searching
	}
      else //did not select item
	{
	  this->visible_resources.erase(this->visible_resources.begin()+ran);
	  items_assessed++; //PROD/SUM: 0<x<[5*5*LOCAL_DENSITY]
	  visible_size--;
	}
    }    

  return NULL; //if did not select any item for eating
}

//NOT USED
/*Resource* Aapje::RandomResourceInSector(Sector s)
{
  std::vector<Resource*> random;
  Rectangle r = s.mbr();  
  std::vector<Resource*> resources;
  if(!RSTAR) resources = this->environment->ResourcesWithinGRID((int)r.min.x-1, (int)r.max.x+1, (int)r.min.y-1, (int)r.max.y+1);
  else resources = this->environment->ResourcesWithin(r); //environment.cpp
  float max_d_sq = s.radius * s.radius;
  float distsq = max_d_sq; 

  for(unsigned i = 0; i< resources.size(); i++)
    {
      Resource* r = resources[i];
      r->UpdateAppearTime(this->environment->global_time, (long)this->environment->YEAR, this->environment->resourcetypes[r->type]->residence_time);

      float newdistsq = r->position.sqdist(this->pos);

      if(  !this->environment->ResourceIsAvailable(r)
	  || 
	  newdistsq > distsq || !r->position.within_sector(s) ) //geom.cpp
	continue;

      random.push_back(r);  //store resource if potentially visible
    }

  if(random.size()>0)
    {
      int ran = ind_uni()*random.size(); //select random resource in view  //PROD/SUM: 0<x<[5*5*LOCAL_DENSITY]
      return random[ran]; //stop searching
    }
  else
    return NULL; //should only reach here if did not find resource
}

//NOT USED
Resource* Aapje::ClosestResourceInSector(Sector s)
{
  std::vector<Resource*> closest;
  Rectangle r = s.mbr();
  std::vector<Resource*> resources;
  if(!RSTAR) resources = this->environment->ResourcesWithinGRID((int)r.min.x-1, (int)r.max.x+1, (int)r.min.y-1, (int)r.max.y+1);
  else resources = environment->ResourcesWithin(r);
  float max_d_sq = s.radius * s.radius;
  float distsq = max_d_sq;
  
  for(unsigned i = 0; i< resources.size(); i++)
    {
      Resource* r = resources[i];
      r->UpdateAppearTime(this->environment->global_time, (long)this->environment->YEAR, this->environment->resourcetypes[r->type]->residence_time);
      float newdistsq = r->position.sqdist(pos);

      if(  !this->environment->ResourceIsAvailable(r)
	  || 
	  newdistsq > distsq || !r->position.within_sector(s) ) //geom.cpp
	continue;
      
      if( newdistsq < distsq ) //TODO: can add distance dependent detection here
	{
	  closest.clear();
	  distsq = newdistsq;
	  closest.push_back(r);
	}
      else 
	if ( newdistsq == distsq ) //TODO: can add distance detection here too
	  {
	    closest.push_back(r);
	  }
    }

  if(closest.size()>0)
    {
      int ran_r = (int)(ind_uni()*closest.size());
      return closest[ran_r];
    }
  else      
    {
      return NULL;
    }
    }*/

void Aapje::MoveToFood()
{
  const bool CHECKS=false;

  //std::cout<<"MOVETOFOOD\n";
  Vec2d foodvec;
  foodvec = this->food_target->position - this->pos;  //PROD/SUM: -d_f<x<-d_r & d_r<x<d_f
  float m_dist = foodvec.length();                    //PROD/SUM: d_r<x<d_f

  //PROD/SUM: -1<x<1
  this->dir = (foodvec)*(1.0/m_dist); //normalize (to not call length() twice) 
  if(CHECKS && (isnan(this->dir.x ) || isnan(this->dir.y))) 
    {
      std::cout<<"aapjes.cpp: MoveToFood(): direction vector NAN "<<this->dir.x<<" "<<this->dir.y<<std::endl;   
      std::exit(0);
    }

  //PROD/SUM: 0<x<[d_f-[d_r-0.01]]
  m_dist = m_dist - (this->d_r-0.01); //subtract reach from distance to get minimal distance to reach food 
  Point2d new_pos;
      
  if(m_dist>=this->d_m)
    {
      new_pos = this->pos + (this->dir*this->d_m);  //PROD/SUM: 0<x<FIELD
      m_dist=this->d_m;
      this->action=MOVETOFOOD; this->t_a += (long)(this->d_m*this->s_m); this->mtf++;
    }
  else
    {
      new_pos = this->pos + (this->dir*m_dist);    //PROD/SUM: 0<x<FIELD
      this->action=MOVETOFOOD; this->t_a += (long)(m_dist*this->s_m); this->mtf++;
    }
  
  this->Move(new_pos, MOVETOFOOD);      

  if(CHECKS && this->food_target==NULL)
    {
      std::cout<<"MTF: no food target ERROR!\n";
      std::exit(0);
    }
  
  if(CHECKS && !this->environment->CheckPositionIsOnGrid(food_target->position))
    {
      std::cout<<"MTF: food is not on field! ERROR! this->environment->XFIELD "<<this->environment->XFIELD<<" this->environment->YFIELD "<<this->environment->YFIELD<<"\n";
      std::exit(0);
    }


  if(CHECKS && !this->environment->CheckPositionIsOnGrid(this->pos))
    {
      std::cout<<"MTF: moved off field! ERROR!\n";
      std::exit(0);
    }
}

void Aapje::MoveForward()
{
  //std::cout<<"MOVEFORWARD\n";
  Point2d new_pos = this->pos + (this->dir*this->d_m);  //PROD/SUM 0<x<FIELD+d_m
  this->Move(new_pos, MOVEFORWARD);
  this->action=MOVEFORWARD; this->t_a += (long)(this->d_m*this->s_m); this->mf++;
}

void Aapje::Move(Point2d new_pos, Action action)
{
  bool on_grid;
  if(this->free_moves<=0) //free moves was added to reduce frequency of having to check if on grid
    {
      on_grid = this->environment->CheckPositionIsOnGrid(new_pos);

      if(!on_grid)
	{
	  switch(action)
	    {
	    case MOVEFORWARD:
	      //okay - can be off field: will choose random heading
	      break;
	    case MOVETOFOOD:
	      //not okay - food should not be off field
	      std::cout<<"ERROR: position to Move() to is off the grid despite doing MOVETOFOOD!\n";
	      std::cout<<"aapjes.cpp: Move() called by MoveToFood() ****************************\n";
	      std::exit(0);
	      break;
	    case MOVETOGROUP:
	      //okay: can move off grid due to large move distance, in that case move random with small distance
	      break;
	    default:
	      //not okay: should not reach here
	      std::cout<<"ERROR: gets to 'default' is switch in Move() *****\n";
	      std::cout<<"ERROR: should only get to Move() with MOVE_ action\n";
	      std::cout<<"aapjes.cpp: Move() called by MoveToFood() ********\n";
	      std::exit(0);
	    }
	}
          
      while(!on_grid)
	{
	  SetRandomHeading();      
	  new_pos = this->pos + (this->dir*this->d_m);
	  on_grid = this->environment->CheckPositionIsOnGrid(new_pos);
	}  
      
      //check how many free moves are possible (i.e. the shortest path to the edge of field)
      int minx = (int)this->pos.x;                    //0<x<XFIELD-1
      int miny = (int)this->pos.y;
      int maxx = (this->environment->XFIELD) - minx;  //PROD/SUM: 1<x<XFIELD
      int maxy = (this->environment->YFIELD) - miny;

      int min_min = std::min(minx,miny)-1;    //PROD/SUM: -1<x<XFIELD-2
      int min_max = std::min(maxx, maxy)-2;   //PROD/SUM: -1<x<XFIELD-2    a safety margin
      
      this->free_moves = std::min(min_min, min_max); //PROD/SUM: -1<x<XFIELD-2
    }
  
  this->pos = new_pos;
  this->moved=true;
  this->environment->groups[this->group_id].moved = true;

  this->free_moves--; //PROD/SUM: -2<x<XFIELD-3

#ifdef _GRAPHICS_
  this->traj->push_front(pos);
  this->traj->pop_back();
#endif	  
}

void Aapje::Nothing()
{
  //std::cout<<"NOTHING\n";
  this->t_a += (long)t_n; this->action=NOTHING; this->n++;
}

//////////////// DISCRETE UPDATING /////////////////////////////

void Aapje::DiscreteUpdate()
{
  //PER MINUTE UPDATING
  //lifehistory
  this->age++;  //PROD/SUM: 0<x<MAXAGE
  this->energy -= this->metabolic_costs;
  if(this->time_since_stimulation>0) this->time_since_stimulation--; //social stimulus

  //digestion + learning
  if(PREFEXP_UPDATE_STYLE!=3)
    {
      if(this->environment->global_time%this->DIG_INT == 0)
	{
	  this->PreferenceExpectationUpdate(); //based on fullness stomach
	  this->DigestAndLearn();              //empties stomach (includes energy gain for LEARNTYPE==0)
	}
    }
  else
    //if(PREFEXP_UPDATE_STYLE==3) //continuous and on stomach fill dependency
    {
      this->DigestContinuously();
      this->AdjustSelectivityBasedOnStomachContent();
    }

  //Output of data
  this->DataOutput();  
}

void Aapje::DigestContinuously()
{
  //go through stomach contents - decrement counter
  std::vector<int>::iterator it = this->stomach.begin();
  std::vector<int>::iterator it2 = this->stomach_times.begin();
  while(it != this->stomach.end()) //if not empty
    {
      (*it2)--;
      if((*it2)==0)
	{
	  //Return of erase: An iterator pointing to the new location of the element that followed the last element erased by the function call. This is the container end if the operation erased the last element in the sequence.
	  it = this->stomach.erase(it); //now points to next element
	  it2 = this->stomach_times.erase(it2);
	  this->stomach_tot--;  //PROD/SUM: 0<x<MAX_STOMACH-1
	  //get energy!?

	}
      else //check outcome for iterator of erase
	{
	  ++it;
	  ++it2;
	}
    }

  //if zero:
  //remove + add 
}

void Aapje::AdjustSelectivityBasedOnStomachContent()
{
  //udpate stomach content estimate
  this->stomach_content_estimate = 
    (1-this->lambda_stomach)*this->stomach_content_estimate +
    this->lambda_stomach*this->stomach_tot;
  //PROD/SUM: 0<x<MAX_STOMACH
  
  //update selectivity PROD/SUM: 0<x<[MAXSTOMACH^20]
  float stomach_pow_N = pow(this->stomach_content_estimate,this->pref_exp_N); //can be precalculated
  float pref_update = (2*this->pref_exp_K*(stomach_pow_N/(stomach_pow_N + this->pref_exp_H_N))) - this->pref_exp_K;  //PROD/SUM: -pref_exp_k<x<[pref_exp_k]
  if(pref_update<0)
    this->pref_exp += pref_update*this->pref_exp;  //PROD/SUM: 0<x<INF: checks for INF below
  else
    this->pref_exp += std::max(pref_update*this->pref_exp, std::numeric_limits<float>::min());
  //PROD/SUM: min_float<x<INF
  //NOTES: checks for INF below

  //PROD/SUM: min_float<x<INF
  this->pref_exp = std::max(this->pref_exp, std::numeric_limits<float>::min());
  //PROD/SUM: min_float<x<max_float (INF taken care of here)
  this->pref_exp = std::min(this->pref_exp, std::numeric_limits<float>::max());
}

//NOT USED
/*void Aapje::StoreBehaviourChoices(int r, bool sociallystimulated, Aapje* demon)
{
  float temp_pref = pow((this->prefs[r]/this->pref_exp),this->foodchoice_selectivity);
  if(this->novel[r]) temp_pref = this->trial_rate;

  float asoc_prob = temp_pref;
  float soc_prob = 0; 
  if(this->environment->SOCIAL_STIMULUS_SWITCH==0) soc_prob = asoc_prob;
  if(this->environment->SOCIAL_STIMULUS_SWITCH==1) soc_prob = asoc_prob + this->temp_social_stimulus;
  if(this->environment->SOCIAL_STIMULUS_SWITCH>1) soc_prob = pow(((temp_pref+this->temp_social_stimulus)/this->pref_exp),this->foodchoice_selectivity);

  if(this->environment->P_SOCIAL_STIMULUS_SWITCH>0 && this->assoc_res_soc[r]>0) soc_prob += this->perm_social_stimulus;

  if(asoc_prob>1) asoc_prob=1;
  if(asoc_prob<0) asoc_prob=0;
  if(soc_prob>1) soc_prob=1;
  if(soc_prob<0) soc_prob=0;

  if(sociallystimulated)
    {
      this->soc_stim++;
      //std::cout<<"1 Socially stimulated\n";
    }

  this->prob_asoc += asoc_prob;
  if(asoc_prob<1.0 && sociallystimulated)  
    {
      this->soc_eat++;      
      //std::cout<<"2 Social stimulus not redundant\n";
      this->effect_soc += soc_prob-asoc_prob;	
      this->pref_tot_soc+=this->prefs[r];
      this->fam_tot_soc+=(1-((this->environment->resourcetypes[r]->quality - this->prefs[r])/this->environment->resourcetypes[r]->quality));
      this->sel_soc += (this->pref_exp - this->prefs[r]);
      this->res_qual_soc += this->environment->resourcetypes[r]->quality;

      this->dem_age += (float)demon->age/this->environment->YEAR;
      this->dem_fam += (1-((this->environment->resourcetypes[r]->quality - demon->prefs[r])/this->environment->resourcetypes[r]->quality));
      if(this->prefs[r]<this->environment->resourcetypes[r]->quality)
	{
	  this->soc_eat_and_learn++;
	}
    }
  else
    {
      this->asoc_eat++;
      this->pref_tot_asoc+=this->prefs[r];
      this->fam_tot_asoc+=(1-((this->environment->resourcetypes[r]->quality - this->prefs[r])/this->environment->resourcetypes[r]->quality));
      this->sel_asoc += (this->pref_exp - this->prefs[r]);
      this->res_qual_asoc += this->environment->resourcetypes[r]->quality;
    }
    }*/

/*void Aapje::DataOutputBehaviourChoice()
{
  
  std::cout<<"B "
	   <<(float)this->environment->global_time/this->environment->YEAR<<" " //1 TIME
	   <<this->id<<" "                                                      //2 ID
	   <<(float)this->age/this->environment->YEAR<<" "                      //3 AGE
	   <<this->pref_exp<<" "                                                //4 pref exp
	   <<this->asoc_eat<<" "                                                //5 Asoc EAT: asocial eat events (no effect social stimulus)
	   <<this->soc_eat<<" ";                                                //6 Soc EAT: social eat events (social stimulus has an impact)
  if(asoc_eat>0){
    std::cout<<(float)this->soc_eat/(this->soc_eat+this->asoc_eat)<<" "        //7 frac soc
	     <<this->prob_asoc/(this->asoc_eat+this->soc_eat)<<" "             //8 prob asoc: average probably of choosing asocially
	     <<this->pref_tot_asoc/this->asoc_eat<<" "                         //9 avg asoc pref
	     <<this->fam_tot_asoc/this->asoc_eat<<" "                          //10 avg asoc fam 
	     <<this->sel_asoc/this->asoc_eat<<" "                              //11 avg sel asoc
	     <<this->res_qual_asoc/this->asoc_eat<<" ";                        //12 avg qual asoc
      }
  else {
    std::cout<<0<<" "                         //7 frac soc
	     <<0<<" "                         //8 avg asoc pref
	     <<0<<" "                         //9 avg asoc pref
	     <<0<<" "                         //10 avg asoc fam 
	     <<0<<" "                         //11 avg sel asoc
	     <<0<<" ";                        //12 avg qual asoc
      }
  if(soc_eat>0){
    std::cout<<this->pref_tot_soc/this->soc_eat<<" "                            //13 avg soc pref
	     <<this->fam_tot_soc/this->soc_eat<<" "                             //14 avg soc fam 
	     <<this->sel_soc/this->soc_eat<<" "                                 //15 avg sel soc
	     <<this->res_qual_soc/this->soc_eat<<" "                            //16 avg qual soc
	     <<this->dem_age/this->soc_eat<<" "                                 //17 avg dem age
	     <<this->dem_fam/this->soc_eat<<" "                                 //18 avg dem fam
	     <<this->effect_soc/this->soc_eat<<" ";                             //19 avg effect social stimulus: soc_prob - asoc_prob
  }
  else{
    std::cout<<0<<" "                            //13 avg soc pref
	     <<0<<" "                            //14 avg soc fam 
	     <<0<<" "                            //15 avg sel soc
	     <<0<<" "                            //16 avg qual soc
	     <<0<<" "                            //17 avg dem age
	     <<0<<" "                            //18 avg dem fam
	     <<0<<" ";                           //19 avg effect soc
  }
  std::cout<<this->soc_stim<<" ";                //20 number of times eats socially stimulated
  std::cout<<this->num_stim<<" ";                //21 number of times socially stimulated
  std::cout<<this->soc_eat_and_learn<<" ";      //22 number of time p+C>p and pref[r]<quality[r]
  std::cout<<std::endl;

  this->prob_asoc=0;
  this->effect_soc=0;
  this->asoc_eat=0;
  this->soc_eat=0;
  this->soc_eat_and_learn=0;
  this->soc_stim=0;
  this->num_stim=0;
  this->pref_tot_asoc=0;
  this->fam_tot_asoc=0;
  this->sel_asoc=0;
  this->res_qual_asoc=0;
  this->pref_tot_soc=0;
  this->fam_tot_soc=0;
  this->sel_soc=0;
  this->res_qual_soc=0;
  this->dem_age=0;
  this->dem_fam=0;
  }*/

void Aapje::DataOutputDeath()
{
  std::cout<<"L "
	   <<(float)this->environment->global_time/this->environment->YEAR<<" " //1 TIME
	   <<this->id<<" "                                                      //2 ID
	   <<(float)this->age/this->environment->YEAR<<" "                      //3 AGE
	   <<this->energy<<" "                                                  //4 ENERGY
	   <<this->lifetime_energy/this->age<<" "                               //5 AVG ENERGY
	   <<this->parent_id<<" "                                               //6 MOTHER
	   <<this->initial_trial_rate<<" "                                      //7 TRIAL RATE
	   <<this->pref_exp_update<<" "                                         //8 PREFEXP UPD
	   <<this->foodchoice_selectivity<<" "                                  //9 SELECTIVITY
	   <<this->offspring<<" "                                               //10 offspring
	   <<this->init_pref_exp<<" "                                           //11 initprefexp
	   <<this->temp_social_stimulus<<" "                                    //12 T soc stim
	   <<this->perm_social_stimulus<<" "                                    //13 P soc stim
	   <<this->d_mf<<" "                                                    //14 d_mf
	   <<this->U<<" "                                                       //15 U learn rate
	   <<this->pe_u_n<<" "                                                  //16 pe update N
	   <<this->pe_u_h<<" "                                                  //17 pe update H
	   <<this->assoc_res_soc_learn_rate<<" "                                //18 cont learn
	   <<this->min_pref_exp<<" "                                            //19 min prefexp
	   <<this->U_TS<<" "                                                    //20 temp stim
	   <<this->U_PS<<" "                                                    //21 perm stim
    	   <<this->U_TS_N<<" "                                                  //22 temp stim N
    	   <<this->U_PS_N<<" "                                                  //23 temp stim N
	   <<this->U_TS_INIT<<" "                                               //24 temp stim
	   <<this->U_PS_INIT<<" "                                               //25 perm stim
    	   <<this->U_TS_N_INIT<<" "                                             //26 temp stim N
    	   <<this->U_PS_N_INIT<<" "                                             //27 temp stim N
	   <<this->explore_rate<<" "                                            //28 explore rate
	   <<this->explore_stimulus<<" "                                    //29 explore stimulus
	   <<this->pref_exp_N<<" "                                              //30 pref exp N
	   <<this->pref_exp_K<<" "                                              //31 pref exp K
	   <<this->pref_exp_H<<" "                                              //32 pref exp H   
	   <<this->lambda_stomach<<" "                                          //33 lambdastomac
	   <<this->l_r<<" "                                                     //34 L_R
 	   <<this->l_c<<" "                                                     //35 L_C
	   <<this->l_M<<" "                                                     //36 L_M
	   <<this->l_f<<" "                                                     //37 L_f
	   <<this->l_a<<" "                                                     //38 L_a
	   <<this->l_t<<" "                                                     //39 L_t
	   <<this->l_p<<" "                                                     //40 L_p
	   <<this->skill_copy_rate<<" "                                        //41 skill copy rate
	   <<this->s_k<<" "                                            //42 weighting of skill copy
	   <<this->t_o<<" "                                                   //43 observation time
	   <<std::endl;
}

void Aapje::DataOutput()
{
  /*if(this->environment->SAVE_BEH_CHOICES
     && 
     this->environment->global_time%(this->environment->SAVE_TIME)==0)
    {
      this->DataOutputBehaviourChoice();      
      }*/

  if(
     (//this->environment->global_time<(unsigned long)this->environment->YEAR 
      //&& this->environment->global_time%this->environment->DAY==0)
      this->environment->global_time%(this->environment->SAVE_TIME)==0)
     ||
     this->environment->global_time%this->environment->YEAR==0
     )
    {
      //print out for each individual its prefs and diet

      float temp_time = ((float)this->environment->global_time)/this->environment->YEAR;
      temp_time = temp_time - (int)temp_time;
      if(temp_time==0) temp_time=1; //end of year result rather than beginning of year
      //std::cout<<"time "<<(float)this->environment->global_time/this->environment->YEAR<<" temptime "<< temp_time <<" energy "<<this->energy_year<<" energy/temptime "<< this->energy_year/temp_time<<"\n";
      //std::cin.get();
      
      if(this->environment->SAVE_PREFS)
	{
	  //PREF, time, ind, age, avg_energy, prefs[1-250]
	  std::cout<<"P "<<(float)this->environment->global_time/this->environment->YEAR<<" "<<this->id<<" "<<(float)this->age/this->environment->YEAR<<" "<<this->energy_year/temp_time;
	  for(int p=0; p<(int)this->environment->resourcetypes.size(); p++)
	  std::cout<<" "<<this->prefs[p];
	  std::cout<<std::endl;

	  std::cout<<"T "<<(float)this->environment->global_time/this->environment->YEAR<<" "<<this->id<<" "<<(float)this->age/this->environment->YEAR<<" "<<this->energy_year/temp_time;
	  for(int p=0; p<(int)this->environment->resourcetypes.size(); p++)
	  std::cout<<" "<<this->total_processing_time[p];
	  std::cout<<std::endl;
	}

      if(this->environment->SAVE_BEH_ENERGY)
	{
	  std::cout<<"A "
		   <<(float)this->environment->global_time/this->environment->YEAR<<" " //1 TIME
		   <<this->id<<" "                                    //2 ID
		   <<(float)this->age/this->environment->YEAR<<" "    //3 AGE
		   <<this->n<<" "                                     //4 nothing events
		   <<this->mf<<" "                                    //5 move forward events
		   <<this->mtf<<" "                                   //6 move to food events
		   <<this->mg<<" "                                    //7 move to group events
		   <<this->fs<<" "                                    //8 food search events
		   <<this->e<<" "                                     //9 eat events
		   <<this->environment->aapjes.size()<<" "            //10 population
		   <<this->energy<<" "                                //11 energy level
		   <<this->initial_trial_rate<<" "                    //12 trial rate
		   <<this->pref_exp_update<<" "                       //13 pref exp update
		   <<this->foodchoice_selectivity<<" "                //14 foodchoice selectivity
		   <<this->init_pref_exp<<" "                         //15 init_pref_exp
		   <<this->energy_year/temp_time<<" "                 //16 energy_year/temptime
		   <<this->group_id<<" "                              //17 group_id
		   <<this->temp_social_stimulus<<" "                  //18 soc stim
		   <<this->perm_social_stimulus<<" "                  //19 soc stim
		   <<this->d_mf<<" "                                  //20 move forwd dist
		   <<this->trial_rate<<" "                            //21 adaptable trial rate
		   <<this->U<<" "                                     //22 U learn rate
		   <<this->pref_exp<<" "                              //23 pref exp
		   <<this->pe_u_n<<" "                                //24 pref exp update N

		   <<this->pe_u_h<<" "                                //25 pref exp update H
		   <<this->assoc_res_soc_learn_rate<<" "              //26 assocresscolearnrate
		   <<this->environment->groups[this->group_id].members.size()<<" "  
	                                                              //27 group size
		   <<this->min_pref_exp<<" "                          //28 min pref exp
		   <<this->U_TS<<" "                                  //29 temp stim
		   <<this->U_PS<<" "                                  //30 perm stim
		   <<this->choice_asoc<<" "                           //31 choice asoc
		   <<this->choice_tsoc<<" "                           //32 choice tsoc
		   <<this->choice_psoc<<" "                           //33 choice psoc
		   <<this->personal_info_prediction<<" "              //34 personal info predict
		   <<this->U_TS_N<<" "                                //35 learn rate ts N
		   <<this->choice_tsoc_n<<" "                         //36 choice tsoc_n
		   <<this->U_PS_N<<" "                                //37 learn rate ts N
		   <<this->choice_psoc_n<<" "                         //38 choice psoc_n    
		   <<this->U_TS_INIT<<" "                             //39 temp stim
		   <<this->U_PS_INIT<<" "                             //40 perm stim
		   <<this->U_TS_N_INIT<<" "                           //41 temp stim N
		   <<this->U_PS_N_INIT<<" "                           //42 temp stim N
		   <<this->explore_rate<<" "                          //43 explore rate
		   <<this->explore_stimulus<<" "                      //44 explore stimulus
		   <<this->pref_exp_N<<" "                            //45 pref exp N
		   <<this->pref_exp_K<<" "                            //46 pref exp K
		   <<this->pref_exp_H<<" "                            //47 pref exp H   
		   <<this->lambda_stomach<<" "                        //48 lambdastomach
		   <<this->l_r<<" "                                   //49 L_R
		   <<this->l_c<<" "                                   //50 L_C
		   <<this->l_M<<" "                                   //51 L_M
		   <<this->l_f<<" "                                   //52 L_f
		   <<this->l_a<<" "                                   //53 L_a
		   <<this->l_t<<" "                                   //54 L_t
		   <<this->l_p<<" "                                   //55 L_p
		   <<this->skill_copy_rate<<" "                       //56 skill copy rate
		   <<this->s_k<<" "                                   //57 weighting of skill copy
		   <<this->t_o<<" "                                   //58 observation time
		   <<std::endl;
	  this->n=0; this->mf=0; this->mtf=0; this->mg=0; this->fs=0; this->e=0; this->o=0;
	  this->choice_asoc=0; this->choice_tsoc=0; this->choice_psoc=0; this->choice_tsoc_n=0; this->choice_psoc_n=0;
	}

      if(this->environment->SAVE_GROUP_IND)
	{
	  std::cout<<"GSI "<<(float)this->environment->global_time/this->environment->YEAR<<" "<<this->id<<" "<<this->maxview_n.size()<<" "<<this->pos.x<<" "<<this->pos.y;
	  for(std::list<Aapje*>::iterator n=this->maxview_n.begin(); n!=this->maxview_n.end(); ++n)
	    std::cout<<" "<<(*n)->id;
	  std::cout<<std::endl;
	}
      
      if(this->environment->SAVE_DIET)
	{
	  int tot_diet=0;
	  std::cout<<"D "<<(float)this->environment->global_time/this->environment->YEAR<<" "<<this->id<<" "<<(float)this->age/this->environment->YEAR<<" "<<this->energy_year/temp_time;
	  for(int p=0; p<(int)this->environment->resourcetypes.size(); p++)
	    {
	      std::cout<<" "<<this->diet[p];
	      tot_diet+=this->diet[p];
	    }
	  std::cout<<std::endl;
	}
      this->diet.clear();
      this->diet = std::vector<int> (this->environment->resourcetypes.size(),0);
      if(this->environment->global_time%this->environment->YEAR==0)
	this->energy_year = 0; //reset energy counter for every time interval?
      //TODO: store energy_year / by some time factor instead of energy/age
      //std::cout<<"POP: "<<this->environment->aapjes.size()<<std::endl;      

      //TODO: want to store reasonable diet data for div and var analysis
      // - problem: env change
      // - sets diet[R] to zero, and replaces previous resource
      // - at any one time the diet is therefore a mix of resources that have been eaten for a while, and those only eaten recently
      // - solutions:
      // 1 - only measure diet in between env changes (short term diversity) - issue here is 
      // 2 - measure diversity on a longer timescale but incremement the number of resources types in the environment - issue here is how to save the data since initially not all resource types are known.
      // 3 - change resources at regular intervals and measure diets within those intervals

      if(this->environment->SAVE_SOC_LEARN)
	{
	  //output demonstrator data
	  //O 1=time 2=id 3=num_times_obs 4=num_dem_obs 5=num_times_obs_mum 6=avg_num_res_obs 7=totrel 8=totage 9=totpexp 10=totpay
	  if(this->tot_dem[0]>0)
	    {
	      std::cout<<"O "<<(float)this->environment->global_time/this->environment->YEAR<<" "<<this->id;
	      std::cout<<" "<<this->tot_dem[0]; //3 num obs
	      std::cout<<" "<<this->tot_dem[1]/this->tot_dem[0]; //4 avg num dems
	      std::cout<<" "<<this->tot_dem[2]/this->tot_dem[0]; //5 avg num mums
	      std::cout<<" "<<this->tot_dem[3]/this->tot_dem[0]; //6 avg res per obs
	      std::cout<<" "<<this->tot_dem[4]/this->tot_dem[1]; //7 avg rel per dem
	      std::cout<<" "<<(this->tot_dem[5]/this->tot_dem[1])/this->environment->YEAR; //8 avg age
	      std::cout<<" "<<this->tot_dem[6]/this->tot_dem[1]; //9 avg exp
	      std::cout<<" "<<this->tot_dem[7]/this->tot_dem[1]; //10 avg pay
	      std::cout<<" "<<this->tot_dem[8]/this->tot_dem[1]; //11 avg freq

	      std::cout<<std::endl;
	    }

	  //chosen demonstrators
	  if(this->chosen_dem[0]>0)
	    {
	      std::cout<<"CD "<<(float)this->environment->global_time/this->environment->YEAR<<" "<<this->id;
	      std::cout<<" "<<this->chosen_dem[0]; //3 num obs
	      std::cout<<" "<<this->chosen_dem[1]; //4 num dems
	      std::cout<<" "<<this->chosen_dem[2]; //5 num mums
	      std::cout<<" "<<this->chosen_dem[3]/this->chosen_dem[0]; //6 avg freq
	      std::cout<<" "<<this->chosen_dem[4]/this->chosen_dem[0]; //7 avg rel
	      std::cout<<" "<<(this->chosen_dem[5]/this->chosen_dem[0])/this->environment->YEAR; //8 avg age
	      std::cout<<" "<<this->chosen_dem[6]/this->chosen_dem[0]; //9 avg exp
	      std::cout<<" "<<this->chosen_dem[7]/this->chosen_dem[0]; //10 avg pay
	      std::cout<<std::endl;
	    }
	  
	  //output copying data
	  //SE 1=time 2=id 3=num_times_copied 4=numtimescopymom 5=avg rel 6=avg age 7=avg_frequency 8=avgpay 9=avgexp 10=num_stim_events
	  if(this->tot_copy_se[0]>0)
	    {
	      std::cout<<"C_SE "<<(float)this->environment->global_time/this->environment->YEAR<<" "<<this->id;
	      std::cout<<" "<<this->tot_copy_se[0]; //num c
	      std::cout<<" "<<this->tot_copy_se[1]; //num mom
	      std::cout<<" "<<this->tot_copy_se[2]/this->tot_copy_se[0]; //avg rel
	      std::cout<<" "<<(this->tot_copy_se[3]/this->tot_copy_se[0])/this->environment->YEAR; //avg age
	      std::cout<<" "<<this->tot_copy_se[4]/this->tot_copy_se[0]; //avg freq
	      std::cout<<" "<<this->tot_copy_se[5]/this->tot_copy_se[0]; //avg exp
	      std::cout<<" "<<this->tot_copy_se[6]/this->tot_copy_se[0]; //avg pay
	      std::cout<<" "<<this->num_stim;
	      //Todo: avg familiarity? or is that reliability? 
	      std::cout<<std::endl;
	    }
	  

	  //SK 1=time 2=id 3=num_times_copied 4=numtimescopymom 5=tot_age 6=tot_relatible 7=avg_frequency 8=totpay 9=totT(exp) 10=tot_time_per_copy_event
	  if(this->tot_copy_sk[0]>0)
	    {
	      std::cout<<"C_SK "<<(float)this->environment->global_time/this->environment->YEAR<<" "<<this->id;
	      std::cout<<" "<<this->tot_copy_sk[0]; //num c
	      std::cout<<" "<<this->tot_copy_sk[1]; //num mom
	      std::cout<<" "<<this->tot_copy_sk[2]/this->tot_copy_sk[0]; //avg reliability
	      std::cout<<" "<<(this->tot_copy_sk[3]/this->tot_copy_sk[0])/this->environment->YEAR; //avg age
	      std::cout<<" "<<this->tot_copy_sk[4]/this->tot_copy_sk[0]; //avg freq
	      std::cout<<" "<<this->tot_copy_sk[5]/this->tot_copy_sk[0]; //avg experience
	      std::cout<<" "<<this->tot_copy_sk[6]/this->tot_copy_sk[0]; //avg pay
	      std::cout<<" "<<(this->tot_copy_sk[7]/this->tot_copy_sk[0])/this->environment->TIME_SCALAR; //avg time per copy
	      std::cout<<" "<<this->tot_copy_sk[8]/this->tot_copy_sk[0]; //avg skill incr.
	      
	      std::cout<<std::endl;	    
	    }
	    
	  //zero all stored data for next time  
	  this->last_chosen_dem = std::vector<float> (8,0.0);
	  this->tot_dem = std::vector<float> (9,0.0);
	  this->chosen_dem = std::vector<float> (8,0.0);
	  this->tot_copy_se = std::vector<float> (7,0.0);
	  this->tot_copy_sk = std::vector<float> (9,0.0);
	  this->num_stim = 0;
	  //std::cin.get();
	}
    }
}

void Aapje::PreferenceExpectationUpdate()
{   
  //pref_exp update
  //1 - if eat resource of greater quality -> prefexp = pref_i; everyminute reduce by update
  // ----- here reduce prefexp; after choosing food, update prefexp;
  //2 - if stomach full: pref_i = (1+update)*pref_i; else (1-update)*pref_i;
  // ----- here update prefexp; (nowhere else)  

  if(this->stomach_tot==this->MAX_STOMACH)
    //PROD/SUM: -INF<x<INF: taken care of below
    this->pref_exp += std::max(this->pref_exp_update*this->pref_exp, std::numeric_limits<float>::min());
  else
    //PROD/SUM: min_float<x<PREF_EXP=INF
    this->pref_exp -= std::max(this->pref_exp_update*this->pref_exp, std::numeric_limits<float>::min());
  
  /*switch(PREFEXP_UPDATE_STYLE)
    {
    case 0: //update on res q + continous reduction
      //this is update after each Search() event
      //this->pref_exp = this->pref_exp_update*this->pref_exp; //0.999*pref_exp_update
      break;

    case 1:
      if(this->stomach_tot==this->MAX_STOMACH)	
	//PROD/SUM: min_float<x<INF -> taken care of below
	this->pref_exp += std::max(this->pref_exp_update*this->pref_exp, std::numeric_limits<float>::min());
      else
	//PROD/SUM: min_float<x<PREF_EXP=INF
	this->pref_exp -= std::max(this->pref_exp_update*this->pref_exp, std::numeric_limits<float>::min());
      break;

    case 2:  
      //Considers age dependency of prefexpupdate
      {    
	float age = (float)this->age/this->environment->YEAR;
	float pow_age = pow(age, this->pe_u_n);  //PROD/SUM: 0<x<[MAX_AGE^PE_U_N]
	float pow_h = pow(this->pe_u_h, this->pe_u_n); //PROD_SUM: 0<x<this->pe_u_h^this->pe_u_n
	float prefexpupdate=0;
	if(age<2)
	  //PROD_SUM: 0<[pref_exp_update]
	  prefexpupdate = this->pref_exp_update*(pow_age/(pow_age+pow_h));
	else
	  prefexpupdate = this->pref_exp_update;

	//std::cout<<"ind "<<this->id<<" age "<<age<<" prefexpupd "<<prefexpupdate<<" maxprefexpupd "<<this->pref_exp_update<<" prefexp "<<this->pref_exp<<" stomach "<<this->stomach_tot<<" peuN "<<this->pe_u_n<<" peuH "<<this->pe_u_h<<"\n";
	
	//PROD/SUM: -INF<x<INF -> are corrected below
	if(this->stomach_tot==this->MAX_STOMACH)
	  this->pref_exp = this->pref_exp + prefexpupdate*this->pref_exp; 
	else
	  this->pref_exp = this->pref_exp - prefexpupdate*this->pref_exp; 

	//std::cout<<"ind "<<this->id<<" age "<<age<<" prefexpupd "<<prefexpupdate<<" maxprefexpupd "<<this->pref_exp_update<<" prefexp "<<this->pref_exp<<"\n";
	//std::cin.get();

      }
      break;

    default:
      std::cout<<"ERROR: this should not happen!\n";
      std::cout<<"ERROR: PREFEXP_UPDATE_STYLE should be 0, 1 or 2!\n";
      std::cout<<"aapjes.ccp: *** PreferenceExpectationUpdate() ***\n";
      std::exit(0);
      break;
      }*/

  //PROD/SUM: min_float<x<max_float
  if(this->pref_exp<this->min_pref_exp) //to correct any INFs
    this->pref_exp = this->min_pref_exp;
  if(this->pref_exp>std::numeric_limits<float>::max()) this->pref_exp = std::numeric_limits<float>::max();
}

void Aapje::DigestAndLearn()
{
  float E = 0.0; //average energy of all items
  float p = 0.0; //average preference (expected energy) of all items
  std::vector<int> s(this->environment->resourcetypes.size(), 0);

  for(std::vector<int>::iterator it=this->stomach.begin(); it!=this->stomach.end(); ++it)
    {
      if(LEARN_TYPE==0)
	{
	  E+=this->environment->resourcetypes[(*it)]->quality; //PROD/SUM: 0<x<MAXQ*MAXSTOMACH
	  p += this->prefs[(*it)];                             //PROD/SUM: 0<x<MAXQ*MAXSTOMACH
	}
      s[(*it)]++;   //PROD/SUM: 0<x<MAXSTOMACH    
    }

  this->temp_aversion.clear();
  this->temp_aversion = std::vector<bool> (this->environment->resourcetypes.size(),false);

  if(this->stomach_tot>0)
    {
      if(LEARN_TYPE==0)
	{
	  this->energy += E; //PROD/SUM: 0<x<[MAXQ*MAXSTOMACH*[MAXAGE/DIGESTION INTERVAL]]
	  if(this->energy>std::numeric_limits<float>::max()) 
	    this->energy=std::numeric_limits<float>::max();
	  this->energy_year +=E; //PROD/SUM: 0<x<[MAXQ*MAXSTOMACH*[YEAR/DIGESTION INTERVAL]]
	  this->lifetime_energy +=E;
	  E = E / this->stomach_tot;//PROD/SUM: 0<x<[MAXQ]
	  E = E + ind_nor()*E_NOISE;//PROD/SUM: 0<x<[MAXQ]+-INF?
	  if(!std::isfinite(E)) //Check for INF
	    {
	      if(E<0) E = -std::numeric_limits<float>::max();
	      else E = std::numeric_limits<float>::max();
	    }
	  p = p / this->stomach_tot; //PROD/SUM: 0<x<MAXQ
	}
      
      for(int r=0; r<(int)s.size(); r++)
	{
	  if(s[r]>0) //for all eaten resources
	    {
	      int type = r;
	      if(this->novel[type]==true) this->novel[type]=false;
	      
	      if(LEARN_TYPE==0)
		this->prefs[type] += this->U * ((float)s[type]/this->stomach_tot) * (E - p);
	      //PROD/SUM: //PROD/SUM: U*[-INF-MAXq]<x<U*[[MAXQ]+INF]

	      if(SATIATION_SWITCH && s[r] >= (int)(SATIATION_FRACTION*this->MAX_STOMACH))
		{
		  this->temp_aversion[r]=true;
		}
	    }
	}
    } //else stay zero
  //std::cout<<" E "<<E<<" p "<<p<<std::endl;

  this->stomach.clear();
  this->stomach_times.clear();
  this->stomach_tot=0;
}

void Aapje::Reset()
{
  long prev_t_a = this->t_a;  
  this->ZeroAap(); //keep id, keep position
  this->t_a = prev_t_a; //stays same t_a
}

bool Aapje::Starves()
{
  if(this->energy<=0) return true;
  else return false;
}

bool Aapje::WillDie()
{
  if(this->Starves()) return true;
  if(this->age>=this->environment->MAX_AGE) return true;
  if(ind_uni()<this->environment->AAP_DEATH_RATE) return true;
  return false;
}

bool Aapje::WillGiveBirth()
{
  if(this->energy >= this->birth_energy_threshold)
    {
      this->energy -= this->birth_energy_investment; //PROD_SUM: threshold-investm<0<MAX_INT
      //NOTE: checked for MAX during reward obtaining (or digestion)
      return true;
    }
  else
    return false;
}

void Aapje::ZeroAap()
{
  this->safe=false;
  this->age = 0;
  this->offspring=0;
  this->action = NOTHING;     //should this be part of general initialization that always happens?
  this->energy = 0;
  this->energy_year=0;
  this->lifetime_energy=0;
  this->food_target = NULL;
  this->t_a = 0;                    //initial acion is not an action
  this->moved = false;

  //counters
  this->mf=0;
  this->fs=0;
  this->mg=0;
  this->mtf=0;
  this->n=0;
  this->e=0;
  this->choice_asoc=0;
  this->choice_tsoc=0;
  this->choice_psoc=0;
  this->choice_tsoc_n=0;
  this->choice_psoc_n=0;

  this->mf_reps=0;
  this->free_moves=0;

  this->prob_asoc=0;
  this->effect_soc=0;
  this->asoc_eat=0;
  this->soc_eat=0;
  this->soc_eat_and_learn=0;
  this->soc_stim=0;
  this->num_stim=0;
  this->pref_tot_asoc=0;
  this->fam_tot_asoc=0;
  this->sel_asoc=0;
  this->res_qual_asoc=0;
  this->pref_tot_soc=0;
  this->fam_tot_soc=0;
  this->sel_soc=0;
  this->res_qual_soc=0;
  this->dem_age=0;
  this->dem_fam=0;

  this->stomach.clear();            //empty stomach vector
  this->stomach_times.clear();
  this->stomach_tot=0;
  this->crowded=false;
  this->maxview_n.clear();
  this->copyspace_n.clear();
  
  this->prefs.clear();
  this->reliability.clear();
  this->novel.clear();
  this->assoc_res_soc.clear();
  this->temp_aversion.clear();
  this->diet.clear();
  this->total_processing_events.clear();
  this->total_processing_time.clear();

  this->pref_exp = this->init_pref_exp;
  this->stomach_content_estimate=0;
  //this->personal_info_prediction = 0;
  this->prefs = std::vector<float> (this->environment->resourcetypes.size(),0.0);
  this->reliability = std::vector<float> (this->environment->resourcetypes.size(),0.0);
  this->assoc_res_soc = std::vector<int> (this->environment->resourcetypes.size(),0);
  this->assoc_res_soc_total = 0;
  this->asoc=false; this->tsoc=false; this->psoc=false;
  this->novel = std::vector<bool> (this->environment->resourcetypes.size(),true);
  this->temp_aversion = std::vector<bool> (this->environment->resourcetypes.size(),false);
  this->diet = std::vector<int> (this->environment->resourcetypes.size(),0);
  this->total_processing_events = std::vector<int> (this->environment->resourcetypes.size(),0);
  this->total_processing_time = std::vector<float> (this->environment->resourcetypes.size(),0.0);
  if(this->environment->POPULATION_STYLE!=1)  this->sqdist_neighs = std::vector<float> (MAX_GROUP_SIZE+1,0.0); //used as static array; +1 prevents going out of bounds
  if(this->environment->POPULATION_STYLE==1) this->sqdist_neighs = std::vector<float> (this->environment->POPULATION_SIZE_AAPJES/this->environment->NUM_GROUPS,0.0);
  if(this->environment->TRANSMISSION_CHAIN==1) this->sqdist_neighs = std::vector<float> (MAX_GROUP_SIZE+1,0.0); //used as static array; +1 prevents going out of bounds

  this->last_chosen_dem = std::vector<float> (8,0.0);
  this->chosen_dem = std::vector<float> (8,0.0);
  this->tot_dem = std::vector<float> (9,0.0);
  this->tot_copy_se = std::vector<float> (7,0.0); this->copy_se_flag=false;
  this->tot_copy_sk = std::vector<float> (9,0.0);

  this->visible_resources.clear();

  this->temp_social_stimulus_n.clear();
  this->temp_social_stimulus_nID.clear();

  this->perm_social_stimulus_n.clear();
  this->perm_social_stimulus_nID.clear();
  this->perm_social_obs_n.clear();        
  this->perm_social_obs_n_tot.clear();


  //social learning
  this->resource_stimulated=0;
  this->demon=NULL;
  this->last_demon=NULL;
  this->follow_target=NULL;
  this->perm_demon=0;
  this->temp_demon=0;
  this->time_since_stimulation=0;
  this->predicted_reward=0;

  if(FULL_KNOW_SWITCH)
    {
      for(unsigned long r=0; r<this->environment->resourcetypes.size(); r++)
	{
	  this->prefs[r] = this->environment->resourcetypes[r]->quality;
	  this->reliability[r] = 1.0;
	  this->novel[r] = false; 
	  this->total_processing_events[r] = this->environment->SIMULATION_TIME;
	  this->total_processing_time[r] = this->environment->SIMULATION_TIME;
	  //std::cout<<"res "<<r<<" pref "<<this->prefs[r]<<std::endl;
	}
    }  
}

void Aapje::CopyAap(Aapje* aap)
{  
  this->t_n = aap->t_n;
  this->t_e = aap->t_e;  
  this->d_r = aap->d_r;  
  this->d_r_sq = aap->d_r_sq;
  this->MAX_STOMACH = aap->MAX_STOMACH;
  this->DIG_INT = aap->DIG_INT;  
  this->pref_exp_update = aap->pref_exp_update;
  this->pe_u_n = aap->pe_u_n;
  this->pe_u_h = aap->pe_u_h;
  this->init_pref_exp = aap->init_pref_exp;
  this->min_pref_exp = aap->min_pref_exp; //should be minimum
  this->pref_exp = this->init_pref_exp;
  this->pref_exp_K = aap->pref_exp_K;
  this->pref_exp_N = aap->pref_exp_N;
  this->pref_exp_H = aap->pref_exp_H;
  this->pref_exp_H_N = aap->pref_exp_H_N;
  this->lambda_stomach = aap->lambda_stomach;

  this->foodchoice_selectivity = aap->foodchoice_selectivity;

  this->pos = aap->pos;
  this->parent_id = aap->id;
  this->birth_energy_threshold= aap->birth_energy_threshold;
  this->birth_energy_investment= aap->birth_energy_investment;      
  this->metabolic_costs= aap->metabolic_costs;

  //SEARCH
  this->t_f = aap->t_f;
  this->a_f = aap->a_f;
  this->half_a_f = aap->a_f/2;
  this->d_f = aap->d_f;
  this->d_fsq = aap->d_fsq; 
  
  //MOVEMENT
  this->p_m = aap->p_m; 
  this->s_m = aap->s_m; 
  //NOTE: this calculates: time per meter -> later multiplied by distance
  this->d_m = aap->d_m;
  //this->d_m_scaled = aap->d_m_scaled; //*this->environment->SPACE_SCALAR;
  this->d_mf = aap->d_mf;
  this->s_mg = aap->s_mg; 
  //NOTE: this calculates: time per meter -> later multiplied by distance
  this->d_mg = aap->d_mg;
  this->d_mg_sq = aap->d_mg_sq;

  //GROUPING
  this->grouping = aap->grouping;
  this->group_id = aap->group_id;

  //if(this->group_id >= (int)this->environment->groups.size())
  //{
  //  std::cout<<"ERROR: group_id >= groups.size\n";
  //  std::cout<<"aapjes.cpp: CopyAap()\n";
  //  std::exit(0);
  //}

  this->follow_target = this->environment->groups[this->group_id].ReturnLeader();

  this->safespace_sq = aap->safespace_sq;
  this->safenumber = aap->safenumber;
  this->prob_check_safe = aap->prob_check_safe;

  //FOOD CHOICE, PREFS & LEARNING
  this->U = aap->U;
  this->U_TS = aap->U_TS;
  this->U_TS_N = aap->U_TS_N;  
  this->U_PS = aap->U_PS;
  this->U_PS_N = aap->U_PS_N;
  this->U_TS_INIT = aap->U_TS_INIT;
  this->U_TS_N_INIT = aap->U_TS_N_INIT;  
  this->U_PS_INIT = aap->U_PS_INIT;
  this->U_PS_N_INIT = aap->U_PS_N_INIT;

  this->explore_rate = aap->explore_rate;
  this->explore_stimulus = aap->explore_stimulus;

  this->initial_trial_rate = aap->initial_trial_rate;
  this->trial_rate = this->initial_trial_rate;
  this->assoc_res_soc_learn_rate = aap->assoc_res_soc_learn_rate;

  this->temp_social_stimulus = aap->temp_social_stimulus;
  this->perm_social_stimulus = aap->perm_social_stimulus;
  this->copy_strategy = aap->copy_strategy;
  this->stimulus_duration = aap->stimulus_duration;
  //this->personal_info_prediction = 0;

  this->t_o = aap->t_o;
  this->skill_copy_rate = aap->skill_copy_rate;
  this->s_k = aap->s_k;

  this->l_M = aap->l_M;
  this->l_c = aap->l_c;
  this->l_f = aap->l_f;
  this->l_a = aap->l_a;
  this->l_t = aap->l_t;
  this->l_p = aap->l_p;
  this->l_r = aap->l_r;

  this->S_f = aap->S_f;
  this->S_a = aap->S_a;
  this->S_t = aap->S_t;
  this->S_p = aap->S_p;
}

float Aapje::MutateGene(float gene, float min, float max, float step, float mutrate)
{
  if(ind_uni() < mutrate) //0<x<1
    {
      gene = gene + (ind_nor()*step); //PROD/SUM: -20-INF<x<20+INF (taken care of below)

      if(gene > max) return max; //INF taken care of
      if(gene < min) return min;
      return gene;
    }  
  else
    return gene;
}

void Aapje::MutateGenome()
{
  //this->initial_trial_rate = this->MutateGene(this->initial_trial_rate, 0.0, 1.0, 0.2, MUTATION_RATE); //1
  this->trial_rate = this->initial_trial_rate;
  this->pref_exp_update = 
    this->MutateGene(this->pref_exp_update, 0.0, 1.0, 0.2, this->environment->MUTATION_RATE); //2
  this->foodchoice_selectivity = 
    this->MutateGene(this->foodchoice_selectivity, 0.0, 20.0, 4.0, this->environment->MUTATION_RATE); //3
 
  this->temp_social_stimulus = 
    this->MutateGene(this->temp_social_stimulus, 0.0, 1.0, 0.2, this->environment->MUTATION_RATE); //4
  this->perm_social_stimulus = 
    this->MutateGene(this->perm_social_stimulus, 0.0, 1.0, 0.2, this->environment->MUTATION_RATE); //5
  this->d_mf = (float)(int)(this->MutateGene(this->d_mf, 0.0, 20.0, 4.0, this->environment->MUTATION_RATE)); //6
  this->U = this->MutateGene(this->U, 0.0, 1.0, 0.2, this->environment->MUTATION_RATE); //7
  //this->pe_u_n = this->MutateGene(this->pe_u_n, 0.0, 20.0, 4.0, MUTATION_RATE); //8
  //this->pe_u_h = this->MutateGene(this->pe_u_h, 0.0, 2.0, 0.4, MUTATION_RATE); //9
  //this->assoc_res_soc_learn_rate = 
  //this->MutateGene(this->assoc_res_soc_learn_rate, 0.0, 1.0, 0.2, MUTATION_RATE); //10
  //this->min_pref_exp = 
  //this->MutateGene(this->min_pref_exp, std::numeric_limits<float>::min(), 1.0,0.2, MUTATION_RATE); //11
  if(this->environment->INIT_PREF_SWITCH)
    this->init_pref_exp = 
      this->MutateGene(this->init_pref_exp, this->min_pref_exp, 1.0, 0.2, this->environment->MUTATION_RATE); //12  
  this->pref_exp = this->init_pref_exp;

  //this->U_TS = this->MutateGene(this->U_TS, 0.0, 1.0, 0.2, MUTATION_RATE); //13  
  //this->U_PS = this->MutateGene(this->U_PS, 0.0, 1.0, 0.2, MUTATION_RATE); //14
  //this->U_TS_N = this->MutateGene(this->U_TS_N, 0.0, 1.0, 0.2, MUTATION_RATE); //15
  //this->U_PS_N = this->MutateGene(this->U_PS_N, 0.0, 1.0, 0.2, MUTATION_RATE); //16
  //this->U_TS_INIT = this->MutateGene(this->U_TS_INIT, 0.0, 1.0, 0.2, MUTATION_RATE); //17  
  //this->U_PS_INIT = this->MutateGene(this->U_PS_INIT, 0.0, 1.0, 0.2, MUTATION_RATE); //18
  //this->U_TS_N_INIT = this->MutateGene(this->U_TS_N_INIT, 0.0, 1.0, 0.2, MUTATION_RATE); //19
  //this->U_PS_N_INIT = this->MutateGene(this->U_PS_N_INIT, 0.0, 1.0, 0.2, MUTATION_RATE); //20

 this->explore_rate = this->MutateGene(this->explore_rate, 0.0, 1.0, 0.2, this->environment->MUTATION_RATE); //21
 if(this->environment->EXPLORE_REL_SWITCH)
   this->explore_stimulus = this->MutateGene(this->explore_stimulus, 0.0, 1.0, 0.2, this->environment->MUTATION_RATE);  //22
 //this->pref_exp_K = this->MutateGene(this->pref_exp_K, 0.0, 1.0, 0.2, MUTATION_RATE);//23
 //this->pref_exp_N = this->MutateGene(this->pref_exp_N, 0.0, 20.0, 4.0, MUTATION_RATE);//24
 //this->pref_exp_H = this->MutateGene(this->pref_exp_H, 0.0, this->MAX_STOMACH, 0.2*this->MAX_STOMACH, MUTATION_RATE);//25
 //this->pref_exp_H_N = pow(this->pref_exp_H, this->pref_exp_N);
 //this->lambda_stomach = this->MutateGene(this->lambda_stomach, 0.0, 1.0, 0.2, MUTATION_RATE);//26

 this->t_o = this->MutateGene(this->t_o, 60, 6000, 1200, this->environment->MUTATION_RATE); //27
 this->skill_copy_rate = this->MutateGene(this->skill_copy_rate, 0.0, 1.0, 0.2, this->environment->MUTATION_RATE);//28
 //this->s_k = this->MutateGene(this->s_k, 0.0, 1.0, 0.2, MUTATION_RATE);
 this->l_M = this->MutateGene(this->l_M, -20.0, 20.0, 4.0, this->environment->MUTATION_RATE);//29
 this->l_c = this->MutateGene(this->l_c, -20.0, 20.0, 4.0, this->environment->MUTATION_RATE);//30
 this->l_t = this->MutateGene(this->l_t, -20.0, 20.0, 4.0, this->environment->MUTATION_RATE);//31
 this->l_a = this->MutateGene(this->l_a, -20.0, 20.0, 4.0, this->environment->MUTATION_RATE);//32
 this->l_f = this->MutateGene(this->l_f, -20.0, 20.0, 4.0, this->environment->MUTATION_RATE);//33
 this->l_p = this->MutateGene(this->l_p, -20.0, 20.0, 4.0, this->environment->MUTATION_RATE);//34
 this->l_r = this->MutateGene(this->l_r, -20.0, 20.0, 4.0, this->environment->MUTATION_RATE);//35

 //this->S_f = this->MutateGene(this->S_f, 0.0, 20.0, 4.0, MUTATION_RATE);//36
 //this->S_a = this->MutateGene(this->S_a, 0.0, 20.0, 4.0, MUTATION_RATE);//37
 //this->S_t = this->MutateGene(this->S_t, 0.0, 20.0, 4.0, MUTATION_RATE);//38
 //this->S_p = this->MutateGene(this->S_p, 0.0, 20.0, 4.0, MUTATION_RATE);//39
}

void Aapje::GenomeRange(int root, int gene1_int, int gene2_int, float min_gene, float max_gene)
{	
  int ind = (gene1_int*root) + gene2_int; //give range of variable
  int full_range = root*root; //can ues full range if only varying one variable

  switch(this->environment->GENOME_RANGE)
    {     
    case 17:
      {
	this->explore_stimulus=min_gene+(max_gene-min_gene)*(((float)ind)/(full_range-1));
	if(this->environment->EXPLORE_SWITCH==0)
	  {
	    std::cout<<"ERROR: trying to do GenomeRange parameter sweep for explore_rate\n";
	    std::cout<<"but EXPLORE_SWITCH==FALSE, must be TRUE!\n";
	    std::exit(0);
	  }
      }
      break;
      
    case 16:
      {
	this->explore_rate=min_gene+(max_gene-min_gene)*(((float)ind)/(full_range-1));
	if(this->environment->EXPLORE_SWITCH==0)
	  {
	    std::cout<<"ERROR: trying to do GenomeRange parameter sweep for explore_rate\n";
	    std::cout<<"but EXPLORE_SWITCH==FALSE, must be TRUE!\n";
	    std::exit(0);
	  }
      }
      break;

    case 15:
      {
	this->temp_social_stimulus=min_gene+(max_gene-min_gene)*(((float)ind)/(full_range-1));
	if(!this->environment->TEMP_STIMULUS_SWITCH)
	  {
	    std::cout<<"ERROR: trying to do GenomeRange parameter sweep for temp_social_stimulus\n";
	    std::cout<<"but TEMP_STIMULUS_SWITCH = FALSE, must be <=2!\n";
	    std::exit(0);
	  }
      }
      break;

    case 14: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->U_PS_N = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
    case 13: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->U_TS_N = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
    case 12: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->U_PS = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
    case 11: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->U_TS = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
    case 10: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->min_pref_exp = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
    case 9: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->U = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
    case 8: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->d_mf = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
    case 7: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->init_pref_exp = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
	this->pref_exp = this->init_pref_exp;
      }
      break;
      
    case 6: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->foodchoice_selectivity = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
     case 5: //U_e only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->pref_exp_update = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
      }
      break;
      
    case 4: //p_T only
      {	//starts at 0 ends at (fullrange)-1; e.g. 0-99
	this->initial_trial_rate = min_gene + (max_gene-min_gene)*( ((float)ind)/(full_range-1) );
	this->trial_rate = this->initial_trial_rate;
	//std::cout<<"Ind "<<this->id<<" trial_rate "<<this->trial_rate<<std::endl;
      }
      break;
      
    case 3: //U_e vs Emin
      this->min_pref_exp = ((float)gene1_int)/root;      //start at zero
      if(this->min_pref_exp<std::numeric_limits<float>::min()) this->min_pref_exp=std::numeric_limits<float>::min();
      this->pref_exp_update = ((float)gene2_int)/root; //start at zero
      break;

    case 2: //p_T vs U
      this->U = ((float)gene1_int+1)/root;  
      this->initial_trial_rate = ((float)gene2_int+1)/root;  
      this->trial_rate = this->initial_trial_rate;
      break;

    case 1: //d_m versus N
      this->d_mf = 20.0*((float)gene1_int+1)/root;  
      this->foodchoice_selectivity = 20.0*((float)gene2_int+1)/root;
      //std::cout<<"Ind "<<this->id<<" dmf "<<this->d_mf<<" N "<<this->foodchoice_selectivity<<std::endl;
      break;

    case 0:
      std::cout<<"ERROR in GenomeRange: GENOMERANGE==0, should not be!\n";
      std::exit(0);

    default:
      std::cout<<"ERROR in GenomeRange: reached default in switch!\n";
      std::exit(0);
    }
}

void Aapje::RandomizeGenome()
{
  //this->initial_trial_rate = ind_uni();
  this->trial_rate = this->initial_trial_rate;
  this->pref_exp_update = ind_uni();
  this->foodchoice_selectivity = ind_uni()*20.0;
  this->temp_social_stimulus = ind_uni();
  this->perm_social_stimulus = ind_uni();
  this->d_mf = (float)(int)(ind_uni()*20.0);
  this->U = ind_uni();
  //this->U_TS = ind_uni();
  //this->U_TS_N = ind_uni();
  //this->U_PS = ind_uni();
  //this->U_PS_N = ind_uni();
  //this->U_TS_INIT = ind_uni();
  //this->U_TS_N_INIT = ind_uni();
  //this->U_PS_INIT = ind_uni();
  //this->U_PS_N_INIT = ind_uni();
  //this->pe_u_n = ind_uni()*20.0;
  //this->pe_u_h = ind_uni()*2.0;
  //this->assoc_res_soc_learn_rate = ind_uni();
  if(this->environment->INIT_PREF_SWITCH)
    this->init_pref_exp = ind_uni();
  if(this->init_pref_exp<this->min_pref_exp) this->init_pref_exp = this->min_pref_exp;
  this->pref_exp = this->init_pref_exp;
  this->explore_rate = ind_uni();  
  if(this->environment->EXPLORE_REL_SWITCH)
    this->explore_stimulus = ind_uni();
  //this->pref_exp_K = ind_uni();
  //this->pref_exp_N = ind_uni()*20;
  //this->pref_exp_H = ind_uni()*this->MAX_STOMACH;
  //this->pref_exp_H_N = pow(this->pref_exp_H, this->pref_exp_N);
  //this->lambda_stomach = ind_uni();
  this->t_o = ind_uni()*this->t_e;
  this->skill_copy_rate = ind_uni();
  //this->s_k = ind_uni();
  this->l_M = 20.0-ind_uni()*40.0;
  this->l_c = 20.0-ind_uni()*40.0;
  this->l_r = 20.0-ind_uni()*40.0;
  this->l_f = 20.0-ind_uni()*40.0;
  this->l_a = 20.0-ind_uni()*40.0;
  this->l_t = 20.0-ind_uni()*40.0;
  this->l_p = 20.0-ind_uni()*40.0;
  //this->S_f = ind_uni()*20;
  //this->S_a = ind_uni()*20;
  //this->S_t = ind_uni()*20;
  //this->S_p = ind_uni()*20;
}



///////////////////////////////////////////////////////////////////////////////////////
//constructor2: parent given = individual is born
Aapje::Aapje(Environment *environment, Aapje* mother)
{
  this->alive=true;
  this->environment = environment; //needs to be set before this->environment->etc can be used

  //get variables from mother!
  this->id = instances++;
  this->ZeroAap(); //non parent variables, position, not trajectory
  this->CopyAap(mother);

  this->t_a = mother->t_a; //zeroaap sets it to zero which messes up action queing
  this->energy = mother->birth_energy_investment; //energy you get from mom

  this->MutateGenome();

  SetRandomHeading();

  this->traj = new std::list<Point2d>();  
  for(int i = 0; i < this->environment->TRAJECTORY_LENGTH_AAP; i++)
    {
      this->traj->push_front(this->pos);
    }
}

//constructor: no parent given = start of simulation
Aapje::Aapje(Config c, Environment *environment, bool RANDOM_AGE_DISTR)
{  
  this->alive=true;
  this->environment = environment; //needs to be set before this->environment->etc can be used

  this->ReadVariables(c);

  this->ZeroAap(); //not parent, environment pointer, variables, position, not trajectory

  if(RANDOM_AGE_DISTR) //for initializing population its equilibrium exponential distribution
    {
      //get age from an expontential distribution (with rate 0.1 per year)
      float age = ind_exp();
      while (age >= 20.0 || age<0){age = ind_exp();}
      this->age = (int)(age*this->environment->YEAR);
    }

  if(RANDOMIZE_GENOME)
    this->RandomizeGenome();

  //SPECIFIC TO START OF SIMULATION
  this->id = instances++;
  this->parent_id = -1;
  this->follow_target = NULL;
  this->energy = this->birth_energy_investment; //max birth energy at start;

  if(this->grouping)
    {
      this->SetRandomGroupPosition(); //sets int_pos and pos
      this->group_id = (this->id%this->environment->NUM_GROUPS);
      this->follow_target = this->environment->groups[this->group_id].ReturnLeader();
    }
  else
    this->SetRandomPosition(); //sets int_pos and pos
  this->SetRandomHeading();

  this->traj = new std::list<Point2d>();  
  for(int i = 0; i < this->environment->TRAJECTORY_LENGTH_AAP; i++)
    {
      this->traj->push_front(this->pos);
    }
}


//NOT USED
/*void Aapje::InsertInXposList()
{
  const bool CHECKS=false;
  if(!this->environment->aapx.empty())
    {      
      if(CHECKS) std::cout<<"before iterator assignment\n";
      std::list<Aapje*>::iterator it = this->environment->aapx.begin(); //get first element
      if(CHECKS) std::cout<<"after iterator assignment\n";
      while (it != this->environment->aapx.end() && (*it)->pos.x < this->pos.x)
	//do this until it has greater x value (and it-- has lower)
	{	 
	  if(CHECKS) std::cout<<"in while loop\n";
	  it++;
	}
      this->environment->aapx.insert(it, this); //insert at position where x is greater

      for(std::list<Aapje*>::iterator it = this->environment->aapx.begin(); it!=this->environment->aapx.end(); it++)
	{
	  (*it)->aapx_pos = it; //the aap the aapx list points to should be itself	  
	  if(CHECKS)
	    {
	      int id = (*it)->id;
	      std::cout<<id;
	      float x = this->environment->aapjes[id]->pos.x;
	      std::cout<<" "<<x;	  
	      std::cout<<" "<<(*this->environment->aapjes[id]->aapx_pos)->id<<std::endl;
	    }
	}
    }
  else //empty
    this->environment->aapx.push_back(this);
    }*/

Aapje::~Aapje()
{
  this->traj->clear();
  delete traj;
  this->traj=NULL;
  this->demon=NULL;
  this->last_demon=NULL;
  this->follow_target=NULL;
  this->food_target=NULL;
  this->environment=NULL;
}

void Aapje::SetRandomGroupPosition()
{
  //grouppos defined globally as (-1,-1) and should only be set once
  if(grouppos.x==-1 && grouppos.y==-1)
    {
      //PROD/SUM: 10<x<10+[FIELD-20]
      grouppos.x = 10 + (ind_uni()*(this->environment->XFIELD-(20))); 
      grouppos.y =  10 + (ind_uni()*(this->environment->YFIELD-(20))); 
    }
  //std::cout<<" x "<<grouppos.x<<" y "<<grouppos.y<<std::endl;

  this->pos.x = grouppos.x + ((2*ind_uni()) - 1)*10; //PROD/SUM: -10<x<10
  this->pos.y = grouppos.y + ((2*ind_uni()) - 1)*10;

  if(!this->environment->CheckPositionIsOnGrid(this->pos))
    {
      std::cout<<"ERROR: individual not on field! x "<<this->pos.x<<" y "<<this->pos.y<<std::endl;
      std::cout<<"aapjes.cpp: ************** SetRandomGroupPosiion **********\n";
      std::exit(0);
    }
  //std::cout<<" x "<<this->pos.x<<" y "<<this->pos.y<<std::endl;
}

//initialization
void Aapje::SetRandomPosition()
{
  this->pos.x = ind_uni()*this->environment->XFIELD; //PROD/SUM: 0<x<XFIELD
  this->pos.y = ind_uni()*this->environment->YFIELD;
}

Vec2d Aapje::RandomVector(float angle1, float angle2)
{
  //if use angle -> can use knowledge on edge of field to choose angle ranges
  //avoids while loop waiting for random direction which leads to position on field

  if(angle1>angle2)
    {
      std::cout<<"ERROR aapjes.cpp (RandomVector): angle1>angl2 -> undefined\n";
      std::exit(0);
    }

  if(angle1 < -2*PI || angle1 > 2*PI || angle2 < -2*PI || angle2 > 2*PI)
    {
      std::cout<<"ERROR aapjes.cpp (RandomVector): angle1 "<<angle1<<" or angle2 "<<angle2<<" is too small or large\n";
      std::exit(0);
    }

  //PROD/SUM: 0<x<2*PI + 2*PI
  float ran_angle = angle1 + ind_uni()*(angle2-angle1);
  Vec2d ran_vec; ran_vec.x=1.0; ran_vec.y=0.0;  //x=1; y=0 => heading angle=0
  
  ran_vec = ran_vec.rotate(ran_angle);   //rotates anti-clockwise PROD/SUM: 0<x<2*PI

  //std::cout<<" angle1 "<<angle1<<" angle2 "<<angle2<<" ranangle "<<ran_angle<<" ran_vec.x "<<ran_vec.x<<" ran_vec.y "<<ran_vec.y<<" atan(ranvec) "<<ran_vec.angle()<<" length "<<ran_vec.length()<<" norm "<<(ran_vec.norm()).length()<<std::endl

  return ran_vec.norm();
}

void Aapje::SetRandomHeading()
{
  Vec2d ran_vec = this->RandomVector(0,2*PI);
  this->dir = ran_vec;
}

void Aapje::ReadVariables(Config c)
{
  //anything added here should be checked with zeroaap and copyaap

  //PROD/SUM: 0<x<TIMESCALAR*T
  this->t_n = (int)(c.get<float>("NOTHING_TIME")*this->environment->TIME_SCALAR);
  this->t_e = (int)(c.get<float>("EAT_TIME")*this->environment->TIME_SCALAR);  
  this->d_r = c.get<float>("REACH");  
  this->d_r_sq = this->d_r*this->d_r; //PROD/SUM: 0<x<INF
  this->MAX_STOMACH = c.get<int>("MAX_STOMACH");
  this->DIG_INT = c.get<int>("DIG_INT");
  this->birth_energy_threshold= c.get<float>("birth_energy_threshold");  
  this->birth_energy_investment= c.get<float>("birth_energy_investment");      
  this->metabolic_costs= c.get<float>("metabolic_costs");    

  //SEARCH
  this->t_f = (int)(c.get<float>("SEARCH_TIME")*this->environment->TIME_SCALAR); //0<x<T*SCALAR
  this->a_f = c.get<float>("SEARCH_ANGLE");  
  this->half_a_f = this->a_f/2;
  this->d_f = c.get<float>("SEARCH_DISTANCE");  
  this->d_fsq = this->d_f*this->d_f;  //PROD/SUM 0<x<INF
  MAX_ITEMS_ASSESSED_PER_SEARCH = c.get<int>("MAX_ITEMS_ASSESSED_PER_SEARCH");
  SEARCH_TYPE = c.get<int>("SEARCH_TYPE");
  
  //MOVEMENT
  this->p_m = c.get<float>("MOVE_PROB"); 
  this->s_m = (int)((1.0/c.get<float>("MOVE_SPEED"))*this->environment->TIME_SCALAR); 
  //NOTE: this calculates: time per meter -> later multiplied by distance
  this->d_m = c.get<float>("MOVE_STEP_DISTANCE");
  this->d_mf = c.get<float>("MOVE_FORWARD_DISTANCE");
  this->s_mg = (int)((1.0/c.get<float>("MOVETOGROUP_SPEED"))*this->environment->TIME_SCALAR); 
  //NOTE: this calculates: time per meter -> later multiplied by distance
  this->d_mg = c.get<float>("MOVETOGROUP_DISTANCE");
  this->d_mg_sq = this->d_mg*this->d_mg;

  //GROUPING
  this->grouping = c.get<bool>("GROUPING_SWITCH");

  this->group_id = 0; //initially all same group
  GROUPING_STYLE = c.get<int>("GROUPING_STYLE");
  MAXVIEW = (c.get<int>("MAXVIEW"));
  MAXVIEW_SQ = (c.get<int>("MAXVIEW"))*(c.get<int>("MAXVIEW")); //PROD INF!
  MAXALIGN_SQ = (c.get<float>("MAXALIGN"))*(c.get<float>("MAXALIGN")); //PROD INF!
  MAXALIGN = (c.get<float>("MAXALIGN"));
  SAFESPACE_SQ = (c.get<int>("SAFESPACE"))*(c.get<int>("SAFESPACE")); //PROD INF!
  SAFESPACE = c.get<int>("SAFESPACE");
  this->safespace_sq = SAFESPACE_SQ;
  SAFENUMBER = c.get<int>("SAFENUMBER");
  this->safenumber = SAFENUMBER;
  this->prob_check_safe = c.get<float>("PROB_CHECK_SAFE"); //NOT USED
  ALIGNPROB = c.get<float>("ALIGNPROB");
  REACH_SQ = this->d_r*this->d_r;  //PROD: INF!


  //FOOD CHOICE, PREFS & LEARNING
  FULL_KNOW_SWITCH = c.get<bool>("FULL_KNOW_SWITCH");
  this->foodchoice_selectivity = c.get<float>("FOOD_CHOICE_SEL"); 
  this->min_pref_exp = std::numeric_limits<float>::min();  
  if(this->environment->INIT_PREF_SWITCH)
    this->init_pref_exp = c.get<float>("INIT_PREF_EXP");  //TODO: set to this min and not evolve?
  else this->init_pref_exp=this->min_pref_exp;
  if(this->init_pref_exp<this->min_pref_exp) 
    {
      std::cout<<"ERROR: initpref < minpref; CHECK PARFILE!\n";
      std::exit(0);
    }
  this->pref_exp = this->init_pref_exp;

  //this->pref_exp_N = c.get<float>("PREF_EXP_N");
  //this->pref_exp_H = c.get<float>("PREF_EXP_H");
  //this->pref_exp_H_N = pow(this->pref_exp_H, this->pref_exp_N);
  //this->pref_exp_K = c.get<float>("PREF_EXP_K");
  //this->lambda_stomach = c.get<float>("lambda_stomach");

  this->pref_exp_update = c.get<float>("PREF_EXP_UPDATE");
  
  //NOT USED
  //this->pe_u_n = c.get<float>("PE_U_N");
  //this->pe_u_h = c.get<float>("PE_U_H");

  PREFEXP_UPDATE_STYLE = c.get<float>("PREFEXP_UPDATE_STYLE");
  this->U = c.get<float>("U");

  //NOT USED
  //this->U_TS = c.get<float>("U_TS");
  //this->U_TS_N = c.get<float>("U_TS_N");
  //this->U_PS = c.get<float>("U_PS");
  //this->U_PS_N = c.get<float>("U_PS_N");
  //this->U_TS_INIT = c.get<float>("U_TS_INIT");
  //this->U_TS_N_INIT = c.get<float>("U_TS_N_INIT");
  //this->U_PS_INIT = c.get<float>("U_PS_INIT");
  //this->U_PS_N_INIT = c.get<float>("U_PS_N_INIT");
  //this->initial_trial_rate = c.get<float>("initial_trial_rate");
  //this->assoc_res_soc_learn_rate = c.get<float>("assoc_res_soc_learn_rate");
  //this->trial_rate = this->initial_trial_rate;

  this->temp_social_stimulus = c.get<float>("TEMP_SOCIAL_STIMULUS");
  this->perm_social_stimulus = c.get<float>("PERM_SOCIAL_STIMULUS");

  //NOT USED
  //this->copy_strategy = c.get<float>("COPY_STRATEGY");

  this->stimulus_duration = c.get<float>("STIMULUS_DURATION");
  this->explore_rate = c.get<float>("EXPLORE_RATE");
  if(this->environment->EXPLORE_REL_SWITCH)
    this->explore_stimulus = c.get<float>("EXPLORE_STIMULUS"); //used for reliability scalar
  else
    this->explore_stimulus = 1.0;

  this->skill_copy_rate = c.get<float>("SKILL_COPY_RATE");
  this->s_k = c.get<float>("S_K");
  if(this->environment->SOC_SKILL_LEARN_SWITCH && this->s_k==0.0)
    {
      std::cout<<"WARNING! S_K = 0 while SOC_SKILL_LEARN_SWITCH=TRUE\n";
      std::exit(0);
    }

  this->t_o = (int)(c.get<float>("OBSERVE_TIME")*this->environment->TIME_SCALAR);
  this->l_M = c.get<float>("L_M"); //maternal bias
  this->l_c = c.get<float>("L_C"); //(unbiased) basic copy rate
  this->l_r = c.get<float>("L_R"); //reliability bias
  this->l_f = c.get<float>("L_T"); //frequency bias
  this->l_a = c.get<float>("L_A"); //age bias
  this->l_t = c.get<float>("L_T"); //experience bias
  this->l_p = c.get<float>("L_P"); //payoff bias

  //NOT USED
  //this->S_f = c.get<float>("S_F");
  //this->S_a = c.get<float>("S_A");
  //this->S_t = c.get<float>("S_T");
  //this->S_p = c.get<float>("S_P");

  SATIATION_SWITCH = c.get<bool>("SATIATION_SWITCH");
  SATIATION_FRACTION = c.get<float>("SATIATION_FRACTION");
  E_NOISE = c.get<float>("E_NOISE"); //NOT USED
  L_NOISE = c.get<float>("L_NOISE");
  P_NOISE = c.get<float>("P_NOISE"); //NOT USED
  COPYSPACE = c.get<int>("COPYSPACE");
  if(COPYSPACE>SAFESPACE) COPYSP_BIGGER=true; else COPYSP_BIGGER=false; //USED?
  COPYSPACE_SQ = COPYSPACE*COPYSPACE;  //PROD INF!
  SOCIAL_STIMULUS_STYLE = c.get<int>("SOC_STIM_STYLE");
  LEARN_TYPE = c.get<int>("LEARN_TYPE");

  RANDOMIZE_GENOME = c.get<bool>("RANDOMIZE_GENOME");
  if(RANDOMIZE_GENOME && this->environment->GENOME_RANGE>0)
    {
      std::cout<<"ERROR: GENOME_RANGE SELECTED but RAND_GENOME==1!\n";
      std::exit(0);
    }

  MAX_GROUP_SIZE = c.get<int>("MAX_GROUP_SIZE"); //used here because in env.cpp it is changed

  //CHECKS:
  //Observeneighbor: int time = std::min(((int)(this->demon->t_a - this->t_a)),this->t_o);
  if(this->environment->MAX_AGE*this->environment->TIME_SCALAR > std::numeric_limits<int>::max())
    {
      std::cout<<"ERROR: t_o too big for (int)time in ObserveNeighbour()\n"; std::exit(0);
    }

  //SocialSkillLearning: 
  //skill_inc = this->s_k*(((float)time)/this->demon->t_e)*((float)(this->demon->total_processing_events[type] - this->total_processing_events[type]));
  if(this->s_k*((float)(this->environment->MAX_AGE*this->environment->TIME_SCALAR)) > std::numeric_limits<float>::max())
    {
      std::cout<<"ERROR: totalprocessingevents too big for (float)skill_inc in SocialSkillLearning()\n"; std::exit(0);
    }

  //this->total_processing_events[type] += skill_inc;
  if(this->s_k*((float)(this->environment->MAX_AGE*this->environment->TIME_SCALAR)*(this->environment->MAX_AGE*this->environment->TIME_SCALAR)) > std::numeric_limits<float>::max())
    {
      std::cout<<"ERROR: totalprocessingevents/time is too small SocialSkillLearning()\n"; std::exit(0);
    }
  
  //this->tot_copy_sk[8] += skill_inc;
  if(this->s_k*((float)(this->environment->MAX_AGE*this->environment->TIME_SCALAR)*(this->environment->MAX_AGE*this->environment->TIME_SCALAR))*this->environment->POPULATION_SIZE_AAPJES > std::numeric_limits<float>::max())
    {
      std::cout<<"ERROR: tot_copy_sk is too small SocialSkillLearning()\n"; std::exit(0);
    }

  //MoveToLeader
  //float distsq = (xdis*xdis)+(ydis*ydis); 
  if(((this->environment->XFIELD*this->environment->XFIELD) + (this->environment->YFIELD*this->environment->YFIELD)) > std::numeric_limits<float>::max())
    {
      std::cout<<"ERROR: XField^2 + Yfield^2 is too large! Need to calculate distances\n"; std::exit(0);
    }

  //ChooseDemonstrator3()
  //this->tot_dem[3]+=num_res;
  if(this->environment->POPULATION_SIZE_AAPJES*this->environment->SAVE_TIME*6 > std::numeric_limits<float>::max())
     {
      std::cout<<"ERROR: tot_dem is too small!\n"; std::exit(0);
    }

  //DigestAndLearn
  //this->energy += E; //PROD/SUM: 0<x<[MAXQ*MAXSTOMACH*[MAXAGE/DIGESTION INTERVAL]]
  if((this->MAX_STOMACH*(this->environment->MAX_AGE/this->DIG_INT))>std::numeric_limits<float>::max())
         {
	   std::cout<<"ERROR: this->energy is too small!\n"; std::exit(0);
	 }
  //std::cout<<"No problems!\n"; std::exit(0);
}
