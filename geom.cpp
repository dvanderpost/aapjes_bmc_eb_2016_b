#include <assert.h>
#include <cmath>
#include <float.h>
#include <iostream>
#include <stdio.h>
#include "geom.h"
#include "rstartree/rectangle.h"

float SafeAcos(float x)
{
  if(x>1.0) 
    {x=1.0;}
  else
    {
      if(x<-1.0) x=-1.0;
    }
  return acos(x);
}

/////////// VECTOR FUNCTIONS /////////////////////////////////////////////////////////

Vec2d Vec2d::operator +(Vec2d w) {
  Vec2d r;
  r.x = x + w.x;
  r.y = y + w.y;

  return r;
}

Vec2d Vec2d::operator -(Vec2d w) {
  Vec2d r;
  r.x = x - w.x;
  r.y = y - w.y;

  return r;
}

Vec2d Vec2d::operator *(float s) {
  Vec2d r;
  r.x = x * s;
  r.y = y * s;
  return r;
}

float Vec2d::dot(Vec2d v, Vec2d w) {
  return (v.x*w.x) + (v.y*w.y);
}

float Vec2d::sqlength() {
  return dot(*this, *this);
}

float Vec2d::length() {
  return std::sqrt(dot(*this, *this));
}

Vec2d Vec2d::norm() {
  return *this*(1.0/length());
}

Vec2d Vec2d::rotate(float angle) { //anti-clockwise with x to right and y up
  Vec2d r;
  float cosA = cos(angle);
  float sinA = sin(angle);
  r.x = x*cosA - y*sinA;
  r.y = x*sinA + y*cosA;
  return r;
}

Vec2d Vec2d::perp()
{
  Vec2d r;
  r.x = -y;
  r.y = x;

  return r;
}

float Vec2d::angle()
{
  return atan2(y,x);
}

float Vec2d::angle_abs(Vec2d v)
{
  return SafeAcos(dot(*this,v));
}

float Vec2d::angle_rel(Vec2d v)
{
  return atan2(v.y, v.x) - atan2(y,x);
}

////////////////// POINT2D_INT FUNCTIONS

/*Point2d_int Point2d_int::operator +(Vec2d v) {
  Point2d_int r;
  r.x = (int)((float)x + v.x + 0.5);
  r.y = (int)((float)y + v.y + 0.5);

  return r;
}

Point2d_int Point2d_int::operator -(Vec2d v) {
  Point2d_int r;
  r.x = (int)(((float)x - v.x) + 0.5);
  r.y = (int)(((float)y - v.y) + 0.5);

  return r;
}

Point2d_int Point2d::convert_to_intPoint2d(int SCALAR)
{
  Point2d_int p;
  p.x = (int)((this->x*SCALAR)+0.5);
  p.y = (int)((this->y*SCALAR)+0.5);
  return p;
}


bool Point2d_int::operator==(const Point2d_int &p) const
{
  return x == p.x && y == p.y;
}

Vec2d Point2d_int::operator -(const Point2d_int &q) const {
  Vec2d r;
  r.x = x - (float)q.x;
  r.y = y - (float)q.y;

  return r;
}

int Point2d_int::dist(Point2d_int p) {
  return (*this-p).length();
}

int Point2d_int::sqdist(Point2d_int p) {
  return (*this-p).sqlength();
}

bool Point2d_int::within_sector2(float xvec, float yvec, Vec2d LV, Vec2d RV)
{
  //using cross product

  //Check if left of LV
  if( ((LV.x*yvec) - (xvec*LV.y)) > 0) return false;

  //Check if right of s.center - RP
  if( ((RV.x*yvec) - (xvec*RV.y)) < 0) return false;
  else return true;
}

bool Point2d::within_sector(Sector s, float maxdistsq)
{
  Vec2d v = (*this)-s.center;  //vector from point to sector origin
  
  float distsq = v.sqlength();  //distance point to sector origin
  
  if(distsq > maxdistsq) //distance compared to max sector distance
    return false;

  if(distsq == 0) //at origin
    return true;

  //std::cout<<"vec angle "<<v.norm().angle_abs(s.dir)<<" max angle "<<s.angle_around_dir<<std::endl;
  //sqlength = dot (self, self)
  //norm = sqlength -> sqrt -> *1/length
  v = v*(1.0/(std::sqrt(distsq)));
  //angle abs = dot (self,vec v) -> acos

  return v.angle_abs(s.dir) < s.angle_around_dir; //within angle + distance
}

bool Point2d::within_sector(Sector s)
{
  Vec2d v = (*this)-s.center;  //vector from point to sector origin

  float distsq = v.sqlength();  //distance point to sector origin

  if(distsq > s.radius*s.radius) //distance compared to max sector distance
    return false;

  if(distsq == 0) //at origin
    return true;

  return v.norm().angle_abs(s.dir) < s.angle_around_dir; //within angle + distance
  }*/


/////////// POINT2D FUNCTIONS /////////////////////////////////////////////////////////

Point2d Point2d::operator +(Vec2d v) {
  Point2d r;
  r.x = x + v.x;
  r.y = y + v.y;

  return r;
}

Point2d Point2d::operator -(Vec2d v) {
  Point2d r;
  r.x = x - v.x;
  r.y = y - v.y;

  return r;
}

bool Point2d::operator==(const Point2d &p) const
{
  return x == p.x && y == p.y;
}

Vec2d Point2d::operator -(const Point2d &q) const {
  Vec2d r;
  r.x = x - q.x;
  r.y = y - q.y;

  return r;
}

float Point2d::dist(Point2d p) {
  return (*this-p).length();
}

float Point2d::sqdist(Point2d p) {
  return (*this-p).sqlength();
}

bool Point2d::within_sector2(float xvec, float yvec, Vec2d LV, Vec2d RV)
{
  //using cross product

  //Check if left of LV
  if( ((LV.x*yvec) - (xvec*LV.y)) > 0) return false;

  //Check if right of s.center - RP
  if( ((RV.x*yvec) - (xvec*RV.y)) < 0) return false;
  else return true;
}

bool Point2d::within_sector(Sector s, float maxdistsq)
{
  Vec2d v = (*this)-s.center;  //vector from point to sector origin
  
  float distsq = v.sqlength();  //distance point to sector origin
  
  if(distsq > maxdistsq) //distance compared to max sector distance
    return false;

  if(distsq == 0) //at origin
    return true;

  //std::cout<<"vec angle "<<v.norm().angle_abs(s.dir)<<" max angle "<<s.angle_around_dir<<std::endl;
  //sqlength = dot (self, self)
  //norm = sqlength -> sqrt -> *1/length
  v = v*(1.0/(std::sqrt(distsq)));
  //angle abs = dot (self,vec v) -> acos

  return v.angle_abs(s.dir) < s.angle_around_dir; //within angle + distance
}

bool Point2d::within_sector(Sector s)
{
  Vec2d v = (*this)-s.center;  //vector from point to sector origin

  float distsq = v.sqlength();  //distance point to sector origin

  if(distsq > s.radius*s.radius) //distance compared to max sector distance
    return false;

  if(distsq == 0) //at origin
    return true;

  return v.norm().angle_abs(s.dir) < s.angle_around_dir; //within angle + distance
}

/////////// SEGMENT FUNCTIONS /////////////////////////////////////////////////////////

Segment::Segment(float bx, float by, float ex, float ey) {
  /*if(px > qx || py > qy) {
    float tmpx = qx;
    qx = px;
    px = tmpx;

    float tmpy = qy;
    qy = py;
    py = tmpy;
    }*/

  B.x = bx; B.y = by; E.x = ex; E.y = ey;
}

float Segment::length() const {
  return (E - B).length();
}

/*
 * This code is an adaptation of the dist_Point_to_Segment() function
 * by Dan Sunday
 * (http://geomalgorithms.com/a02-_lines.html)
 */
float Segment::dist(Point2d p)
{
  Vec2d v = E - B;
  Vec2d w = p - B;

  double c1 = Vec2d::dot(w,v);

  if(c1 <= 0)
    return p.dist(B);

  double c2 = Vec2d::dot(v,v);

  if(c2 <= c1)
    return p.dist(E);

  double b = c1/c2;
  Point2d pb = E + (v*b);
  return p.dist(pb);
}

float Segment::dist(Segment T)
{
  float sc, tc;
  return dist(T, &sc, &tc);
}

/*
 * This code is an adaptation of the dist3D_Segment_to_Segment() function
 * by Dan Sunday
 * (http://www.softsurfer.com/Archive/algorithm_0106/algorithm_0106.htm).
 */
float Segment::dist(Segment T, float *sc, float *tc)
{
  // vector of and between the segments
  Vec2d   u = E - B;
  Vec2d   v = T.E - T.B;
  Vec2d   w = B - T.B;

  // dot products of vectors
  float    a = Vec2d::dot(u,u);    // always >= 0
  float    b = Vec2d::dot(u,v);
  float    c = Vec2d::dot(v,v);    // always >= 0
  float    d = Vec2d::dot(u,w);
  float    e = Vec2d::dot(v,w);

  // nominators and denominators for sc and tc
  float    D = a*c - b*b;   // always >= 0
  float    sN, sD = D;      // sc = sN / sD, default sD = D >= 0
  float    tN, tD = D;      // tc = tN / tD, default tD = D >= 0

  // compute the line parameters of the two closest points
  if (D < FLT_EPSILON) {    // the lines are almost parallel
    sN = 0.0;               // force using point B on segment S
    sD = 1.0;               // to prevent possible division by 0.0 later
    tN = e;
    tD = c;
  }
  else {                    // get the closest points on the infinite lines
    sN = (b*e - c*d);
    tN = (a*e - b*d);
    if (sN < 0.0) {         // sc < 0 => the s=0 edge is visible
      sN = 0.0;
      tN = e;
      tD = c;
    }
    else if (sN > sD) {     // sc > 1 => the s=1 edge is visible
      sN = sD;
      tN = e + b;
      tD = c;
    }
  }

  if (tN < 0.0) {           // tc < 0 => the t=0 edge is visible
    tN = 0.0;
    // recompute sc for this edge
    if (-d < 0.0)
      sN = 0.0;
    else if (-d > a)
      sN = sD;
    else {
      sN = -d;
      sD = a;
    }
  }
  else if (tN > tD) {      // tc > 1 => the t=1 edge is visible
    tN = tD;
    // recompute sc for this edge
    if ((-d + b) < 0.0)
      sN = 0.0;
    else if ((-d + b) > a)
      sN = sD;
    else {
      sN = (-d + b);
      sD = a;
    }
  }
  // finally do the division to get sc and tc
  // NOTE: we have to use the c++ abs() than can distinguish floats and ints
  *sc = (std::abs(sN) < FLT_EPSILON ? 0.0 : sN / sD);
  *tc = (std::abs(tN) < FLT_EPSILON ? 0.0 : tN / tD);

  // get the difference of the two closest points
  Vec2d dP = w + (u * *sc) - (v * *tc);  // = S(sc) - T(tc)

  return dP.length();   // return the closest distance
}

std::vector<Point2d> Segment::split(float split_length)
{
  float length = this->length();
  float num_splits = length/split_length;
  float split_interval = 1.0/num_splits;
  Vec2d v = E-B;

  std::vector<Point2d> points;

  float curr_length = 0.0;
  for(int i = 0; i < num_splits+1; i++)
    {
      Point2d p = B+(v*curr_length);
      points.push_back(p);

      curr_length += split_interval;
    }

  return points;
}

/////////// SECTOR FUNCTIONS /////////////////////////////////////////////////////////

Sector::Sector(Point2d c, float r, Vec2d d, float a)
{
  center = c;
  radius = r;
  dir = d;
  angle_around_dir = a;
}

Rectangle Sector::mbr()
{
  Rectangle r;
  r.min.x = center.x-radius;
  r.min.y = center.y-radius;
  r.max.x = center.x+radius;
  r.max.y = center.y+radius;

  return r;
}

std::ostream& operator<<(std::ostream& output, const Point2d& p) {
    output << "(" <<  p.x << ", " << p.y <<")";
    return output;  // for multiple << operators.
}
