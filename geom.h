#ifndef _GEOM_H_GUARD_
#define _GEOM_H_GUARD_

#include <vector>
#include <iostream>

// forward declarations
class Rectangle;
class Sector;

class Vec2d
{
 public:
  float x, y;
  Vec2d operator + (Vec2d);
  Vec2d operator - (Vec2d);
  Vec2d operator * (float);
  float sqlength();
  float length();
  Vec2d norm();
  Vec2d rotate(float angle);
  Vec2d perp();
  float angle();
  float angle_abs(Vec2d v);
  float angle_rel(Vec2d v);
  static float dot(Vec2d v, Vec2d w);
};

/*class Point2d_int
{
  friend std::ostream& operator<<(std::ostream& output, const Point2d_int& p);
 public:
  int x, y;
  Point2d_int() { x = 0; y = 0; };
  Point2d_int(int nx, int ny) { x = nx; y = ny; };
  Point2d_int operator + (Vec2d);
  Point2d_int operator - (Vec2d);
  //Point2d convert_to_flPoint2d();
  bool operator==(const Point2d_int &p) const;
  Vec2d operator - (const Point2d_int &p) const;
  int dist(Point2d_int p);
  int sqdist(Point2d_int p);
  bool within_sector2(float xvec, float yvec, Vec2d LP, Vec2d RP);
  };*/


class Point2d
{
  friend std::ostream& operator<<(std::ostream& output, const Point2d& p);
 public:
  float x, y;
  Point2d() { x = 0; y = 0; };
  Point2d(float nx, float ny) { x = nx; y = ny; };
  Point2d operator + (Vec2d);
  Point2d operator - (Vec2d);
  //Point2d_int convert_to_intPoint2d(int SCALAR);

  bool operator==(const Point2d &p) const;
  Vec2d operator - (const Point2d &p) const;
  float dist(Point2d p);
  float sqdist(Point2d p);
  bool within_sector(Sector s);
  bool within_sector(Sector s, float maxdist);
  bool within_sector2(/*Sector s, float maxdist,*/float xvec, float yvec, Vec2d LP, Vec2d RP);
};

class Segment
{
 public:
  Point2d B, E;
  Segment() {};
  Segment(float bx, float by, float ex, float ey);
  float length() const;
  float dist(Point2d p);
  float dist(Segment T);
  float dist(Segment T, float *sc, float *tc);
  std::vector<Point2d> split(float split_length);
};

class Sector
{
 public:
  Sector(Point2d c, float r, Vec2d d, float a);
  Point2d center;
  float radius;
  Vec2d dir;
  float angle_around_dir;
  Rectangle mbr();
};

#endif // _GEOM_H_GUARD_
