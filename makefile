CC		= g++
STD_CFLAGS	= -Wall -Wextra -Wpedantic
PAR_FLAGS       = -fopenmp
OPT_FLAGS       = -O3
DEBUG_FLAGS	= -g
PROFILE_FLAGS	= -pg
RSTAR_OBJECTS	= rstartree.o storage.o util.o rectangle.o
SUPPORT_OBJECTS = random.o geom.o config.o
ENV_OBJECTS     = environment.o aapjes.o patch.o resource.o
STDOBJECTS	= main.o $(ENV_OBJECTS) $(RSTAR_OBJECTS) $(SUPPORT_OBJECTS)

PROFILE		= OFF
ifeq ($(PROFILE), ON)
CFLAGS		= $(PROFILE_FLAGS) $(STD_CFLAGS)
else
CFLAGS		= $(STD_CFLAGS)
endif

DEBUG		= OFF
ifeq ($(DEBUG), ON)
CFLAGS1		= $(DEBUG_FLAGS) $(CFLAGS)
else
CFLAGS1		= $(CFLAGS) $(OPT_FLAGS)
endif

PARALLEL	= OFF
ifeq ($PARALLEL), ON)
CFLAGS2		= $(PAR_FLAGS) $(CFLAGS1)
else
CFLAGS2		= $(CFLAGS1)
endif


GRAPHICS 	= OFF
ifeq ($(GRAPHICS), ON)
LIBS		= -lGL -lGLU -lglut -lpng
#-D_GRAPHICS_ turns on graphics
OURFLAGS	= -D_GRAPHICS_
OBJECTS		= graphics.o $(STDOBJECTS)
else
OBJECTS		= $(STDOBJECTS)
endif

aapjes: $(OBJECTS)
	$(CC) $(OURFLAGS) $(CFLAGS2) -o aapjes $(OBJECTS) $(LIBS)

# delete compiled files
clean:
	rm -f aapjes
	rm -f *.o *.a

# general rule that can compile almost everything
%.o: %.cpp %.h
	$(CC) $(OURFLAGS) $(CFLAGS2) -c $<

%.o: rstartree/%.cc rstartree/%.h
	$(CC) $(CFLAGS2) -c $<
