#include <float.h>
#include <cmath>
#include "segment.h"
#include "point2d.h"
#include "vec2d.h"


Segment::Segment(float bx, float by, float ex, float ey) { 
  /*if(px > qx || py > qy) {
    float tmpx = qx;
    qx = px;
    px = tmpx;

    float tmpy = qy;
    qy = py;
    py = tmpy;
    }*/

  B.x = bx; B.y = by; E.x = ex; E.y = ey; 
};

float Segment::length() {
  return (E - B).length();
}

float Segment::dist(Segment T) 
{
  float sc, tc;
  return dist(T, &sc, &tc);
}

/*
 * This code is an adaptation of the dist3D_Segment_to_Segment() function
 * by Dan Sunday 
 * (http://www.softsurfer.com/Archive/algorithm_0106/algorithm_0106.htm).
 */
float Segment::dist(Segment T, float *sc, float *tc)
{
  // vector of and between the segments
  Vec2d   u = E - B;
  Vec2d   v = T.E - T.B;
  Vec2d   w = B - T.B;

  // dot products of vectors
  float    a = Vec2d::dot(u,u);    // always >= 0
  float    b = Vec2d::dot(u,v); 
  float    c = Vec2d::dot(v,v);    // always >= 0
  float    d = Vec2d::dot(u,w); 
  float    e = Vec2d::dot(v,w); 

  // nominators and denominators for sc and tc
  float    D = a*c - b*b;   // always >= 0
  float    sN, sD = D;      // sc = sN / sD, default sD = D >= 0
  float    tN, tD = D;      // tc = tN / tD, default tD = D >= 0

  // compute the line parameters of the two closest points
  if (D < FLT_EPSILON) {    // the lines are almost parallel
    sN = 0.0;               // force using point B on segment S
    sD = 1.0;               // to prevent possible division by 0.0 later
    tN = e;
    tD = c;
  }
  else {                    // get the closest points on the infinite lines
    sN = (b*e - c*d);
    tN = (a*e - b*d);
    if (sN < 0.0) {         // sc < 0 => the s=0 edge is visible
      sN = 0.0;
      tN = e;
      tD = c;
    }
    else if (sN > sD) {     // sc > 1 => the s=1 edge is visible
      sN = sD;
      tN = e + b;
      tD = c;
    }
  }
  
  if (tN < 0.0) {           // tc < 0 => the t=0 edge is visible
    tN = 0.0;
    // recompute sc for this edge
    if (-d < 0.0)
      sN = 0.0;
    else if (-d > a)
      sN = sD;
    else {
      sN = -d;
      sD = a;
    }
  }
  else if (tN > tD) {      // tc > 1 => the t=1 edge is visible
    tN = tD;
    // recompute sc for this edge
    if ((-d + b) < 0.0)
      sN = 0.0;
    else if ((-d + b) > a)
      sN = sD;
    else {
      sN = (-d + b);
      sD = a;
    }
  }
  // finally do the division to get sc and tc
  // NOTE: we have to use the c++ abs() than can distinguish floats and ints
  *sc = (std::abs(sN) < FLT_EPSILON ? 0.0 : sN / sD);
  *tc = (std::abs(tN) < FLT_EPSILON ? 0.0 : tN / tD);
    
  // get the difference of the two closest points
  Vec2d dP = w + (u * *sc) - (v * *tc);  // = S(sc) - T(tc)

  return dP.length();   // return the closest distance
}
