#ifndef _SEGMENT_H_GUARD_
#define _SEGMENT_H_GUARD_

#include "point2d.h"

class Segment {
public:
  Point2d B, E;
  Segment() {};
  Segment(float bx, float by, float ex, float ey);
  float length();
  float dist(Segment T);
  float dist(Segment T, float *sc, float *tc);
};

#endif // _SEGMENT_H_GUARD_
