/*
 Copyright (C) 2010 by The Regents of the University of California
 
 Redistribution of this file is permitted under
 the terms of the BSD license.
 
 Date: 11/01/2009
 Author: Sattam Alsubaiee <salsubai (at) ics.uci.edu>
         Shengyue Ji <shengyuj (at) ics.uci.edu>
*/

#ifndef _RECTANGLE_H_GUARD
#define _RECTANGLE_H_GUARD

#include "point2d.h"

class Rectangle
{
public:
    // the mbr's lower value
    Point2d min;
    // the mbr's upper value
    Point2d max;

    // check whether two rectangles intersect or not
    bool intersects(const Rectangle &rect) const;
    // check whether two rectangles touch each other
    bool touches(const Rectangle &rect) const;
    // check whether the rectangle is contained in the other rectangle
    bool contains(const Rectangle &rect) const;
    // return the squared minimum distance between a point and a rectangle
    double minDist2(Point2d p) const;
    // return the rectangle's area
    double area() const;
    // return the enlarged area needed to include an object
    double enlargedArea(const Rectangle &rect) const;
    // enlarge an object into a rectangle
    void enlarge(const Rectangle &rect);
    // return the overlapped area between two rectangles
    double overlapedArea(const Rectangle &rect) const;
    // return the perimeter of a rectangle
    double margin() const;
    
    bool operator==(const Rectangle &r) const;
};

#endif // _RECTANGLE_H_GUARD
