#ifndef _POINT2D_H_GUARD_
#define _POINT2D_H_GUARD_

// forward declarations
class Vec2d;

class Point2d {
public:
  float x, y;
  Point2d() { x = 0; y = 0; };
  Point2d(float nx, float ny) { x = nx; y = ny; };
  Point2d operator + (Vec2d);
  Point2d operator - (Vec2d);
  bool operator==(const Point2d &p) const;
  Vec2d operator - (Point2d);
};

#endif // _POINT2D_H_GUARD_
