#include <cmath>
#include "vec2d.h"

Vec2d Vec2d::operator +(Vec2d w) {
  Vec2d r;
  r.x = x + w.x;
  r.y = y + w.y;

  return r;
}

Vec2d Vec2d::operator -(Vec2d w) {
  Vec2d r;
  r.x = x - w.x;
  r.y = y - w.y;

  return r;
}

Vec2d Vec2d::operator *(float s) {
  Vec2d r;
  r.x = x * s;
  r.y = y * s;
  return r;
}

float Vec2d::dot(Vec2d v, Vec2d w) {
  return (v.x*w.x) + (v.y*w.y);
}

float Vec2d::sqlength() {
  return dot(*this, *this);
}

float Vec2d::length() {
  return sqrt(dot(*this, *this));
}

Vec2d Vec2d::norm() {
  return *this*(1.0/length());
}

Vec2d Vec2d::rotate(float angle) {
  Vec2d r;
  r.x = x*cos(angle) - y*sin(angle);
  r.y = x*sin(angle) + y*cos(angle);
  return r;
}
