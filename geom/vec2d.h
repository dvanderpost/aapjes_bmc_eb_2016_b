#ifndef _VEC2D_H_GUARD_
#define _VEC2D_H_GUARD_

class Vec2d {
public:
  float x, y;
  Vec2d operator + (Vec2d);
  Vec2d operator - (Vec2d);
  Vec2d operator * (float);
  float sqlength();
  float length();
  Vec2d norm();
  Vec2d rotate(float angle);
  static float dot(Vec2d v, Vec2d w);
};

#endif // _VEC2D_H_GUARD_
