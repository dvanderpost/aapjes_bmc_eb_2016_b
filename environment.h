#ifndef _ENVIRONMENT_H_GUARD
#define _ENVIRONMENT_H_GUARD

#include <vector>
#include <list>
#include <queue>
#include "constants.h"
#include "patch.h"
#include "resource.h"
#include "aapjes.h"
#include <set>

// forward declarations
class Storage; //storage.h
class RTree;   //rstartree.h
class Patch;   //patch.h
class Resource; //resource.h
class Rectangle; 
class Config;
class Aapje;    //aapje.h

class Group
{
  //private:
 public: 
  //functions
  Group(int new_group_id); //INITIALIZE
  ~Group();        //DESTRUCTOR
  void RemoveAapje(Aapje* aap);
  void RemoveAndCheckForNewLeader(Aapje* aap);
  void AddAapje(Aapje* aap);
  void ChooseNewLeader();
  Aapje* ReturnLeader();
  void NewRandomLeader();

  //variables
  int id;
  bool moved;
  std::vector<Aapje*> members;
};


class Environment
{
private:
  
  //////////////////////////VARIABLES
  Storage *st_resources;
  RTree *rstar_resources;
  Storage *st_patches;
  RTree *rstar_patches;

  //GRAPHICS
  bool DISPLAY_EVERY_ACTION;
  int DISPLAY_TIME_INTERVAL; 

  //SCALING
  long cont_time;  //minutes / TIME_SCALAR
  long global_time_units;

  //RESOURCES
  int number_res_types; //NEEDED?
  int appear_type;
  int residence_time;
  int quality_type;
  float Q_STDEV;
  float Q_MEAN;
  float NEGATIVE_QUALITY_SCALAR;
  int MIN_N;
  int MAX_N;
  float MAX_H, MIN_H;
  int LEARN_TYPE;

  int PATCH_RES_TYPES;
  int NUMBER_RES_ITEMS_PER_PATCH;
  float patch_size;
  int number_patches;
  int distr_res_in_patch; 
  int PATCH_DISTRIBUTION;
  int number_res_types_per_patch;
  int subset_res_types_per_patch;

  int UNIFORM_RES_TYPES;
  float UNIFORM_DENSITY; 
  
  int RANDOM_RES_TYPES;
  float RANDOM_DENSITY;

  //AAPJES
  float SEL_COEF;

  //SIMULATION TYPE
  bool TRANS_OPTION_GRADUAL_GROUP_BUILD_UP;
  bool TRANS_OPTION_MOST_EXPERIENCED_SOLITARY;
  bool BIRTH_DEATH;
  long GROUPING_TIME;
  int SWITCH_LEADER_TIME;
  bool GROUPING_SWITCH;
  int GROUPING_STYLE;

  int MAX_GROUP_SIZE2;
  int HALF_MAX_GROUP_SIZE;

  /////////////// FUNCTIONS
  void InitVariables(Config c);
  void InitRtrees();
  void DeleteRtrees();
  void InitEnvironment();

  void InitAapjes(Config c);

  void GenerateResourceTypes();
  long GenerateAppearanceTime(int r);
  float GenerateQuality(int r);
  int GenerateN();
  float GenerateHPowN(int n);

  void GeneratePatches();
  void PositionPatches();
  void PositionPatchesRandomly();
  void PositionPatchesRegularly();
  void GeneratePatchyResources();
  void GeneratePatchyResources_RandomVec(Patch *patch);
  void GeneratePatchyResources_Random(Patch *patch);
  void GeneratePatchyResources_Grid(Patch *patch);

  void GenerateUniformResources(); 

  void GenerateRandomResources(); 

  void GenerateResource(Point2d pos, int res_type);  

  //ACTION SCHEDULING AND LIFEHISTORY
  void DiscreteUpdate(Config c);
  void EnvChange();
  void ChangeResource(int r);
  void TransmissionChain();
  Aapje* AapjeReproduction(Aapje* mom);
  Aapje* AapjeReproduction(Aapje* mom, Aapje* dead);
  void CheckForGroupSplit(int group_id);
  void RemoveAndCheckSplitGroup(Aapje* aap);

  Aapje* SelectMom();
  void KillAapjes(std::vector<Aapje*> theDead);
  void KillAapje(Aapje* dead);
  void DataOutput();
  bool SAVE_RESOURCETYPES;

  void ExpCondition(Config c);

 public:
  //VARIABLES
  int YEAR;
  int DAY;
  long global_time; //minutes
  long SIMULATION_TIME;    //minutes
  int XFIELD, YFIELD;
  int TIME_SCALAR;
  int POPULATION_SIZE_AAPJES;
  int POPULATION_STYLE;
  int MAX_GROUP_SIZE;
  bool REPLACE_DEAD;
  int GENOME_RANGE;
  float GR_MIN, GR_MAX;
  int GENOME_RANGE_GROUP;

  float AAP_DEATH_RATE;
  int MAX_AGE;

  std::vector< std::vector< std::vector< Resource*> > > resourcesGRID;
  std::vector<Patch*> patches;
  std::vector<Resource*> resources;
  std::vector<ResourceType*> resourcetypes; 
  std::vector<Aapje*> aapjes;
  std::vector<Group> groups;
  std::list<Aapje*> aapx;

  //FUNCTIONS
  Environment(Config c); //INITIALIZE
  ~Environment();        //DESTRUCTOR

  void Simulate(Config c);      //RUN SIMULATION

  std::vector<Resource*> ResourcesWithin(Rectangle r);
  std::vector<Patch*> PatchesWithin(Rectangle r);
  std::vector<Patch*> PatchesWithinGRID(Rectangle r);
 std::vector<Resource*> ResourcesWithinGRID(int minx, int maxx, int miny, int maxy);
 void ResourcesWithinGRID(std::vector<Resource*> *resources, int minx, int maxx, int miny, int maxy);

  int num_resource_items(); //REDUNDANT?
  int num_patches(); //NEEDED? should be parameter 
  bool ResourceIsAvailable(Resource *r);
  bool CheckPositionIsOnGrid(Point2d pos);

  int NUM_GROUPS;

  bool SAVE_PREFS;
  bool SAVE_DIET;
  bool SAVE_BEH_ENERGY;
  bool SAVE_BEH_CHOICES;
  bool SAVE_GROUP_IND;
  bool SAVE_GROUP_ENV;
  bool SAVE_SOC_LEARN;
  int SAVE_TIME;

  int TRIAL_RATE_SWITCH;
  bool LEARN_UPDATE_SWITCH;
  bool TEMP_STIMULUS_SWITCH;
  bool SOC_SKILL_LEARN_SWITCH;
  bool SKILL_LEARN;
  int EXPLORE_SWITCH;
  int P_SOCIAL_STIMULUS_SWITCH;
  int SKILL_LEARN_TYPE;
  bool INIT_PREF_SWITCH;
  bool EXPLORE_REL_SWITCH;
  bool SOLITARY_SOCIAL_LEARNING;

  bool BIAS_F_SWITCH,BIAS_A_SWITCH,BIAS_T_SWITCH,BIAS_P_SWITCH,BIAS_M_SWITCH,BIAS_R_SWITCH;
  bool BIASES_SWITCH, CHOOSE_OLDEST;

  int ENVIRONMENTAL_CHANGE;
  int ENVIRONMENTAL_CHANGE_TIME;
  float ENVIRONMENTAL_CHANGE_RATE;
  bool RANDOM_AGE_DISTRIBUTION;

  bool TRANSMISSION_CHAIN;

  int TRAJECTORY_LENGTH_AAP;
  int EXP_CONDITION;
  float MUTATION_RATE;
};

#endif // _ENVIRONMENT_H_GUARD
