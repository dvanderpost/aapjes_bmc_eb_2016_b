#ifndef _GRAPHICS_H_GUARD_
#define _GRAPHICS_H_GUARD_

#include "environment.h"
#include "aapjes.h"

// forward declarations
class Config;

namespace graphics
{
  enum display
  {
    TREE_DISPLAY,
    BRANCH_DISPLAY,
    INDIVIDUAL_DISPLAY
  };
  extern display display_mode;
  extern bool running;
  extern bool updating;
  extern bool stepwise;
  
  void init(Config c);
  void update();
  void set_environment(Environment *env);
  void set_population(std::vector<Aapje*> pop);
  void SetFollowTarget();
  void ToggleFollow();
}
#endif // _GRAPHICS_H_GUARD_
