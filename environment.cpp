#ifdef _GRAPHICS_
#include "graphics.h"
#endif

#include "rstartree/rstartree.h"
#include "environment.h"
#include "resource.h"
#include "patch.h"
#include "constants.h"
#include "random.h"
#include "config.h"
#include <iostream>
#include <math.h>
#include <set>

using std::cout;
using std::cin;
using std::endl;
using std::vector;

//////////////////////////////////////////////////////////////////
//Defining Group

Group::Group(int new_group_id)
{
  this->id = new_group_id;
  this->members.clear();
  this->moved=true; //set so that at beginning all will check for safety
}

Group::~Group()
{
  //Don't delete here: they are deleted from aapjes
  //for(std::vector<Aapje*>::iterator it = this->members.begin(); it != this->members.end(); ++it)
  //delete *it;
  this->members.clear();
}

void Group::RemoveAapje(Aapje* aap)
{
  for(std::vector<Aapje*>::iterator it=this->members.begin(); it!=this->members.end(); ++it)
    {
      if((*it)==aap){this->members.erase(it); break;}
    }
}

Aapje* Group::ReturnLeader()
{
  if(this->members.empty()) return NULL;
  else return this->members.front();
}

void Group::NewRandomLeader()
{
  //WARNING: should only be done on groups where !this->members.empty
  //get random individual
  int ran_ind = (int)(env_uni()*(int)this->members.size());
  Aapje* rand_aap = this->members[ran_ind];

  //move to front of group
  this->RemoveAapje(rand_aap);
  this->members.insert(this->members.begin(), rand_aap); //Change to deque / list?
  //all members make the first individual in members array their new leader
  this->ChooseNewLeader();
}

void Group::ChooseNewLeader()
{
  for(std::vector<Aapje*>::iterator it=this->members.begin(); it!=this->members.end(); ++it)
    {
      if(it==this->members.begin()) (*it)->follow_target=NULL;
      else (*it)->follow_target = this->members.front(); 
      //first NULL, and then first;
    }
}

void Group::RemoveAndCheckForNewLeader(Aapje* aap)
{
  this->RemoveAapje(aap); //aap is erased but can be accessed
  if(aap->follow_target==NULL) //if the dead was the former leader
    this->ChooseNewLeader();
}

void Environment::RemoveAndCheckSplitGroup(Aapje* aap)
{
  //if(aap->group_id >= (int)this->groups.size())
  //{
  //  std::cout<<"ERROR: group_id >= groups.size\n";
  //  std::cout<<"environment.cpp: RemoveAndCheckSplitGroup()\n";
  //  std::exit(0);
  //}

  this->groups[aap->group_id].RemoveAapje(aap); //aap is erased but can be accessed
  
  //first check on follow target after someone died! + find oldest
  Aapje* oldest = (*this->groups[aap->group_id].members.begin());
  for(std::vector<Aapje*>::iterator it=this->groups[aap->group_id].members.begin(); it!=this->groups[aap->group_id].members.end(); ++it)
    {
      if((*it) == aap)
	{
	  std::cout<<"ERROR: *it "<<(*it)->id<<" is aap! "<<aap->id<<std::endl;
	  std::exit(0);
	}

      if((*it)->follow_target == aap) 
	{
	  (*it)->follow_target = aap->follow_target; //could be NULL!
	  if((*it)->follow_target)
	    std::cout<<"Ind "<<(*it)->id<<" followed "<<aap->id<<"; Will follow "<<(*it)->follow_target->id<<std::endl;
	  else
	    std::cout<<"Ind "<<(*it)->id<<" followed "<<aap->id<<"; Will follow nobody"<<std::endl;
	}
      if((*it)->age > oldest->age) oldest = (*it); 
    }
  std::cout<<"Oldest "<<oldest->id<<" age "<<oldest->age<<std::endl;
  //std::cin.get();
  
  if(aap->follow_target==NULL)
    {
      std::cout<<"Aap follow target was NULL; Will check if new groups are needed\n";
      //for(std::vector<Aapje*>::iterator it2=this->groups[aap->group_id].members.begin(); it2!=this->groups[aap->group_id].members.end(); ++it2)
      //{
      //std::cout<<"Group members: ind "<<(*it2)->id<<std::endl;
      //}
      //std::cin.get();

      // ---- for all [!oldest] && [follow target == NULL] [+ depedents] -> new_group_id
      int new_group_id = this->groups.size()-1;
      for(std::vector<Aapje*>::iterator it2=this->groups[aap->group_id].members.begin(); it2!=this->groups[aap->group_id].members.end(); ++it2)
	{
	  if((*it2)!=oldest && (*it2)->follow_target==NULL)
	    {
	      std::cout<<"Ind "<<(*it2)->id<<" is independent leader and needs new group\n";

	      //this counts as need for new group
	      new_group_id++; // = this->groups.size();

	      std::cout<<"New group is "<<new_group_id<<std::endl;

	      //Find all that should be in this group	      
	      (*it2)->group_id = new_group_id;
	      std::list<Aapje*> leaders; leaders.push_back(*it2);

	      while(!leaders.empty())
		{
		  //take first leader
		  Aapje* focal = *leaders.begin();

		  //get through group who is dependent
		  for(std::vector<Aapje*>::iterator itt=this->groups[aap->group_id].members.begin(); itt!=this->groups[aap->group_id].members.end(); ++itt)
		    {
		      if((*itt)->follow_target == focal)
			{
			  //change their group id
			  //add to vector leaders
			  leaders.push_back(*itt);
			  std::cout<<"Ind "<<(*itt)->id<<" depends on "<<focal->id<<" and will join group "<<new_group_id<<std::endl;
			  (*itt)->group_id = new_group_id;
			}
		      else
			std::cout<<"Ind "<<(*itt)->id<<" does not depend on "<<focal->id<<" and will not join group "<<new_group_id<<std::endl;
		    }//for group members	
		  //remove first leader from vector
		  leaders.pop_front(); //removes first element
		}
	      //std::cin.get();	      
	    }//end if not oldest but is independent
	}//for group members
      //std::cin.get();

      //Make new groups:
      for(int g=this->groups.size(); g<new_group_id+1; g++)
	{
	  Group new_group(g);
	  this->groups.push_back(new_group);
	  this->groups[g].members.reserve(this->MAX_GROUP_SIZE);
	}
      
      //Go through list and remove those with different group_id:
      std::vector<Aapje*>::iterator it3=this->groups[aap->group_id].members.begin();
      Aapje* focal;
      while(it3!=groups[aap->group_id].members.end())
	{
	  if((*it3)->group_id != aap->group_id)
	    {
	      std::cout<<"Ind "<<(*it3)->id<<" will be moved to group "<<(*it3)->group_id<<std::endl;
	      focal = *it3; //store focal
	      it3 = this->groups[aap->group_id].members.erase(it3); //the iterator now points to next
	      if(focal->group_id < (int)this->groups.size())
		this->groups[focal->group_id].AddAapje(focal); //add to its new group
	      else
		{
		  std::cout<<"Focal group size "<<focal->group_id<<" is too large for groupsize array "<<this->groups.size()+1<<"\n";
		  std::exit(0);
		}
	    }
	  else ++it3;
	}
      //std::cin.get();
    }//end if follow_target == NULL
  else
    {
      std::cout<<"Aap was not independent, so no new groups needed\n";
    }
}

void Group::AddAapje(Aapje* aap)
{
  this->members.push_back(aap);
}

//Allows group to grow to MAX SIZE (MAX_GROUP_SIZE) then split in two (MAX_GROUP_SIZE/2); after every AddAapje check group size; Make new group + add half one group (randomly) to new group + remove from old group + change group_id;
void Environment::CheckForGroupSplit(int group_id)
{
  //if(group_id >= (int)this->groups.size())
  //{
  //  std::cout<<"ERROR: group_id >= groups.size\n";
  //  std::cout<<"environment.cpp: CheckForGroupSplit()\n";
  //  std::exit(0);
  //}

  //split if too big
  if((int)this->groups[group_id].members.size()>=this->MAX_GROUP_SIZE)
    {
      int new_group_id = this->groups.size();
      Group new_group(new_group_id);
      this->groups.push_back(new_group);
      this->groups[new_group_id].members.reserve(this->MAX_GROUP_SIZE);
      
      for(int n = 0; n<this->HALF_MAX_GROUP_SIZE; n++)
	{
	  //select random ind
	  int rand_id = (int)(this->groups[group_id].members.size()*env_uni());
	  Aapje* rand_aap = this->groups[group_id].members[rand_id];
	  //remove from group
	  this->groups[group_id].RemoveAapje(rand_aap);
	  //add to new group
	  this->groups[new_group_id].AddAapje(rand_aap);
	  //change groupID
	  rand_aap->group_id = new_group_id;
	}
      //std::cin.get();

      //After sorting groups -> make sure a single leader is established
      this->groups[group_id].ChooseNewLeader();
      this->groups[new_group_id].ChooseNewLeader();
    }
  //else std::cout<<"Will not split group\n";    
}

//TODO: migration? (from small to optimum size?)
//TODO: allow groups to die?: check when ever removeaap is done.
//PROBLEM: will have to change all group_ids of all individuals in groups with greater ID
//OPTION: leave these in group array but empty, and ignore

///////////////////////////////////////////////////////////////////
//Defining Priotiy Queue
class Compare
{
public:
    bool operator() (const Aapje * left, const Aapje * right)
    {
      return left->t_a > right->t_a;
    }
};

std::priority_queue<Aapje*,
                    std::vector<Aapje *, std::allocator<Aapje*> >,
		    Compare> AapjeQ;


/////////////////////////////////////////////////////////////////////
//CONSTRUCTOR
Environment::Environment(Config c)
{
  this->InitVariables(c);
  if(RSTAR) this->InitRtrees();
  this->InitEnvironment();
  this->InitAapjes(c);

  //INCLUDE GRAPHICS OR NOT
#ifdef _GRAPHICS_
  graphics::init(c);
#endif

}

//DESTRUCTOR
Environment::~Environment()
{
  if(RSTAR)
    {
      this->DeleteRtrees();

      for(std::vector<Resource*>::iterator it = resources.begin(); it != resources.end(); ++it)
	delete *it;
      this->resources.clear();
    }
  else
    {
      for(int x=0; x<this->XFIELD; x++)
	for(int y=0; y<this->XFIELD; y++)
	  {
	    for(std::vector<Resource*>::iterator it = resourcesGRID[x][y].begin(); it != resourcesGRID[x][y].end(); ++it)
	      {
		delete *it;
	      }	  
	    this->resourcesGRID[x][y].clear();
	  }
    }

  for(std::vector<ResourceType*>::iterator it = resourcetypes.begin(); it != resourcetypes.end(); ++it)
    delete *it;
  this->resourcetypes.clear();

  for(std::vector<Patch*>::iterator it = patches.begin(); it != patches.end(); ++it)
    delete *it;
  this->patches.clear();

  this->groups.clear();

  for(std::vector<Aapje*>::iterator it = aapjes.begin(); it != aapjes.end(); ++it)
    delete *it;
  this->aapjes.clear();
}

////////////////////////////////////////////////////////////////////
//READ IN VARIABLES FROM CONFIG FILE
void Environment::InitVariables(Config c)
{
  this->MUTATION_RATE = c.get<float>("MUTATION_RATE");

  this->TIME_SCALAR = c.get<int>("TIME_SCALAR");

  //scaling
  this->XFIELD = c.get<int>("XFIELD");
  this->YFIELD = c.get<int>("YFIELD");

  this->DAY =  c.get<int>("DAY");             //in minutes
  this->YEAR =  c.get<int>("YEAR")*this->DAY; //in minutes
  this->SIMULATION_TIME = (long)(c.get<float>("SIMULATION_TIME")*this->YEAR);
  
  //graphics
  this->DISPLAY_EVERY_ACTION = c.get<bool>("DISPLAY_EVERY_ACTION");
  this->DISPLAY_TIME_INTERVAL = c.get<int>("DISPLAY_TIME_INTERVAL");

  //resource types
  this->appear_type = c.get<long>("appear_type");
  this->residence_time = c.get<long>("residence_time")*this->YEAR;
  this->quality_type = c.get<int>("quality_type");
  this->Q_STDEV = c.get<float>("Q_STDEV");
  this->Q_MEAN = c.get<float>("Q_MEAN");
  this->NEGATIVE_QUALITY_SCALAR = c.get<float>("NEGATIVE_QUALITY_SCALAR");
  this->MIN_N = c.get<int>("MIN_N");
  this->MAX_N = c.get<int>("MAX_N");
  if(this->MIN_N>this->MAX_N)
    {
      std::cout<<"ERROR MIN N > MAX N! Check Parameters!\n";
      std::exit(0);
    }

  this->MAX_H = c.get<float>("MAX_H")*this->DAY;
  this->MIN_H = c.get<float>("MIN_H")*this->DAY;
  if(this->MIN_H>this->MAX_H)
    {
      std::cout<<"ERROR MIN H > MAX H! Check Parameters!\n";
      std::exit(0);
    }
  this->LEARN_TYPE = c.get<int>("LEARN_TYPE");
 
  //patches
  this->PATCH_RES_TYPES = c.get<int>("PATCH_RES_TYPES");
  this->NUMBER_RES_ITEMS_PER_PATCH=c.get<int>("NUMBER_RES_ITEMS_PER_PATCH");
  this->number_patches = c.get<int>("number_patches");
  this->distr_res_in_patch = c.get<int>("distr_res_in_patch");
  this->patch_size = c.get<float>("patch_size");
  this->number_res_types_per_patch =  c.get<int>("number_res_types_per_patch");
  if(this->PATCH_RES_TYPES%this->number_res_types_per_patch!=0)
    {
      std::cout<<"ERROR! number_res_types_per_patch not a factor of patchrestypes. CHECK PARAMETERS!\n";
      std::exit(0);     
    }
  this->subset_res_types_per_patch =  c.get<int>("subset_res_types_per_patch");
  if(this->number_res_types_per_patch<this->subset_res_types_per_patch)
    {
      std::cout<<"ERROR! subset_res_types_per_patch > number_res_types_per_patch. CHECK PARAMETERS!\n";
      std::exit(0);     
    }

  this->PATCH_DISTRIBUTION =  c.get<int>("PATCH_DISTRIBUTION");  
  
  this->UNIFORM_RES_TYPES=c.get<int>("UNIFORM_RES_TYPES");
  this->UNIFORM_DENSITY=c.get<float>("UNIFORM_DENSITY"); 
  
  this->RANDOM_RES_TYPES=c.get<int>("RANDOM_RES_TYPES");
  this->RANDOM_DENSITY=c.get<float>("RANDOM_DENSITY");
  
  this->number_res_types =  this->PATCH_RES_TYPES +  this->RANDOM_RES_TYPES +  this->UNIFORM_RES_TYPES;

  //CHECKS
  if(  this->number_res_types_per_patch > this->number_res_types )
    {
      std::cout<<"ERROR: number_res_types_per_patch > number_res_types!\n";
      std::exit(0);
    }

  //AAPJES
  this->POPULATION_SIZE_AAPJES = c.get<int>("POPULATION_SIZE_AAPJES");
  this->POPULATION_STYLE = c.get<int>("POPULATION_STYLE");
  this->SEL_COEF = c.get<float>("SEL_COEF");

  this->NUM_GROUPS = c.get<int>("NUM_GROUPS");
  if(this->NUM_GROUPS>this->POPULATION_SIZE_AAPJES)
    {
      std::cout<<"ERROR: more groups NUMGROUPS than POPSIZE!\n";
      std::cout<<"Please limit NUM_GROUP to =POPULATION_SIZE_AAPJES\n";
      std::exit(0);
    }

  this->GROUPING_TIME = c.get<long>("GROUPING_TIME")*this->YEAR;
  this->SWITCH_LEADER_TIME = c.get<int>("SWITCH_LEADER_TIME")*this->DAY;
  this->GROUPING_STYLE = c.get<int>("GROUPING_STYLE");
  this->GROUPING_SWITCH = c.get<bool>("GROUPING_SWITCH");
  if(this->GROUPING_TIME>0 && this->NUM_GROUPS<this->POPULATION_SIZE_AAPJES)
    {
      std::cout<<"ERROR: this->GROUPING_TIME>0 but this->NUM_GROUPS<this->POPULATION_SIZE_AAPJES\n";
      std::cout<<"If want to start with solitary NUM_GROUP=POPSIZEAAPJES\n";
      std::exit(0);
    }
  this->TRAJECTORY_LENGTH_AAP = c.get<int>("TRAJECTORY_LENGTH");
  this->TRANSMISSION_CHAIN = c.get<bool>("TRANSMISSION_CHAIN");
  this->TRANS_OPTION_GRADUAL_GROUP_BUILD_UP = c.get<bool>("TRANS_OPTION_GRADUAL_GROUP_BUILD_UP");  
  this->TRANS_OPTION_MOST_EXPERIENCED_SOLITARY = c.get<bool>("TRANS_OPTION_MOST_EXPERIENCED_SOLITARY");  
  this->BIRTH_DEATH = c.get<bool>("BIRTH_DEATH");

  if(this->BIRTH_DEATH && this->TRANSMISSION_CHAIN)
    {
      std::cout<<"ERROR: both BIRTHDEATH and TRANSMISSION_CHAIN\n";
      std::cout<<"Choose one of either and comment out other optoins\n";
      std::exit(0);
    }

  if(this->GROUPING_TIME==0)
    {
      this->MAX_GROUP_SIZE = c.get<int>("MAX_GROUP_SIZE");
      this->HALF_MAX_GROUP_SIZE = this->MAX_GROUP_SIZE/2;
    }
  else
    {
      this->MAX_GROUP_SIZE2 = c.get<int>("MAX_GROUP_SIZE");
      this->MAX_GROUP_SIZE = 2;
      this->HALF_MAX_GROUP_SIZE = 1;
    }

  this->AAP_DEATH_RATE = c.get<float>("AAP_DEATH_RATE"); //per minute
  this->MAX_AGE = c.get<float>("MAX_AGE")*this->YEAR;

  //SAVING DATA
  this->SAVE_RESOURCETYPES = c.get<bool>("SAVE_RESTYPES");
  this->SAVE_PREFS = c.get<bool>("SAVE_PREFS");
  this->SAVE_DIET = c.get<bool>("SAVE_DIET");
  this->SAVE_BEH_ENERGY = c.get<bool>("SAVE_BEH_ENERGY");
  this->SAVE_GROUP_IND = c.get<bool>("SAVE_GROUP_IND");
  this->SAVE_GROUP_ENV = c.get<bool>("SAVE_GROUP_ENV");
  this->SAVE_BEH_CHOICES = c.get<bool>("SAVE_BEH_CHOICES");
  this->SAVE_SOC_LEARN = c.get<bool>("SAVE_SOC_LEARN");
  this->SAVE_TIME = (int)(c.get<float>("SAVE_TIME")*this->DAY);
  //std::cout<<"SAVE TIME "<<this->SAVE_TIME<<std::endl;
  //std::cin.get();

  this->TRIAL_RATE_SWITCH = c.get<int>("TRIAL_RATE_SWITCH");  
  this->LEARN_UPDATE_SWITCH = c.get<bool>("LEARN_UPDATE_SWITCH");
  this->TEMP_STIMULUS_SWITCH = c.get<bool>("TEMP_STIMULUS_SWITCH");
  this->SKILL_LEARN = c.get<bool>("SKILL_LEARN_SWITCH");
  this->SOC_SKILL_LEARN_SWITCH = c.get<bool>("SOC_SKILL_LEARN_SWITCH");
  //this->SOCIAL_STIMULUS_SWITCH = c.get<int>("SOCIAL_STIMULUS_SWITCH");
  this->P_SOCIAL_STIMULUS_SWITCH = c.get<int>("P_SOCIAL_STIMULUS_SWITCH");
  this->EXPLORE_SWITCH = c.get<int>("EXPLORE_SWITCH");
  this->SKILL_LEARN_TYPE = c.get<int>("SKILL_LEARN_TYPE");
  this->INIT_PREF_SWITCH = c.get<bool>("INIT_PREF_SWITCH");
  this->EXPLORE_REL_SWITCH = c.get<bool>("EXPLORE_REL_SWITCH");
  this->SOLITARY_SOCIAL_LEARNING = c.get<bool>("SOLITARY_SOCIAL_LEARNING");

  this->BIAS_F_SWITCH = c.get<bool>("BIAS_F_SWITCH");
  this->BIASES_SWITCH = c.get<bool>("BIASES_SWITCH");
  this->BIAS_A_SWITCH = c.get<bool>("BIAS_A_SWITCH");
  this->BIAS_T_SWITCH = c.get<bool>("BIAS_T_SWITCH");
  this->BIAS_P_SWITCH = c.get<bool>("BIAS_P_SWITCH");
  this->BIAS_M_SWITCH = c.get<bool>("BIAS_M_SWITCH");
  this->BIAS_R_SWITCH = c.get<bool>("BIAS_R_SWITCH");
  this->CHOOSE_OLDEST = c.get<bool>("CHOOSE_OLDEST");

  this->ENVIRONMENTAL_CHANGE = c.get<int>("ENVIRONMENTAL_CHANGE");
  this->ENVIRONMENTAL_CHANGE_TIME = (int)(c.get<float>("ENVIRONMENTAL_CHANGE_TIME")*this->DAY);
  this->ENVIRONMENTAL_CHANGE_RATE = c.get<float>("ENVIRONMENTAL_CHANGE_RATE");
  if(this->ENVIRONMENTAL_CHANGE_TIME>0 && this->ENVIRONMENTAL_CHANGE_RATE>0)
    {
      std::cout<<"ERROR: BOTH ENVCHANGETIME and ENVCHANGERATE > 0! Choose 1!\n";
      std::exit(0);
    }
  if(this->ENVIRONMENTAL_CHANGE_TIME>0 && this->ENVIRONMENTAL_CHANGE_TIME>this->SAVE_TIME)
   {
      std::cout<<"ERROR: ENVCHANGETIME > SAVETIME! Will lead to data problems!\n";
      std::exit(0);
    }  

  this->RANDOM_AGE_DISTRIBUTION = c.get<bool>("RANDOM_AGE_DISTRIBUTION");

  this->REPLACE_DEAD = c.get<bool>("REPLACE_DEAD");
  this->GENOME_RANGE = c.get<int>("GENOME_RANGE");
  this->GR_MIN = c.get<float>("GR_MIN");
  this->GR_MAX = c.get<float>("GR_MAX");
  this->GENOME_RANGE_GROUP = c.get<int>("GENOME_RANGE_GROUP");

  if(this->GENOME_RANGE>0 && this->BIRTH_DEATH==1)
    {
      if(!this->REPLACE_DEAD)
	{
	  std::cout<<"ERROR: GENOME_RANGE SELECTED but REPLACE DEAD false!\n";
	  std::exit(0);
	}
      if(this->POPULATION_STYLE==0)
	{
	  std::cout<<"ERROR: GENOME_RANGE SELECTED but POP_STYLE==0!\n";
	  std::exit(0);
	}
    }

  this->EXP_CONDITION = c.get<int>("EXP_CONDITION");
}

void Environment::Simulate(Config c)
{  
  this->global_time = 0;  //clock in minutes
  this->global_time_units = (long)(this->TIME_SCALAR*this->global_time); //NOT NEEDED
  
  //OVERALL CHECK IF TIME UNITS FIT IN VARIABLES
  if((unsigned long) std::numeric_limits<long>::max() < (unsigned long) this->YEAR*this->TIME_SCALAR*this->SIMULATION_TIME)
    {
      std::cout<<"ERROR: time units out of bound\n";
      std::cout<<"t_a can be maximamally "<<std::numeric_limits<long>::max()<<std::endl;
      std::cout<<"but needs to be "<<(long) this->YEAR*this->TIME_SCALAR*this->SIMULATION_TIME <<std::endl;
      std::cout<<"SOLUTIONS: "<<std::endl;
      std::cout<<"1. Reduce SIMULATION_TIME"<<std::endl;
      std::cout<<"2. Redefine all time variables with type `long' as `unsigned long'"<<std::endl;
      std::exit(0);
    }
  
  //initialize priority queue
  for(std::vector<Aapje*>::iterator it = this->aapjes.begin(); it!=this->aapjes.end(); ++it)
    {
      if((*it)==NULL){std::cout<<"ERROR (Simulate()): initializing priorQ with NULL aapje!\n"; std::exit(0);}
      AapjeQ.push(*it);
    }
  
  // - loop from time = 0 to time = this->YEAR*this->TIME_SCALAR*this->SIMULATION_TIME
  while(!this->aapjes.empty() && this->global_time < this->SIMULATION_TIME)
    {
#ifdef _GRAPHICS_
      graphics::set_environment(this); //TODO: does this need to be done each time point?
      graphics::set_population(this->aapjes); //TODO: remove?
      if(graphics::running || !DISPLAY_EVERY_ACTION)
	{
#endif
	  
	  //call highest priority event + remove from Priority Q
	  Aapje* focal = AapjeQ.top();
	  AapjeQ.pop();                 //remove from priorityQ (reinsert after new time assigned)
	  
	  //if(focal==NULL){std::cout<<"ERROR: focal is NULL (Simulate())\n"; std::exit(0);}
	  
	  if(!focal->alive) //is dead
	    {
	      //std::cout<<"Aapje "<<focal->id<<" from group "<<focal->group_id<<" will die!\n";
	      //if population is fixed the dead individual is replaced
	      if(this->POPULATION_STYLE>0 && this->BIRTH_DEATH)
		{
		  Aapje* mom = this->SelectMom();
		  //if(mom==NULL){std::cout<<"ERROR: mom is NULL\n"; std::exit(0);}
		  //std::cout<<"Aapje "<<mom->id<<" reproduces!\n";

		  Aapje* offspring;

		  //fixed population, but variable group size
		  if(this->POPULATION_STYLE==2) offspring = this->AapjeReproduction(mom); 

		  //fixed population and fixed group size
		  if(this->POPULATION_STYLE==1) offspring = this->AapjeReproduction(mom, focal); //group_id + pos of dead
		  //std::cout<<"Aapje "<<offspring->id<<" is born into group "<<offspring->group_id<<"\n";
		  //Need energy reduction mom!
		  if(mom->energy >= mom->birth_energy_threshold)		    
		    mom->energy -= mom->birth_energy_investment;
		  else
		    mom->energy -= 0.5*mom->energy;

		  //used when keep population constant but measuring reproductive success
		  if(this->REPLACE_DEAD)
		    {
		      offspring->CopyAap(focal);
		      offspring->pos = focal->pos;
		      offspring->group_id = focal->group_id;
		      offspring->t_a = mom->t_a;
		    }
		  //if(offspring==NULL){std::cout<<"ERROR: offspring is NULL\n"; std::exit(0);}
		  AapjeQ.push(offspring);	      
		}
	      focal->DataOutputDeath();
	      //bool found=false;
	      for(std::vector<Aapje*>::iterator it=this->aapjes.begin(); it!=aapjes.end(); ++it)
		if((*it)==focal) 
		  {
		    //found=true;
		    //std::cout<<"Aapje "<<focal->id<<" removed from aapjes list!\n";
		    it = this->aapjes.erase(it); //removes from this array + moves iterator to next
		    break;
		  }
	      //if(!found){std::cout<<"ERROR: dead aapjes not found in aapjes array\n"; std::exit(0);}
	      
	      //std::cout<<"Aapje "<<focal->id<<" is killed in KillAapje!\n";
	      this->KillAapje(focal); //TODO: check if focal needs to be deleted?
	      
	      //std::cout<<"Aapje "<<focal->id<<" pointer is deleted\n";
	      //if(focal==NULL) {std::cout<<"ERROR Focal is NULL!\n"; std::exit(0);}
	      delete focal;
	      focal=NULL;
	      //std::cout<<"Aapje "<<focal->id<<" pointer is deleted 2\n";
	      //std::cin.get();
	    }
	  else //is alive
	    {
	      //Do event
	      focal->Todo(); //get new t_a: t_a is always time point at which action ends
	      
	      //If first individual to end action in next minute
	      if( focal->t_a > this->global_time_units )		
		this->DiscreteUpdate(c);
	      
	      AapjeQ.push(focal); //return to Q (if dead: will be removed next action)
	      if(this->POPULATION_STYLE==0 && this->BIRTH_DEATH && focal->WillGiveBirth())
		{
		  //std::cout<<"Aapje "<<focal->id<<" reproduces!\n";
		  Aapje* offspring = this->AapjeReproduction(focal);
		  //if(offspring==NULL){std::cout<<"ERROR: offspring is NULL\n"; std::exit(0);}
		  AapjeQ.push(offspring);
		} 
	    }//end if(alive)
	  //Not returned to AapjeQ if dead and NULL
	  
#ifdef _GRAPHICS_ //TODO: make options for every time point / every action
	  if(DISPLAY_EVERY_ACTION && graphics::stepwise)
	    graphics::running = false;
	}  //end if(graphics::running || !DISPLAY_EVERY_ACTION)
      if(DISPLAY_EVERY_ACTION) graphics::update();	
#endif	
    }//while loop for simtime
}

void Environment::DiscreteUpdate(Config c)
{
  this->global_time++; //change to next minute
  this->global_time_units = (long)(this->TIME_SCALAR*this->global_time); 
  //continuous scaled time  

  if(this->SAVE_GROUP_ENV &&
     (
      (
       this->global_time%(this->SAVE_TIME)==0)
      ||
      this->global_time%this->YEAR==0
      )
     )
    {
      //for all groups  
      for(std::vector<Group>:: iterator it=this->groups.begin();
	  it!=this->groups.end(); ++it)
	{	  
	  if(!(*it).members.empty())
	    {
	      std::cout<<"GSE "<<(float)this->global_time/this->YEAR
		       <<" "<<(*it).id
		       <<" "<<(*it).members.size();
	      for(std::vector<Aapje*>::iterator n=(*it).members.begin(); n!=(*it).members.end(); ++n)
		std::cout<<" "<<(*n)->id;
	      std::cout<<std::endl;
	      //std::cin.get();
	    }//if not empty
	}//for all groups
    }
  
  if(this->GROUPING_SWITCH && this->GROUPING_TIME>0)
    {
      if(this->global_time >= this->GROUPING_TIME)
	{
	  this->MAX_GROUP_SIZE = this->MAX_GROUP_SIZE2;
	  this->HALF_MAX_GROUP_SIZE = this->MAX_GROUP_SIZE/2;
	}
    }

  if(this->GROUPING_STYLE==2 && 
     this->global_time%this->SWITCH_LEADER_TIME==0)
    {
      //for all groups
      for(std::vector<Group>:: iterator it=this->groups.begin();
	  it!=this->groups.end(); ++it)
	{
	  //choose new leader
	  if((int)(*it).members.size()>1)
	    (*it).NewRandomLeader();
	}
    }

  //everyone does this per minute
  if(this->BIRTH_DEATH)
    for(std::vector<Aapje*>::iterator it=this->aapjes.begin(); 
	it!=aapjes.end(); ++it)
      {
	//if(*it==NULL){std::cout<<"ERROR: aapjes is NULL for DiscreteUpdate\n"; std::exit(0);}
	if( (*it)->WillDie())
	  {
	    (*it)->alive=false;
	  }
	else
	  (*it)->DiscreteUpdate();
      }	  
  else
    for(std::vector<Aapje*>::iterator it=this->aapjes.begin(); it!=aapjes.end(); ++it)
      (*it)->DiscreteUpdate(); 
  
  //per minute is checked whether someone is replaced
  if(this->TRANSMISSION_CHAIN) this->TransmissionChain(); //resets some individuals; not alter t_a
  
  if(this->ENVIRONMENTAL_CHANGE>0) this->EnvChange();

  if(this->EXP_CONDITION>0) this->ExpCondition(c);
  
#ifdef _GRAPHICS_ //TODO: make options for every time point / every action
  if(!DISPLAY_EVERY_ACTION && graphics::stepwise)
    graphics::running = false;     
  if(!DISPLAY_EVERY_ACTION)
    graphics::update();	
#endif		  
}
 
void Environment::ChangeResource(int r)
{
  this->resourcetypes[r]->quality = this->GenerateQuality(r);
  this->resourcetypes[r]->n = this->GenerateN();
  this->resourcetypes[r]->h_pow_n = this->GenerateHPowN(this->resourcetypes[r]->n);
}

void Environment::ExpCondition(Config c)
{
  switch(this->EXP_CONDITION)
    {
    case 4: //VARIABLE LEARN BUT NO DEATHS AND BIRTHS
      
      if(this->global_time==(0+1))	  
	for(int g=0; g<(int)groups.size(); g++)
	  for(int i=0; i<(int)groups[g].members.size(); i++)
	    groups[g].members[i]->temp_social_stimulus = 0.0; //initialize without SE
 
      if(this->global_time==((40*YEAR)+1))
	{
	  //per group, select individual
	  for(int g=0; g<(int)groups.size(); g++)
	    {
	      for(int i=0; i<(int)groups[g].members.size(); i++)
		{
		  groups[g].members[i]->temp_social_stimulus = c.get<float>("TEMP_SOCIAL_STIMULUS"); //set SE to parfile value
		  groups[g].members[i]->metabolic_costs = 0.0;
		}//end i in group g
	    }//end g in groups
	  this->AAP_DEATH_RATE = 0; //set death rate to zero
	  this->MAX_AGE = this->SIMULATION_TIME;
	  if(this->MUTATION_RATE>0)  //mutations should be zero - else abort
	    {
	      std::cout<<"ERROR: Mutation rate > 0 in case 1 of ExpConditions\n";
	      std::exit(0);
	    }
	}//end if time == 40*year
      break;

    case 3: //VARIABLE LEARN
      //set SE to zero a beginning
      //go to 40 years: set SE on (init value)

      if(this->global_time==(0+1))	  
	for(int g=0; g<(int)groups.size(); g++)
	  for(int i=0; i<(int)groups[g].members.size(); i++)
	    groups[g].members[i]->temp_social_stimulus = 0.0; //initialize without SE
 
      if(this->global_time==((40*YEAR)+1))
     	for(int g=0; g<(int)groups.size(); g++)
	  for(int i=0; i<(int)groups[g].members.size(); i++)
	    groups[g].members[i]->temp_social_stimulus = c.get<float>("TEMP_SOCIAL_STIMULUS"); //set SE to parfile value

      break;

    case 2: //VARIABLE NO-LEARN
      //set SE to zero at beginning
      //go to 40 years, then set constant (no deaths, no learn)
      //then set SE on (if initialized above zero)

      if(this->global_time==1)	  
	for(int g=0; g<(int)groups.size(); g++)
	  for(int i=0; i<(int)groups[g].members.size(); i++)
	    groups[g].members[i]->temp_social_stimulus = 0.0; //initialize without SE
      
      if(this->global_time==((40*YEAR)+1))
	{
	  //per group, select individual
	  for(int g=0; g<(int)groups.size(); g++)
	    {
	      for(int i=0; i<(int)groups[g].members.size(); i++)
		{
		  //make all others mememory equal to memory of selected_ind:
		  groups[g].members[i]->U = 0.0; //set learning rates to zero		  
		  groups[g].members[i]->temp_social_stimulus = c.get<float>("TEMP_SOCIAL_STIMULUS"); //set SE to parfile value
		  groups[g].members[i]->metabolic_costs = 0.0;
		}//end i in group g
	    }//end g in groups
	  this->SKILL_LEARN = false; //stop skill learning as well
	  this->AAP_DEATH_RATE = 0; //set death rate to zero
	  this->MAX_AGE = this->SIMULATION_TIME;
	  if(this->MUTATION_RATE>0)  //mutations should be zero - else abort
	    {
	      std::cout<<"ERROR: Mutation rate > 0 in case 1 of ExpConditions\n";
	      std::exit(0);
	    }
	}//end if time == 40*year

      break;

    case 1: //ALL SAME NO-LEARN
      //set SE to zero at beginning
      //go to 40 years then make everyone same as oldest and keep constant (no deaths, no learn)
      //then set SE on (if initialized above zero)

      if(this->global_time==1)	  
	for(int g=0; g<(int)groups.size(); g++)
	  for(int i=0; i<(int)groups[g].members.size(); i++)
	    groups[g].members[i]->temp_social_stimulus = 0.0; //initialize without SE

      if(this->global_time==((40*YEAR)+1))
	{
	  //per group, select individual
	  for(int g=0; g<(int)groups.size(); g++)
	    {
	      //per group select oldest indidivual: 
	      int selected_ind = 0;
	      for(int i=1; i<(int)groups[g].members.size(); i++)
		{
		  if(groups[g].members[i]->age > groups[g].members[0]->age)
		    selected_ind = i;
		}//end i in group g
	      
	      for(int i=0; i<(int)groups[g].members.size(); i++)
		{
		  //make all others mememory equal to memory of selected_ind:
		  groups[g].members[i]->prefs = groups[g].members[selected_ind]->prefs;
		  groups[g].members[i]->reliability = groups[g].members[selected_ind]->reliability;
		  groups[g].members[i]->total_processing_events = groups[g].members[selected_ind]->total_processing_events;
		  groups[g].members[i]->total_processing_time = groups[g].members[selected_ind]->total_processing_time;
		  groups[g].members[i]->temp_aversion = groups[g].members[selected_ind]->temp_aversion;
		  groups[g].members[i]->novel = groups[g].members[selected_ind]->novel;
		  groups[g].members[i]->pref_exp = groups[g].members[selected_ind]->pref_exp;

		  groups[g].members[i]->U = 0.0; //set learning rates to zero		  
		  groups[g].members[i]->temp_social_stimulus = c.get<float>("TEMP_SOCIAL_STIMULUS"); //set SE to parfile value		 
		  groups[g].members[i]->metabolic_costs = 0.0;
		}//end i in group g

	    }//end g in groups
	  this->SKILL_LEARN=false;  //stop skill learning as well
	  this->AAP_DEATH_RATE = 0; //set death rate to zero
	  this->MAX_AGE = this->SIMULATION_TIME;
	  if(this->MUTATION_RATE>0)  //mutations should be zero - else abort
	    {
	      std::cout<<"ERROR: Mutation rate > 0 in case 1 of ExpConditions\n";
	      std::exit(0);
	    }
	}//end if time == 40*year
      break;

    case 0:
      std::cout<<"Error case 0 in ExpCondition, should not happen!\n";
      std::exit(0);

    default:
      std::cout<<"Error case is not one of defined cases in ExpCondition, should not happen!\n";
      std::exit(0);
    }
}

void Environment::EnvChange()
{
  if(env_uni()<this->ENVIRONMENTAL_CHANGE_RATE || (this->ENVIRONMENTAL_CHANGE_TIME>0 && this->global_time%this->ENVIRONMENTAL_CHANGE_TIME==0))
    {
      //select random resource
      int ran = (int)(env_uni()*this->resourcetypes.size());
      this->ChangeResource(ran);

      if(this->SAVE_RESOURCETYPES){
	//std::cout<<(float)this->global_time/this->DAY<<std::endl; //1 TIME
	std::cout<<"QN "<<(float)this->global_time/this->YEAR; //1 TIME
	for(std::vector<ResourceType*>::iterator it=resourcetypes.begin(); it!=resourcetypes.end(); ++it)
	  std::cout<<" "<<(*it)->quality;
	std::cout<<std::endl;
      }

      for(std::vector<Aapje*>::iterator it=this->aapjes.begin(); it!=aapjes.end(); ++it)    
	(*it)->MakeNaiveAboutResource(ran);
    }
}
 
void Environment::TransmissionChain()
{
  //TODO: make function where grouping grows gradually from a given time point
  // and new grouping individuals are placed at or near existing grouping ones
  // or near oldest living grouping individual
  // grouping individuals only group with grouping individuals of the same group number
  
  if(this->global_time%this->YEAR==0) //every year
    {
      //select individual -> replace
      int ind = this->global_time/YEAR - (((this->global_time/YEAR)/20)*(20)); 
      //after year 1 => ind 0; year 20
      
      if(TRANS_OPTION_GRADUAL_GROUP_BUILD_UP) //only if not yet grouping
	{
	  if(this->global_time >= this->GROUPING_TIME)
	    {
	      //std::cout<<"TRANS: its grouping time! "<<(float)this->global_time/YEAR<<"\n";
	      //std::cout<<"TRANS: selected individual "<<ind<<"\n";
	      
	      int older_gr_ind = ind-1;
	      if(older_gr_ind<0) older_gr_ind=(POPULATION_SIZE_AAPJES-1);

	      //if not already in same group
	      if(this->aapjes[ind]->group_id!=this->aapjes[older_gr_ind]->group_id)
		{
		  //std::cout<<"TRANS: oldest grouping individual "<<older_gr_ind<<"\n";
		  this->aapjes[ind]->grouping=true;	  
		  this->aapjes[ind]->pos = this->aapjes[older_gr_ind]->pos;	   
		  this->groups[this->aapjes[ind]->group_id].RemoveAndCheckForNewLeader(this->aapjes[ind]);
		  this->aapjes[ind]->group_id = this->aapjes[older_gr_ind]->group_id;	      
		  this->groups[aapjes[ind]->group_id].AddAapje(this->aapjes[ind]);
		  //std::cout<<"TRANS: joins oldest grouping individual "<<older_gr_ind<<" in group "<<this->aapjes[ind]->group_id<<" of size "<<this->groups[aapjes[ind]->group_id].members.size()<<"\n";
		}
	    }//end this->global_time >= this->GROUPING_TIME
	}//end TRANS OPTION GRADUAL BUILT UP
      
      //Switches simulation from solitary to grouping: all attain positoin + experience of oldest individual = hard case for grouping to cumulate on top of solitary
      if(TRANS_OPTION_MOST_EXPERIENCED_SOLITARY)
	{
	  if(this->global_time == this->GROUPING_TIME)
	    {
	      //OPTION: SET ALL EQUAL TO MOST EXPERIENCED SOLITARY
	      int max_age = 0; int oldest_ind=-1;
	      for(int i = 0; i<(int)this->aapjes.size(); i++)
		if(this->aapjes[i]->age > max_age)
		  {
		    max_age=this->aapjes[i]->age; 
		    oldest_ind=i;		
		  }
	      this->aapjes[oldest_ind]->grouping=true;
	      
	      //transfer knowledge to others ...
	      for(int i = 0; i<(int)this->aapjes.size(); i++)
		if(i!=oldest_ind)
		  {
		    this->aapjes[i]->grouping=true;
		    this->aapjes[i]->prefs = this->aapjes[oldest_ind]->prefs;
		    this->aapjes[i]->novel = this->aapjes[oldest_ind]->novel;
		    this->aapjes[i]->pos = this->aapjes[oldest_ind]->pos;
		  }
	    }
	}//TRANS_OPTION_MOST_EXPERIENCED_SOLITARY
      
      //Always reset the focal TRANS individual
      this->aapjes[ind]->Reset(); //no need to have birth deaths. no new ID
      
    }//every year
 }//end TransmissionChain();

Aapje* Environment::SelectMom()
{
  Aapje* mom;

  //go through population: totalclaim = (sum energy levels)^SEL_COEFF
  float total_energy=0;
  for(std::vector<Aapje*>::iterator it = this->aapjes.begin(); it != aapjes.end(); ++it)
    {
      /*if((total_energy + pow((*it)->energy, this->SEL_COEF)) > std::numeric_limits<float>::max())
	{
	  std::cout<<"Warning! total energy in selectmom exceeds max!\n";
	  std::cout<<" env.cpp: SelectMom()\n";
	  std::exit(0);
	  }*/

      float pw = pow((*it)->energy/10000, this->SEL_COEF);
      total_energy += pw; //pow((*it)->energy, this->SEL_COEF);
      if(!std::isfinite((*it)->energy))
	 std::cout<<"id "<<(*it)->id<<" tot "<<total_energy<<" energy "<<(*it)->energy<<" pow "<<pw<<std::endl;
   }

  //select random claim: env_uni()*totalclaim (prob of selecting ind proportional to claim)
  float claim = env_uni()*total_energy;
  //std::cout<<"claim "<<claim<<std::endl;
 
  //go through population and find individual associated with claim
  float total_energy2=0;
  for(std::vector<Aapje*>::iterator it = this->aapjes.begin(); it != aapjes.end(); ++it)
    {
      total_energy2 += pow((*it)->energy/10000, this->SEL_COEF);
      mom = (*it);
      if(claim<=total_energy2) return mom;
    }
  //return mom;
  std::cout<<"ERROR: did not select mom!\n";
  std::cout<<"claim "<<claim<<" total_energy"<<total_energy<<" total_energy2 "<<total_energy2<<std::endl;
  std::cout<<"ERROR: environment.cpp *** SelectMom() ****\n";
  std::exit(0);
}

void Environment::KillAapje(Aapje* dead)
{
  if(XPOS_LIST) (void)dead->RemoveFromXposList();
  if(CENTROID_GROUPING)
    {
      //std::cout<<"Aapje "<<dead->id<<" will be in RemoveAapje (from group)\n";
      this->groups[dead->group_id].RemoveAndCheckForNewLeader(dead);
      //std::cin.get();
    }
}

void Environment::KillAapjes(std::vector<Aapje*> theDead)
{
  while(!theDead.empty())
    {
      //std::cout<<"Killaapjes aap "<<(*theDead.begin())->id<<std::endl;
      //Lifespan data already printed when selected for death     
      Aapje* dead = (*theDead.begin());
      // - remove from xposlist
      if(XPOS_LIST) (void)(*theDead.begin())->RemoveFromXposList(); 
      //returned iterator not needed here
      // - remove for theDead vector
      if(CENTROID_GROUPING)
	{      
	  this->groups[dead->group_id].RemoveAndCheckForNewLeader(dead);
	}
      theDead.erase(theDead.begin()); 
      delete dead; //CHECK: does this still work NOT USED NOW
      dead=NULL;
    }
}

Aapje* Environment::AapjeReproduction(Aapje* mom)
{
  //if(!mom->alive) {std::cout<<"Error: mom is dead!\n"; std::exit(0);}
  //if(mom==NULL) {std::cout<<"Error: mom is NULL!\n"; std::exit(0);}
  mom->offspring++;
  Aapje *f;
  Aapje *frg = new Aapje(this, mom); //mutations occur during construction object from mom
  f = frg;
  //std::cout<<"offspring "<<f->id<<std::endl;
  //std::cout<<"mom group id "<<mom->group_id<<" off group id "<<f->group_id<<"\n";
  this->aapjes.push_back(f);
  if(XPOS_LIST) f->InsertInXposList();
  if(CENTROID_GROUPING) 
    {
      this->groups[f->group_id].AddAapje(f);
      this->CheckForGroupSplit(f->group_id);
    }
  return f;
}

Aapje* Environment::AapjeReproduction(Aapje* mom, Aapje* dead)
{
  mom->offspring++;
  Aapje *f;
  Aapje *frg = new Aapje(this, mom); //mutations occur during construction object from mom
  f = frg;
  f->group_id = dead->group_id;

  //if(f->group_id >= (int)this->groups.size())
  //{
  //  std::cout<<"ERROR: f->group_id >= groups.size\n";
  //  std::cout<<"environment.cpp: AapjeReproduction()\n";
  //  std::exit(0);
  //}


  f->pos = dead->pos;
  //std::cout<<"offspring "<<f->id<<std::endl;
  this->aapjes.push_back(f);
  if(XPOS_LIST) f->InsertInXposList();
  if(CENTROID_GROUPING)
    {
      this->groups[f->group_id].AddAapje(f);
      //No check for group split because group size in constant -or- FollowMom
    }
  return f;
}


///////////////////////////////////////////////////////////////////////
//InitAapjes

void Environment::InitAapjes(Config c)
{
  if(CENTROID_GROUPING)
    for(int i = 0; i < this->NUM_GROUPS; i++)
      {
	Group new_group = Group(i);
	this->groups.push_back(new_group);
      }

  for(int i = 0; i < this->POPULATION_SIZE_AAPJES; i++)
    {
      Aapje *f;
      Aapje *frg = new Aapje(c, this, this->RANDOM_AGE_DISTRIBUTION);
      
      f = frg;
      this->aapjes.push_back(f);
      if(CENTROID_GROUPING)
	{
	  this->groups[f->group_id].AddAapje(f); 
	  //group_id is set in aap constructor as: f->id%this->NUM_GROUPS
	}
    }

  if(this->GENOME_RANGE>0)
    {
      if(this->GENOME_RANGE_GROUP==0)
	{
	  int root = std::sqrt((float)this->POPULATION_SIZE_AAPJES);
	  if(root*root!=this->POPULATION_SIZE_AAPJES)
	    {
	      std::cout<<"sqrt(POPULATION_SIZE_AAPJES) is not obvious!\n";
	      std::cout<<"POPSIZE "<<this->POPULATION_SIZE_AAPJES<<" root "<<root<<std::endl;
	      std::cout<<"Please choose new POPSIZE which is the square of an integer\n";
	      std::exit(0);
	    }
	  for(int it1=0; it1<root; it1++)
	    for(int it2=0; it2<root; it2++)
	      this->aapjes[(it1*root)+it2]->GenomeRange(root, it1, it2, this->GR_MIN, this->GR_MAX);
	}

      //1 All in group same:
      if(this->GENOME_RANGE_GROUP==1)
	{
	  int root = std::sqrt((float)this->NUM_GROUPS);
	  if(root*root!=this->NUM_GROUPS)
	    {
	      std::cout<<"sqrt(NUM_GROUPS) is not obvious!\n";
	      std::cout<<"NUM_GROUPS "<<this->NUM_GROUPS<<" root "<<root<<std::endl;
	      std::cout<<"Please choose new NUM_GROUPS which is the square of an integer\n";
	      std::exit(0);
	    }

	  for(int it1=0; it1<root; it1++)
	    for(int it2=0; it2<root; it2++)
	      {
		//group_id
		int group_id = (it1*root)+it2;
		//find all X individuals with that group_id + do
		//every [group_id + ind_id*NUM_GROUP] individuals 
		//this->aapjes[X]->GenomeRange(root, it1, it2);
		for(int ind=0; ind<this->POPULATION_SIZE_AAPJES; ind++)
		  {
		    if(this->aapjes[ind]->group_id == group_id)
		      {
			this->aapjes[ind]->GenomeRange(root, it1, it2, this->GR_MIN, this->GR_MAX);
		      }
		  }
	      }
	}

      //2 All in group different
      if(this->GENOME_RANGE_GROUP==2)
	{
	  int root = std::sqrt((float)this->MAX_GROUP_SIZE);
	  if(root*root!=this->MAX_GROUP_SIZE)
	    {
	      std::cout<<"sqrt(MAX_GROUP_SIZE) is not obvious!\n";
	      std::cout<<"MAX_GROUP_SIZE "<<this->NUM_GROUPS<<" root "<<root<<std::endl;
	      std::cout<<"Please choose new MAX_GROUP_SIZE which is the square of an integer\n";
	      std::exit(0);
	    }
	  
	  //for each group
	  for(int g=0; g<this->NUM_GROUPS; g++)
	    {
	      int it1=0; int it2=0;
	      for(int a=0; a<this->POPULATION_SIZE_AAPJES; a++)
		{
		  if(this->aapjes[a]->group_id==g)
		    {
		      this->aapjes[a]->GenomeRange(root, it1, it2, this->GR_MIN, this->GR_MAX);
		      it2++;
		      if(it2==root)
			{
			  it1++;
			  it2=0;
			}
		    }
		}
	    }
	}
    }

  if(XPOS_LIST)
    for(std::vector<Aapje*>::iterator it = this->aapjes.begin(); it != aapjes.end(); ++it)
      {
	(*it)->InsertInXposList();
      }
}

///////////////////////////////////////////////////////////////////////
//MAKE FUNCTION GENERATING RESOURCES
void Environment::InitEnvironment()
{  
  //reserve memory
  int total_number_res_types = this->PATCH_RES_TYPES + this->UNIFORM_RES_TYPES + this->RANDOM_RES_TYPES;
  this->resourcetypes.reserve(total_number_res_types);
  this->patches.reserve(this->number_patches);
  if(RSTAR) 
    this->resources.reserve((this->NUMBER_RES_ITEMS_PER_PATCH*this->number_patches)+(this->UNIFORM_DENSITY*(this->XFIELD*this->YFIELD)));
  else
    this->resourcesGRID =  std::vector< std::vector< std::vector<Resource*> > > (this->XFIELD, std::vector< std::vector<Resource*> >(this->YFIELD, std::vector<Resource*>() ) ) ;

  //std::cout<<"Generating Res Types\n";
  this->GenerateResourceTypes();
  if(this->SAVE_RESOURCETYPES) this->DataOutput();
  
  if(this->PATCH_RES_TYPES>0 && this->number_patches>0)
    {  
      //std::cout<<"Generating Patches\n";
      this->GeneratePatches();      
      //std::cout<<"Generating Patches Resources\n";
      this->GeneratePatchyResources(); 
    }

  if(this->RANDOM_RES_TYPES>0 && this->RANDOM_DENSITY>0)
    {
      this->GenerateRandomResources(); 
    }
  
   if(this->UNIFORM_RES_TYPES>0 && this->UNIFORM_DENSITY>0)
    {
      this->GenerateUniformResources(); 
    }
}

void Environment::DataOutput()
{	  
  std::cout<<"Q ";
  for(std::vector<ResourceType*>::iterator it=resourcetypes.begin(); it!=resourcetypes.end(); ++it)
    std::cout<<" "<<(*it)->quality;
  std::cout<<std::endl;

  std::cout<<"N ";
  for(std::vector<ResourceType*>::iterator it=resourcetypes.begin(); it!=resourcetypes.end(); ++it)
    std::cout<<" "<<(*it)->n;
  std::cout<<std::endl;

  std::cout<<"H ";
  for(std::vector<ResourceType*>::iterator it=resourcetypes.begin(); it!=resourcetypes.end(); ++it)
    std::cout<<" "<<(*it)->h_pow_n;
  std::cout<<std::endl;
}

void Environment::GenerateResourceTypes()
{
  int restime = residence_time; 
  int total_number_res_types = this->PATCH_RES_TYPES + this->UNIFORM_RES_TYPES + this->RANDOM_RES_TYPES;

  for(int t=0; t<total_number_res_types; t++)
    {
      long apptime = GenerateAppearanceTime(t);
      float quality = GenerateQuality(t);      
      //std::cout<<"rt "<<t<<" q "<<quality<<std::endl;
      
      if(LEARN_TYPE==1)
	{
	  int n; float h_pow_n;
	  if(quality>0)
	    {
	      n = GenerateN();
	      h_pow_n = GenerateHPowN(n);
	    }
	  else {h_pow_n = 0.0000001; n=20;} //make maximal immediately

	  ResourceType r = ResourceType(apptime, restime, quality, h_pow_n, n);
	  //std::cout<<"res "<<t<<" q "<<quality<<" n "<<n<<" hpown "<<h_pow_n<<std::endl;
	  ResourceType *nr = new ResourceType(r);
	  resourcetypes.push_back(nr);	  
	}
      else
	{
	  ResourceType r = ResourceType(apptime, restime, quality);	
	  ResourceType *nr = new ResourceType(r);
	  resourcetypes.push_back(nr);
	}
    }
}

float Environment::GenerateHPowN(int n)
{
  float h = MIN_H + (env_uni()*(MAX_H - MIN_H));
  //std::cout<<"h "<<h/this->DAY<<std::endl;
  //std::cout<<"n "<<n<<std::endl;
  //std::cout<<pow(h,n)<<std::endl;
  return pow(h,n);
}

int Environment::GenerateN()
{
  return this->MIN_N + (int)(env_uni()*(float)((this->MAX_N+1)-this->MIN_N));
}

float Environment::GenerateQuality(int r)
{  
  switch(this->quality_type)
    {
    case 0: //normal distr  
      {    
	float quality = this->Q_MEAN + env_nor()*this->Q_STDEV;
	if(quality<0.0) return quality*this->NEGATIVE_QUALITY_SCALAR;
	else return quality;
      }
    case 1: //uniform distr
      return  ((float)r/this->number_res_types)/5.0; //TODO: make so that same STDEV + MEAN?
    case 2: //all same
      return this->Q_MEAN;
    default:
      return this->Q_MEAN;
    }
}

long Environment::GenerateAppearanceTime(int r)
{
  switch(this->appear_type)
    {
    case 0: //sync
      return 0;
    case 1: //random
      return  (long)(env_uni()*this->YEAR);
    case 3: //regular
      return (long)(r*(this->YEAR/this->number_res_types));
    default:
      return 0;
    }
}
 
void Environment::GeneratePatches()
{
  int num_patch_types = this->PATCH_RES_TYPES/this->number_res_types_per_patch;
  int num_patch_per_type = this->number_patches/num_patch_types;  

  int count = 0;
  for(int t=0; t<(int)num_patch_types; t++)
    {
      int start = t*this->number_res_types_per_patch;
      std::vector<int> patch_resourcetypes;
      for(int r=start; r<start+this->number_res_types_per_patch; r++)
	{
	  patch_resourcetypes.push_back(r);
	}

      for(int i=0; i<(int)num_patch_per_type; i++)
	{	  
	  float radius = patch_size;
	  Point2d pos = Point2d(-1, -1);         //position
	  Patch p = Patch(count, pos, radius);   //create patch
	  Patch *np = new Patch(p);
	  this->patches.push_back(np);
	  if(RSTAR) this->rstar_patches->insertData((uintptr_t) np, p.mbr());      

	  std::vector<int> resourcetypes_subset = patch_resourcetypes;
	  while( resourcetypes_subset.size()>(unsigned long)this->subset_res_types_per_patch )
	    {
	      int ran = env_uni()*resourcetypes_subset.size();
	      resourcetypes_subset.erase(resourcetypes_subset.begin()+ran);
	    }
	  np->resourcetypes = resourcetypes_subset;
	  count++;	
	}  
    }
  this->PositionPatches();
}

void Environment::PositionPatches()
{
  switch(PATCH_DISTRIBUTION)
    {
    case 0: //RANDOM
      this->PositionPatchesRandomly();
      break;
    case 1: //REGULAR SPACED
      this->PositionPatchesRegularly();
      break;
    default:
      this->PositionPatchesRandomly();
    }
}

void Environment::PositionPatchesRegularly()
{
  int row = sqrt((float)this->patches.size());

  if((unsigned long)(row*row) != this->patches.size())
    {
      std::cout<<std::endl;
      std::cout<<"ERROR: cannot place the number of patches requested!\n";
      std::cout<<"Number of patches:"<<this->patches.size()<<"; Sqrt is: "<<sqrt((float)this->patches.size())<<"; Sqrt needs to be an integer.\n";
      std::cout<<"environment.cpp: ***** PositionPatchesRegularly*****\n";
      std::exit(0);
    }

  //available space for a row= (XFIELD - (2*patch_size))
  //distance between patches = (XFIELD - (2*patch_size))/row
  int dist = ((this->XFIELD - this->patch_size) - this->patch_size)/(row-1);

  //for placing patches in random order
  std::vector<Patch*> temp_patches = this->patches;

  for(int x=0; x<row; x++)
    for(int y=0; y<row; y++)
      {
	if(temp_patches.size()==0)
	  {
	    std::cout<<"ERROR: trying to place more patches than there are!\n";
	    std::cout<<"environment.cpp: ***** PositionPatchesRegularly*****\n";
	    std::exit(0);
	  }
	int ran_patch = env_uni()*temp_patches.size();
	int p = temp_patches[ran_patch]->id;

	float posx = this->patch_size + x*dist;
	float posy = this->patch_size + y*dist;       
	this->patches[p]->position = Point2d(posx, posy);     //position
	temp_patches.erase(temp_patches.begin()+ran_patch);
      }
}


void Environment::PositionPatchesRandomly()
{
  for(std::vector<Patch*>::iterator it = this->patches.begin(); it != patches.end(); ++it)
    {
      float ranx = this->patch_size + (env_uni()*(this->XFIELD - (2*this->patch_size)));
      float rany = this->patch_size + (env_uni()*(this->YFIELD - (2*this->patch_size)));
      (*it)->position = Point2d(ranx, rany);     //position
    }
}
 
void Environment::GeneratePatchyResources()
{  
  for(std::vector<Patch*>::iterator it = this->patches.begin(); it != patches.end(); ++it)
    {
      switch(this->distr_res_in_patch)
	{
	case 0:
	  this->GeneratePatchyResources_Random(*it);
	  break;
	case 1:	  
	  this->GeneratePatchyResources_RandomVec(*it);
	  break;
	case 2:
	  this->GeneratePatchyResources_Grid(*it);
	  break;
	default:	  
	  this->GeneratePatchyResources_Random(*it);
	}      
    }
}

void Environment::GeneratePatchyResources_Grid(Patch *patch)
{
  //make list of all resources of all types
  std::vector<int> patch_resources;      
  int num_res_per_type = this->NUMBER_RES_ITEMS_PER_PATCH / patch->resourcetypes.size();
  for(int t=0; t<(int)patch->resourcetypes.size(); t++)
    for(int r=0; r<num_res_per_type; r++)
      {
	patch_resources.push_back(patch->resourcetypes[t]); //array of all resources types in patch
      }
  
  //count how many position in patch = resources per layer
  //check how many layers to place?
  int startx = patch->position.x - patch->radius;
  int endx = patch->position.x + patch->radius;
  int starty = patch->position.y - patch->radius;
  int endy = patch->position.y + patch->radius;

  //check how many layers to fill (some layers may not be completely full)
  int distsq = patch->radius*patch->radius;
  int pos_per_layer=0;
  for(int x=startx; x<endx; x++)
   for(int y=starty; y<endy; y++)
     {
       Point2d pos(x,y);
       if( (pos - patch->position).sqlength() <= distsq )
	 pos_per_layer++;
     }
  float layers = (float)this->NUMBER_RES_ITEMS_PER_PATCH / pos_per_layer;

  while(layers > 0)
    {
      for(int x=startx; x<endx; x++)
	for(int y=starty; y<endy; y++)
	  {
	    if(patch_resources.size()==0) break;
	    Point2d pos(x,y);
	    int ran = env_uni()*patch_resources.size();
	    if( (pos - patch->position).sqlength() <= distsq && env_uni()<layers)
	      //place resource of random type	    
	      this->GenerateResource(pos, patch_resources[ran]);	    
	    patch_resources.erase(patch_resources.begin()+ran);
	  }
      layers -= 1.0;
    }    
}

void Environment::GeneratePatchyResources_RandomVec(Patch *patch)
{     
  int num_res_per_type = this->NUMBER_RES_ITEMS_PER_PATCH / patch->resourcetypes.size();
  for(int t=0; t<(int)patch->resourcetypes.size(); t++)
    for(int r=0; r<num_res_per_type; r++)
      {
	int type = patch->resourcetypes[t]; //array of all resources types in patch     
	Point2d pos;
	Vec2d ran; 
	ran.x = env_uni(); ran.y = env_uni();
	if(env_uni()<0.5) ran.x= -ran.x; if(env_uni()<0.5) ran.y= -ran.y; 
	ran = ran.norm(); 
	pos.x = patch->position.x + (ran.x*patch->radius*env_uni());
	pos.y = patch->position.y + (ran.y*patch->radius*env_uni());
	this->GenerateResource(pos, type);
      }
}

void Environment::GeneratePatchyResources_Random(Patch *patch)
{  
  int num_res_per_type = this->NUMBER_RES_ITEMS_PER_PATCH / patch->resourcetypes.size();
  for(int t=0; t<(int)patch->resourcetypes.size(); t++)
    for(int r=0; r<num_res_per_type; r++)
      {
	int type = patch->resourcetypes[t]; //array of all resources types in patch     
	Point2d pos;
	float rx = env_uni(); if(env_uni()<0.5) rx = -rx;
	float ry = env_uni(); if(env_uni()<0.5) ry = -ry;
	pos.x = patch->position.x + (patch->radius*rx);
	pos.y = patch->position.y + (patch->radius*ry);
  
	while( (pos-patch->position).length() > patch->radius)
	  {
	    float rx = env_uni(); if(env_uni()<0.5) rx = -rx;
	    float ry = env_uni(); if(env_uni()<0.5) ry = -ry;
	    pos.x = patch->position.x + (patch->radius*rx);
	    pos.y = patch->position.y + (patch->radius*ry);
	    if(env_uni()<0.5) pos.x= -pos.x; if(env_uni()<0.5) pos.y= -pos.y; 
	  }
	this->GenerateResource(pos, type);
      }
}


void Environment::GenerateUniformResources()
{
  //go across grid + with certain probability fill with random resource type
  //resource types is from RANDOM_RES_TYPE to UNIFORM_RES_TYPE
  int res_per_type = (int)((this->UNIFORM_DENSITY*this->XFIELD*this->YFIELD)/this->UNIFORM_RES_TYPES);

  //make vector of all resoure types availability
  std::vector<int> uni_types;
  for(int t=(PATCH_RES_TYPES+RANDOM_RES_TYPES); t<(PATCH_RES_TYPES+RANDOM_RES_TYPES+UNIFORM_RES_TYPES); t++)
    for(int r=0; r<res_per_type; r++)
      uni_types.push_back(t);
  
  int r_count=(PATCH_RES_TYPES+RANDOM_RES_TYPES);
  for(int x=0; x<this->XFIELD; x++)
    for(int y=0; y<this->YFIELD; y++)
      {	
	if(uni_types.size()==0)
	  break;

	float avg_num_res = UNIFORM_DENSITY;
	int ran = env_uni()*uni_types.size();

	while(avg_num_res>1)
	  {
	    Point2d pos = Point2d(x, y); //position
	    GenerateResource(pos, uni_types[ran]);
	    avg_num_res-=1.0;
	  }

	if(env_uni()<avg_num_res)
	  {
	    Point2d pos = Point2d(x, y); //position
	    GenerateResource(pos, uni_types[ran]);
	  }	  

	uni_types.erase(uni_types.begin()+ran);
	r_count++;
      }

  if(uni_types.size()>0)
    {
      std::cout<<"Uniform resources placed randomly: "<<uni_types.size()<<std::endl;
      for(unsigned long r=0; r<uni_types.size(); r++)
	{	
	  int ran = env_uni()*uni_types.size();
	  Point2d pos = Point2d(env_uni()*this->XFIELD, env_uni()*this->YFIELD); //random position	
	  GenerateResource(pos, uni_types[ran]);
	  uni_types.erase(uni_types.begin()+ran);
	  r_count++;       
	}
    }
}

void Environment::GenerateRandomResources()
{
  int res_per_type = (int)((this->RANDOM_DENSITY*this->XFIELD*this->YFIELD)/this->RANDOM_RES_TYPES);
  //std::cout<<"res per type "<<res_per_type<<" restypes "<<resourcetypes.size()<<std::endl;
  if((unsigned long)(PATCH_RES_TYPES+RANDOM_RES_TYPES)<=resourcetypes.size() && RANDOM_RES_TYPES>0)
    {
      for(int t=PATCH_RES_TYPES; t<(PATCH_RES_TYPES+RANDOM_RES_TYPES); t++)
	for(int r=0; r<(int)res_per_type; r++)
	  {
	    Point2d pos = Point2d((env_uni()*this->XFIELD), (env_uni()*this->YFIELD)); //position 
	    while(pos.x >= this->XFIELD) {pos.x = env_uni()*this->XFIELD;}
	    while(pos.y >= this->YFIELD) {pos.y = env_uni()*this->YFIELD;}
	    GenerateResource(pos, t);
	    //std::cout<<"r "<<t*res_per_type + r<<std::endl;
	  }
    }
  else
    {
      std::cout<<"ERROR: needs to make random resources, but not enough types have been made\n";
      std::cout<<"**** environment.cpp - GenerateRandomResources() *****\n";
      std::exit(0);
    }
}

void Environment::GenerateResource(Point2d pos, int res_type) 
{
  ResourceType *rtype = resourcetypes[res_type];
  long app_time = rtype->appear_time; //beginning of first year
  if(RSTAR)
    {
      Resource res = Resource(pos, app_time, res_type);
      Resource *nres = new Resource(res);
      resources.push_back(nres);
      rstar_resources->insertData((uintptr_t) nres, res.mbr());
    }
  else
    {
      Resource res = Resource(pos, app_time, res_type);
      Resource *nres = new Resource(res);
      resourcesGRID[(int)nres->position.x][(int)nres->position.y].push_back(nres);
    }
}

///////////////////////////////////////////////////////////////////
//OBSERVABLES

bool Environment::CheckPositionIsOnGrid(Point2d pos)
{
  if(pos.x<0 || pos.y<0 || pos.x>=this->XFIELD || pos.y>=this->YFIELD)
    return false;
  else
    return true;
}

bool Environment::ResourceIsAvailable(Resource *r)
{
  if( r->IsAvailable(this->global_time, (long)this->YEAR, this->resourcetypes[r->type]->residence_time) ) 
    return true;
  else 
    return false;
}

int Environment::num_patches()
{
  return patches.size(); //TODO: should be redundant
}

int Environment::num_resource_items()
{
  return resources.size();
}

/////////////////////////////////////////////////////////////////////////////
////////////////// R STAR TREE 
void Environment::InitRtrees()
{
  st_resources = new Storage(); //rstartree/storage.h
  rstar_resources = new RTree(st_resources); //rstarttree/rstartree.h

  // creating an R*-tree with a branch factor of 32, and a fill factor of 0.7
  rstar_resources->create(32, 0.7); //TODO: parameters

  st_patches = new Storage();
  rstar_patches = new RTree(st_patches);

  // creating an R*-tree with a branch factor of 32, and a fill factor of 0.7
  rstar_patches->create(32, 0.7);
}

void Environment::DeleteRtrees()
{
  delete rstar_resources;
  delete st_resources;

  delete rstar_patches;
  delete st_patches;
}

void Environment::ResourcesWithinGRID(std::vector<Resource*> *resources, int minx, int maxx, int miny, int maxy)
{
  if(minx<0) minx=0;
  if(miny<0) miny=0;
  if(maxx>=this->XFIELD) maxx=this->XFIELD-1;
  if(maxy>=this->YFIELD) maxy=this->YFIELD-1;

  for(int x = minx; x<maxx+1; x++) //range of x values
    for(int y = miny; y<maxy+1; y++) //range of y values
      {
	if (!this->resourcesGRID[x][y].empty()) //if there are resources there
	  {
	    resources->insert(resources->begin(), this->resourcesGRID[x][y].begin(), this->resourcesGRID[x][y].end());
	  }
      }
}

std::vector<Resource*> Environment::ResourcesWithinGRID(int minx, int maxx, int miny, int maxy)
{
  if(minx<0) minx=0;
  if(miny<0) miny=0;
  if(maxx>=this->XFIELD) maxx=this->XFIELD-1;
  if(maxy>=this->YFIELD) maxy=this->YFIELD-1;

  std::vector<Resource*> objects;
  for(int x = minx; x<maxx+1; x++) //range of x values
    for(int y = miny; y<maxy+1; y++) //range of y values
      {
	if (!resourcesGRID[x][y].empty()) //if there are resources there
	  {
	    objects.insert(objects.begin(), resourcesGRID[x][y].begin(), resourcesGRID[x][y].end());
	  }
      }
  return objects;
}

vector<Resource*> Environment::ResourcesWithin(Rectangle r)
{
  vector<Resource*> objects;
  rstar_resources->rangeQueryID(objects, r);

  return objects;
  }

std::vector<Patch*> Environment::PatchesWithinGRID(Rectangle r)
{
  std::vector<Patch*> objects;
  for(int p=0; p<(int)this->patches.size(); p++)
    {
      if(
	 this->patches[p]->position.x>=r.min.x
	 &&
	 this->patches[p]->position.x<r.max.x
	 &&
	 this->patches[p]->position.y>=r.min.y
	 &&
	 this->patches[p]->position.y<r.max.y
	 )
	objects.push_back(patches[p]);
    }
  return objects;
}

vector<Patch*> Environment::PatchesWithin(Rectangle r)
{
  vector<Patch*> objects;
  rstar_patches->rangeQueryID(objects, r);

  return objects;
}
