### SIMULATION TYPES ##################################################################

TRANSMISSION_CHAIN = 0  #0 = false; 1 = true; for tranmission change (switch off BIRTH_DEATH)
TRANS_OPTION_GRADUAL_GROUP_BUILD_UP = 0     #0/1 on/off
TRANS_OPTION_MOST_EXPERIENCED_SOLITARY = 0; #0/1 on/off if on switch off other 

BIRTH_DEATH = 1         #0 = false; 1 = true; for birth death (switch off TRANSMISSION)
POPULATION_STYLE = 2    #0 = dynamic
                        #1 = fixed pop + fixed group (selected according to energy + birth costs)
			#2 = fixed pop + dynamic groups
SEL_COEF = 3            # power to which rel fitness is raised if POPULATION_STYLE==1 or 2
SIMULATION_TIME = 120.0 #200.0   #in YEARS #2828000 # 60*60*12
RANDOMIZE_GENOME        = 0     # 0 = no - starts with initial value; 1 = randomizes in range
REPLACE_DEAD            = 0     # makes new offspring from genome of dead to keep pop gene range

EXP_CONDITION           = 0     #0 = none
			  	#1 = ALLSAME-NOLEARN set SE to zero; after 40 years: select oldest IND, make all others same; set learning to zero; set death rate to zero; set SE to initialized value;
				#2 = VARIABLE-NOLEARN set SE to zero; after 40 years: set learning to zero; set death rate to zero; set SE to initialized value;
				#3 = VARIABLE LEARN; set SE to zero; after 40 set SE to init value
				#4 = VARIABLE LEARN - NODEATHS
				   

#NOTE: for EVO settings REPLACE_DEAD==1 and POPULATION_STYLE==1/2
GENOME_RANGE            = 0    # 0 = none; varies one/two genes between min and max regularly
			        # 1 = d_mf versus N
				# 2 = p_T versus U
				# 3 = prefexp update vs minprefexp
				# 4=p_T; 5=U_e; 6=N; 7=E_i; 8=d_mf; 9=U; 10=E_min;
				# 11=U_TS; 12=U_PS; 13=U_TS_N; 14=U_PSN;
				# 15=temp_soc_stim; 16=explore rate; 17=explore stimulus
GR_MIN     		= 0.0001 # min gene value for Genomerange
GR_MAX			= 0.2    # max gene value for Genomerange
GENOME_RANGE_GROUP      = 1         # 0 = no grouping
			            # 1 = group optimum: only between group diffs
                      		    # 2 = within group optimum: only within group diffs

### SAVING DATA ###########################################################################
SAVE_RESTYPES = 1       # lines start with Q/QN, N or H: per resource gives quality, N or H
SAVE_PREFS = 1          # lines start with P: gives actual preference for resource
SAVE_DIET = 1           # lines start with D: gives number of items eaten per resource
SAVE_BEH_ENERGY = 1     # lines start with A: gives number of actions; energy; evolvign genes
SAVE_BEH_CHOICES = 0    # lines start with B: gives beh choices + effect of social stimulus
SAVE_GROUP_IND = 0      # lines start with GSI: group size (excluding self) followed by all neighbors IDs
SAVE_GROUP_ENV = 1      # lines start with GSE: all groups + mother + members + location are printed
SAVE_SOC_LEARN = 1	       
SAVE_TIME = 180          # in days #NOTE! will be converted to INT so THIS*DAY should be an integer (or is rounded down to one)

### TIMESCALE ###############################################################################
DAY = 720             #in munutes #720  #60*12 minutes
YEAR = 360            #in DAYS  #282800  #365 days
TIME_SCALAR = 6000    #60 each cont_time unit = second; 600 cont_time unit = milisecond; 6000 = 0.1*milisecond

### ENVIRONMENT #############################################################################
XFIELD = 6261 #2800  #3430*3430 = 1.5*(2800*2800)
YFIELD = 6261 #2800
ENVIRONMENTAL_CHANGE = 1        #0 None
                                #1 Resources change in quality and become new resources
                                #2 Resources change in quality, but remain known 
ENVIRONMENTAL_CHANGE_RATE = 0.000019025 #resources per minute rate of change! 0.000003805 = 1 per year; 0.00095125 = 250 per year; 0.000076104= 20 per year; 0.000019025 = 5 per year; 500 = 0.0019025
ENVIRONMENTAL_CHANGE_TIME = 0  #in days; 72 = 360/5 res per year; 1.5 = 360/250 res per year #NOTE! will be converted to INT so THIS*DAY should be an integer (or is rounded down to one)

### RESOURCES TYPES ##########################################################################
appear_type = 0             #0 = synchronized; 1 = random; 2 = regular: year/res_types;
residence_time = 1          #in YEARS
quality_type = 0            #0 = normal distr; 1 = uniform; 2 = all same;
Q_STDEV = 0.1               #standard deviation of res quality
Q_MEAN = 0.1                #mean of res quality
NEGATIVE_QUALITY_SCALAR = 1 #negative qualities are multiplied by this to increase explr costs
MIN_N = 1                   #minumum power of pref learnin curve for LEARNTYPE==1
MAX_N = 4                   #maximum power of pref learning curve for LEARNTYPE==1
MIN_H = 1                 #minimum value for halftime max LEARNTYPE==1: DAYS
MAX_H = 1                 #maximum value for halftime max LEARNTYPE==1: DAYS

### RESOURCE DIV & DISTRIB ###################################################################
#DENSITY = 4900*1200/(2800*2800)=0.75

# PATCHY #
PATCH_RES_TYPES = 250 #250 #450
#0.25 / 400, 0.5 / 800, 0.75 / 1200, 1.0 / 1600, 1.25 / 2000, 1.5 / 2400, 2.25/ 3600, 3.0 / 4800, 4.5 /7200, 6.0 / 9600
number_patches = 24500 #4900 #15000 #7350       #sqrt must be int for regular spaced 
PATCH_DISTRIBUTION = 0              #0 = random; 1 = regular spaced
NUMBER_RES_ITEMS_PER_PATCH = 1200 #2400 #limits: items = numpatches*itemsperpatch
number_res_types_per_patch = 5      #less of equal to 'number_res_types'
subset_res_types_per_patch = 3      #less or equal to 'Num_res_types_per_patch'
distr_res_in_patch = 0              #0 = random; 
	                            #1 = random vector (center more dense); 
				    #2 = regular grid
patch_size = 10                     #meters; radius

# UNIFORM #
UNIFORM_RES_TYPES = 0 #250
UNIFORM_DENSITY = 0.0 #0.75 #probability per '1 by 1 grid cell' resource placed
		      #items: (gridcells=area)*unidensity

# RANDOM #
RANDOM_RES_TYPES = 0
RANDOM_DENSITY = 0.0 #0.75  #density per msq for whole area; but placed randomly
		      #items: area*density

### LIFE-HISTORY ##########################################################################
AAP_DEATH_RATE = 0.000000381    # per minute probability of dying: lifespan approx: 1/deathrate
RANDOM_AGE_DISTRIBUTION = 1     #0 = all age = 0; 1 = ages from exponential distribution (rate=0.1)
MAX_AGE = 20 # in years
birth_energy_threshold  = 10000  #energy when can give birth
birth_energy_investment = 5000  #energy given to offspring
metabolic_costs         = 0.01  #energy per minute lost to metabolism
MUTATION_RATE           = 0.00   #rate per gene ONLY FOR BIRTH-DEATH scenario

### GROUPING #############################################################################
GROUPING_SWITCH = 1     #0 = solitary; 1 = grouping; for grouping (can be delayed if GRTIME>0)
GROUPING_STYLE = 0      #0 = center of group; ignores MAXVIEW (can see all group members)
	       	 	#1 = (defunct! NOT WORKING: needs to be CHECKED!) fullest quadrant 
			#2 = follow leader: ignore safenumber + align - uses SAFESPACE only
SWITCH_LEADER_TIME = 1  #units=days; interval for getting new random leader
GROUPING_TIME = 0.0     #year at which MAX_GROUP_SIZE comes into effect; 
	      		#before then MAX_GROUP_SIZE is effectively 1;
POPULATION_SIZE_AAPJES = 100  #240  #16*16=256   #13*13=169
NUM_GROUPS = 10               #minimally 1! Set to POPSIZE if want to start SOL
MAX_GROUP_SIZE = 21           #used when POPSTYLE==0 to determine when groups split

### LEARNING & SELECTIVITY ###################################################################
FULL_KNOW_SWITCH = 0      #boolean: 0 = false (all naive prefs=0); 1 = perfect knowl;
LEARN_TYPE = 1            #0 = pref development via digestion;  DIET_LEARNING
			  #1 = immediate payoffs = prefs;       SKILL_LEARNING
SKILL_LEARN_SWITCH = 1    #0 = yes; 1 = no (for EXP_CONDITION)
SKILL_LEARN_TYPE = 2      #1 = based on events (discrete)
		          #2 = based on time (continuous)
L_NOISE = 0.005           #stdev of noise on payoff learned from practicing behaviour
P_NOISE = 0.0 #0.2        #scale parameter of gamma distr (shape=2)
FOOD_CHOICE_SEL  = 8.539  #exponent of food choice function
INIT_PREF_EXP    = 0.0    #to initialize individuals selectivity 
INIT_PREF_SWITCH = 0      #0=OFF; 1=ON; [BOOL]  0=set to minpref 1=evolves
MIN_PREF_EXP     = 0.0    #minimum value of pref_exp: this was initprefexp 
		          #USED as min value fo mutation
PREF_EXP_UPDATE  = 0.1955 #0->0.999; 1->0.1 #factor by which pref_exp is adjusted
U = 0.7473                #learning rate; pref development update constant
LEARN_UPDATE_SWITCH = 1   #0(false): pref = reward; 1(true): pref = U*(reward-pref) //NEEDED?
E_NOISE = 0.0             #stdev of noise about energy average from digestion for LEARN_TYPE=0
SATIATION_SWITCH = 1     #0 = no satiation; 1 = satiation when full stomach for
                         # DIG_INT time period
SATIATION_FRACTION = 0.9 #fullness of stomach needed to be satiated
EXPLORE_RATE = 0.00281  #rate at which foodchoicerprob is raised by explore_stimulus
EXPLORE_STIMULUS = 1.0   #probability with which foodchoice prob is raised //USED when ERS=1
EXPLORE_REL_SWITCH = 0   #0=OFF; 1=ONl [BOOL] if off then EXPLSTIM=1

### DIRECT SOCIAL LEARNING ####################################################################
TEMP_STIMULUS_SWITCH = 1   #0 = OFF; 1 = ON [BOOL]
SOC_SKILL_LEARN_SWITCH = 0 #0 = OFF; 1 = ON [BOOL]
SOLITARY_SOCIAL_LEARNING = 0 #0 = OFF; 1 = ON [BOOL]
COPYSPACE = 20             #distance resource of feeding neigh can be seen
STIMULUS_DURATION = 30     #minutes that the stimulus lasts
BIASES_SWITCH       = 0    #0 = OFF; 1 = ON [BOOL]
CHOOSE_OLDEST       = 0    #0 = OFF; 1 = ON [BOOL]
L_C                 = 0.0 #random bias
BIAS_F_SWITCH       = 0   #0=OFF; 1=ON [BOOL]
BIAS_A_SWITCH       = 0   #0=OFF; 1=ON [BOOL]
BIAS_T_SWITCH       = 0   #0=OFF; 1=ON [BOOL]
BIAS_P_SWITCH       = 0   #0=OFF; 1=ON [BOOL]
BIAS_M_SWITCH       = 0   #0=OFF; 1=ON [BOOL]
BIAS_R_SWITCH       = 0   #0=OFF; 1=ON [BOOL]
L_F                 = 0.0 #freq bias
L_A                 = 0.0 #age bias
L_T                 = 0.0 #exp bias
L_P                 = 0.0 #payoff bias
L_M                 = 0.0 #mother bias
L_R  		    = 0.0 #reliability bias
TEMP_SOCIAL_STIMULUS = 0.332  #0.332 #preference boost due to seeing someone: prob = (pref/E)^N + C
PERM_SOCIAL_STIMULUS = 0.0    #preference boost due to seeing someone: prob = ((pref+C)/E)^N
SKILL_COPY_RATE     = 0.0  #0.446041372243839 #0.0 #rate at which to do OBSERVE
OBSERVE_TIME        = 0.0  #0.5 #minutes
S_K                 = 0.1 #max skill leanring rate 

### FORAGING #################################################################################
NOTHING_TIME = 1         #minutes
EAT_TIME = 1             #minutes
MAX_STOMACH = 20         #items
DIG_INT = 100            #minutes
REACH = 0.9              #meters

MOVE_SPEED = 60     #meters per minute #0.015 minutes per meter
MOVE_FORWARD_DISTANCE = 8   #steps of distance MOVE_STEP_DISTANCE
MOVE_STEP_DISTANCE = 1      #meters

SEARCH_TIME = 0.5       #minutes
SEARCH_ANGLE = 3.14 
SEARCH_DISTANCE = 2
MAX_ITEMS_ASSESSED_PER_SEARCH = 20
SEARCH_TYPE = 0  #1 = random resource; 2 = closest resource; 0 = preference dependent; #NOT USED

### GROUPING #################################################################################
MAXVIEW = 150 #NOT USED
MAXALIGN = 20
SAFESPACE = 17#12#17 #10  r = sqrt(n*100/3); when scaling radius in light of 3 individual 10 radiusff
SAFENUMBER = 9 #5#9 #3
PROB_CHECK_SAFE = 0.9  #Added to avoid ridgid structures
ALIGNPROB = 0.9
MOVETOGROUP_DISTANCE = 3
MOVETOGROUP_SPEED = 180 #meters per minute #66.67 #0.015 minutes per meter (should be 3 times as high?)

#### GRAPHICS ############################################################################
DISPLAY_EVERY_ACTION = 0    #0=false; 1=true;
DISPLAY_TIME_INTERVAL = 1.0   #display output every 'n' minutes
treesdifferentcolors = 1
displayfood = 1
fooddisplaysize = 0.25
TRAJECTORY_LENGTH = 10

###########################################################################################
### NOT USED ##############################################################################
###########################################################################################
PE_U_N           = 20     #power of age dependent update sigmoid
PE_U_H           = 0      #half max age of age dependent update sigmoid

PREF_EXP_N       = 2
PREF_EXP_H       = 18.5
PREF_EXP_K       = 0.2
lambda_stomach   = 0.405702178903319 #0.6
MOVE_PROB  = 0.8       #NOT USED

CENTROID_DIST = 10 #NOT USED?

S_F                 = 0.0 #freq bias //NOT USED
S_A                 = 0.0 #age bias //NOT USED
S_T                 = 0.0 #exp bias //NOT USED
S_P                 = 0.0 #payoff bias //NOT USED

			  #PERMANENT
			  #4 = constant SOC_STIM (can go above quality, needs to be brought back by pers. info)
			  #5 = if demonstrator has greater payoff -> adopt it with some rate or proportion?

COPY_STRATEGY = 0         #0 = choose random of those seen eating
			  #1 = choose the oldest individual
			  #2 = choose the majority	  
			  #3 = copy if older

#TEMPORARY SOCIAL STIMULUS     

U_TS = 0.0                 #0.0 #update constant for temp stim
U_TS_N = 0.0                  #update constant for temp stim neigh specific
U_TS_INIT = 0.0               #0.0 #update constant for temp stim
U_TS_N_INIT = 0.0             #update constant for temp stim neigh specific
SOCIAL_STIMULUS = 0.0         # needed for version 2014.10
SOCIAL_STIMULUS2 = 0.0        # needed for version 2014.10

P_SOCIAL_STIMULUS_SWITCH = 0  #0 = not included; 
			      #1 = included, but only non-neighbor specific
			      #2 = include PLUS neighbor specific
U_PS = 0.0                     #update constant for perm stim
U_PS_N = 0.0                   #update constant for perm stim
U_PS_INIT = 0.0                #update constant for perm stim
U_PS_N_INIT = 0.0              #update constant for perm stim
assoc_res_soc_learn_rate = 0.0 #update factor for assoc between res and soc stim.

initial_trial_rate = 0.0  #probability to try novel resource //NOT USED
TRIAL_RATE_SWITCH = 0     #0 = trial rate is fixed prob;     //NOT USED
		          #1 = trial rate depends on prefexp and is fixed
                          #2 = trial rate depends on prefexp and is adapted
SOCIAL_STIMULUS_SWITCH = 2 #0 = NONE //NOT USED
		           #1 = temp
		           #2 = temp + skill
                           #3 = temp + skill + perm
SOC_STIM_STYLE = 0	  #Relevant for stimulus enhancment //NOT USED
			  #0 = constant SOC_STIM + truncation; 
                          #1 = constant proportion of foodchoice prob
                          #2 = constant proportion of sq of foodchoice prob
                          #3 = constant proportion of p(1-p)
PREFEXP_UPDATE_STYLE = 1  #0: res q and reduction*0.999 over time;  //NOT REALLY USED
                          #1: update on stomach contexts (up and down by 0.1 when full or not)
                          #2: on stomach but with age dependent update
			  #3: continous (every minute) with stomach fill dependency
EXPLORE_SWITCH = 1       #0 = no exploration; #NOT USED!
	                 #1 = only explore rate -> try food otherwise would not
			 #2 = add explore stimulus to pref -> see if want to try it
			 #3 = allowing reliability to affect expl rate via expl stim weighting
