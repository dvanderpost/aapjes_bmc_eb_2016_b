#source('rscripts/CC_vs_H_EC5.R')

READ_DATA=0
SAVE_PLOT=1

H = c('0.1', '1','5', '10')
H_LABELS = c('0.1', '1','5','10')

YMIN = -0.25
YMAX = 0.5

XMIN = 0.5
XMAX = length(H)+0.5

MIN_AGE=2

MIN_TIME=20

par(mfrow=c(1,1))

SL = c('LE', 'SE', 'SE OL', 'OL')
VERSION='2015.16' 
EC=c('1_5yr') 
EC2=c('EC5')

LTY = c(1,1,1,1,1,1,2)
COLS=c('blue', 'orange', 'red','grey')
LWD=2
PCH=c(15,16,17,18)

YLAB=c('Time', 'Ind', 'Age', 'Qtime', 'Total eaten', 'Average reward', 'Energy intake', 'Average experience', 'Repertoire quality', 'Average skill', 'Repertoire diversity')

####################################################################################
if(READ_DATA==1){
for( c in 1:3 ){
	if( c == 1 ){ SE=0; OBS=0; }
	if( c == 2 ){ SE=1; OBS=0; }
	if( c == 3 ){ SE=0; OBS=1; }
	if( c == 4 ){ SE=1; OBS=1; }
	for( s in 1:10 ){
		for(e in 1:1){
		for(h in 1:length(H)){ #collate all env data per soc L condition
			if(!(as.numeric(H[h])==10 & c<3)){
			FILENAME = paste( 'data/gr_no_evo/NEVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H',H[h],'_',H[h],'_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS',SE,'_SSL',OBS,'_EC',EC,'_v',VERSION,'_is',s,'_rs',s,'_QA.dat', sep="");
			print(FILENAME)
			str=FILENAME;
			d=read.table(str,header=FALSE, sep=" ");
			if(s==1 & h==1 & e==1){ dat = cbind( d,rep(s, nrow(d)), rep(h, nrow(d)) ) }
			else{ dat = rbind(dat, cbind( d,rep(s, nrow(d)), rep(h, nrow(d)) ) ) }
			}
		}#end for H
		}#end for E
	}#end for s
	if( c == 1){ le = dat; }
	if( c == 2){ se = dat; }
	if( c == 3){ obs = dat; }
	if( c == 4){ se_obs = dat; }
}#end for c
}#end READ_DATA

####################################################################################
if(SAVE_PLOT==1) pdf(paste('data/figures/CC_vs_H_EC5.pdf', sep=''))

plot(-10,-10, xlim=c(XMIN,XMAX), ylim=c(YMIN,YMAX), xlab='Task difficulty (H)', ylab='Cumulative cultural change in energy intake', xaxt='n', cex.lab=1.5, cex.axis=1.5)
axis(1, at=1:length(H), labels=H, cex.lab=1.5, cex.axis=1.5) 

abline(h=0)

BVAR = 7 #7=energy; 9=quality; 10=skill; 11=ddiv; 5 = totf

print(c('VAR',BVAR))

print('Plots')

for(c in 1:3){ #for each social L condition
	for(h in 1:length(H)){
		if(!(c<3 & as.numeric(H[h])==10)){ #skip H=10 for LE
		if(c==1) temp=le;
		if(c==2) temp=se;
		if(c==3) temp=obs;
		if(c==4) temp=se_obs;

		htemp20 = temp[temp[,13]==h & temp[,1]==MIN_TIME,]      #get year 20 data for h
		m20 = tapply(htemp20[,BVAR], htemp20[,12], mean)  #caculate means per sim
		htemp120 = temp[temp[,13]==h & temp[,1]==120,]    #get year 120 data for h
		m120 = tapply(htemp120[,BVAR], htemp120[,12], mean) #caculate means per sim
		diffs = (m120-m20)/m20    #get relative difference

		m = mean(diffs) #get overal mean of diffs
		sd = sd(diffs)  #get SD of diffs

		wtest = wilcox.test(diffs)
		BC = 1
		if(wtest$p.value<(0.05/BC) & m>0){
			points(h+(c-2.5)/10, m, col=COLS[c], pch=PCH[c], cex=2.5)
		}
		PCH2=c(0,1,2,5)
		if(wtest$p.value>=(0.05/BC) | m<=0){
			points(h+(c-2.5)/10, m, col=COLS[c], pch=PCH2[c], cex=2.5)
		}
		
		lines(c(h+(c-2.5)/10,h+(c-2.5)/10),c(m-sd,m+sd),col=COLS[c], lwd=LWD)		

		if(as.numeric(H[h])==0.1) {l = m; x = h+(c-2.5)/10;}
		if(as.numeric(H[h])>0.1) {l = append(l,m); x = append(x,h+(c-2.5)/10);}

		}#end if	
	}#end h
	lines(x, l, lwd=3, col=COLS[c], lty=LTY[c])
}#end c

legend('topleft', legend=c('LE','SE','OL'), pch=PCH, col=COLS, cex=1.5)

if(SAVE_PLOT==1) dev.off()
