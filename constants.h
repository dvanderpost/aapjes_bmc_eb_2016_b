#ifndef _CONSTANTS_H_GUARD_
#define _CONSTANTS_H_GUARD_

const float PI = 3.141592654;
const float halfPI = PI/2;
const float GRAVITY = 9.81;

const bool XPOS_LIST = false;
const bool CENTROID_GROUPING = true;
const bool RSTAR = false;

//#define XPOS_LIST 0;
//#define CENTROID_GROUPING 1;
//#define RSTAR 0;

#endif // _CONSTANTS_H_GUARD_
